#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

namespace engine::renderer {
    class Mesh {
    public:
        Mesh(GLenum primitive = GL_TRIANGLES);

        ~Mesh();

        void setPositions(const std::vector<float>& data);

        void setUVs(const std::vector<float>& data);

        void setNormals(const std::vector<float>& data);

        void setIndices(const std::vector<unsigned>& data);

        void finalize();

        GLuint getVao() const;

        unsigned getVertexCount() const;

        bool isIndexed() const;

        GLenum getPrimitive() const;

    private:
        GLuint vao;
        GLuint vbo;
        GLuint ebo;
        unsigned vertexCount;
        bool indexed;
        GLenum primitive = GL_TRIANGLES;
        std::vector<glm::vec3> positions;
        std::vector<glm::vec2> uvs;
        std::vector<glm::vec3> normals;
        std::vector<unsigned> indices;
    };
}
