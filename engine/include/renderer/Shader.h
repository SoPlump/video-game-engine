#pragma once

#include <string>
#include <GL/glew.h>
#include <stdexcept>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace engine::renderer {
    class Shader {
    public:
        Shader(const std::string& vertexPath, const std::string& fragmentPath);

        ~Shader();

        void use() const;

        GLuint programId() const;

        void setUniform(const std::string& name, float value) const;

        void setUniform(const std::string& name, bool value) const;

        void setUniform(const std::string& name, int value) const;

        template<int size>
        void setUniformVector(const std::string& name, const float* value, int count = 1) {
            static_assert(size >= 2 && size <= 4, "Invalid vector size, size must be between 2 and 4 inclusive");
        }

        template<int size, typename T, glm::qualifier qualifier>
        inline void setUniformVector(const std::string& name, const glm::vec<size, T, qualifier>& vector) {
            setUniformVector<size>(name, glm::value_ptr(vector), 1);
        }

        template<int size>
        void setUniformMatrix(const std::string& name, const float* value, int count = 1) {
            static_assert(size >= 2 && size <= 4, "Invalid matrix size, size must be between 2 and 4 inclusive");
        }

        template<int size, typename T, glm::qualifier qualifier>
        inline void setUniformMatrix(const std::string& name, const glm::mat<size, size, T, qualifier>& matrix) {
            setUniformMatrix<size>(name, glm::value_ptr(matrix), 1);
        }

    private:
        GLuint id;
    };

    template<>
    void Shader::setUniformVector<2>(const std::string& name, const float* value, int count);

    template<>
    void Shader::setUniformVector<3>(const std::string& name, const float* value, int count);

    template<>
    void Shader::setUniformVector<4>(const std::string& name, const float* value, int count);

    template<>
    void Shader::setUniformMatrix<2>(const std::string& name, const float* value, int count);

    template<>
    void Shader::setUniformMatrix<3>(const std::string& name, const float* value, int count);

    template<>
    void Shader::setUniformMatrix<4>(const std::string& name, const float* value, int count);
}
