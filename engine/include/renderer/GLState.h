#pragma once

#include <GL/glew.h>

namespace engine::renderer {
    class GLState {
    public:
        GLState();

        ~GLState();

        void setActiveVao(GLuint vao);

        void setActiveShader(GLuint id);

    private:
        GLuint activeVao = 0;
        GLuint activeShaderId = 0;
    };
}
