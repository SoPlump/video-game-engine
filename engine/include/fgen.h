#pragma once

//System includes
#include <vector>
#include <memory>

//Personal includes
#include "system/physics.h"
#include "system/RigidBody.h"
#include "Vector.h"

/**
* A force generator can be asked to add a force to one or more
* particles.
*/
class ForceGenerator
{
public:

	/**
	* Overload this in implementations of the interface to calculate
	* and update the force applied to the given particle
	*/
	virtual void updateForce(std::shared_ptr<engine::physics::RigidBody> rigidbody, engine::physics::real duration) = 0;
}; 

class ForceRegistry
{
protected:

	/**
	* Keeps track of one force generator and the particle ir applies to
	*/
	struct ForceRegistration
	{
		std::shared_ptr<engine::physics::RigidBody> rigidbody;
		std::shared_ptr<ForceGenerator> fg;

		bool operator == (const ForceRegistration& reg) {
			return (rigidbody == reg.rigidbody) && (fg == reg.fg);
		}
	};


	/**
	* Holds the list of registrations
	*/
	typedef std::vector<ForceRegistration> Registry;
	Registry registrations;

public:

	ForceRegistry();

	/**
	* Registers the given force generator to apply to the given particle
	*/
	void add(std::shared_ptr<engine::physics::RigidBody> rigidbody, std::shared_ptr<ForceGenerator> fg);

	/**
	* Removes the given registered pair from the registry.
	* If the pair is not registered, this method will have no effect.
	*/
	void remove(std::shared_ptr<engine::physics::RigidBody> rigidbody, std::shared_ptr<ForceGenerator> fg);

	/**
	* Clears all registrations from the registry.
	*This will not delete the particles or the force generators themselves, just the records of their connection.
	*/
	void clear();

	/**
	* Calls all the force generators to update the forces of their corresponding particles
	*/
	void updateForces(engine::physics::real duration);
};

class Gravity : public ForceGenerator {
	/** Holds the acceleration due to gravity. */
	engine::physics::Vector3r gravity;

public:

	/** Creates the generator with the given acceleration. */
	Gravity(const engine::physics::Vector3r& gravity);

	/** Applies the gravitational force to the given particle */
	virtual void updateForce(std::shared_ptr<engine::physics::RigidBody> rigidBody, engine::physics::real duration);
};

class Spring : public ForceGenerator
{
	/*
	* The point that the spring is attached to in the rigidbody's local coordinates
	* The point on which the spring force will be applied on the rigidbody
	*/
	engine::physics::Vector3r connectionPoint;

	/*
	* The rigidbody connected at the other end of the spring
	*/
	engine::physics::RigidBody* otherEnd;

	/*
	* The point that the spring is attached to on the second rigidbody in that rigidbody's local coordinates
	* The point in which the spring force will be applied on that other rigidbody
	*/
	engine::physics::Vector3r otherConnectionPoint;

	/** Holds the spring constant. */
	engine::physics::real springConstant;

	/** Holds the rest length of the spring. */
	engine::physics::real restLength;

public:
	Spring(const engine::physics::Vector3r& localConnectionPoint, engine::physics::RigidBody* otherRigidBody, const engine::physics::Vector3r& otherConnectionPoint, engine::physics::real springConstant, engine::physics::real restLength);

	virtual void updateForce(std::shared_ptr<engine::physics::RigidBody> rigidBody, engine::physics::real duration);
};


class Buoyancy : public ForceGenerator
{
	/*
	* The maximum submersion depth before object genertes its maximum buoyancy force
	*/
	engine::physics::real maxDepth;

	/*
	* Volume of the rigidbody
	*/
	engine::physics::real volume;

	/*
	* The height of the water plane bove y=0. The plane is parallel to the XZ plane
	*/
	engine::physics::real waterHeight;

	/**
	* Density of the liquid. The density influences the buoyancy of an object.
	*/
	engine::physics::real liquidDensity;

	/**
	* The center of buoyancy of the rigidbody
	*/
	engine::physics::Vector3r centerOfBuoyancy;

public:
	Buoyancy(const engine::physics::Vector3r& centerOfBuoyancy, engine::physics::real maxDepth, engine::physics::real volume, engine::physics::real waterHeight, engine::physics::real liquidDensity = 1000.0f);

	virtual void updateForce(std::shared_ptr<engine::physics::RigidBody> rigidBody, engine::physics::real duration);
};