#pragma once

#include "ecm/Component.h"
#include "components/Transform3D.h"
#include <glm/glm.hpp>
#include <memory>

class Camera : public Component {
public:
    Camera(Entity* const parent);

    ~Camera() override;

    void update(double dt) override;

    void render() const override;

    glm::mat4 getViewMatrix() const;

    virtual const glm::mat4& getProjectionMatrix() const = 0;

    static Camera* mainCamera;

private:
    std::shared_ptr<Transform3D> transform;
};
