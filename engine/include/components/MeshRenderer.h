#pragma once

#include "ecm/Component.h"
#include "renderer/Mesh.h"
#include "renderer/Shader.h"
#include "renderer/GLState.h"
#include "components/Transform3D.h"

class MeshRenderer : public Component {
public:
    MeshRenderer(
        Entity* parent,
        std::shared_ptr<engine::renderer::Mesh> mesh,
        std::shared_ptr<engine::renderer::Shader> shader,
        std::shared_ptr<engine::renderer::GLState> glState
    );
    ~MeshRenderer() override;

    void update(double dt) override;

    void render() const override;

private:
    std::shared_ptr<engine::renderer::Mesh> mesh;
    std::shared_ptr<engine::renderer::Shader> shader;
    std::shared_ptr<Transform3D> transform;
    std::shared_ptr<engine::renderer::GLState> glState;
};
