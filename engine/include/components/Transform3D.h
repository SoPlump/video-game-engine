#pragma once

#include "ecm/Component.h"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

class Transform3D : public Component {
public:
    static const glm::vec3 AXIS_X;
    static const glm::vec3 AXIS_Y;
    static const glm::vec3 AXIS_Z;

    explicit Transform3D(Entity* parent);
    ~Transform3D();

    void update(double dt) override;
    void render() const override;

    glm::mat4 getTransform();

    glm::vec3 getPosition() const;
    float getPositionX() const;
    float getPositionY() const;
    float getPositionZ() const;
    void setPosition(const glm::vec3& position);
    void setPositionX(float x);
    void setPositionY(float y);
    void setPositionZ(float z);
    void move(const glm::vec3& movement);

    glm::vec3 getScale() const;
    float getScaleX() const;
    float getScaleY() const;
    float getScaleZ() const;
    void setScale(const glm::vec3& scale);
    void setScaleX(float scaleX);
    void setScaleY(float scaleY);
    void setScaleZ(float scaleZ);
    void scale(const glm::vec3& scale);

    glm::vec3 getRotation() const;
    float getRotationX() const;
    float getRotationY() const;
    float getRotationZ() const;
    glm::quat getQuaternion() const;
    void setRotation(const glm::vec3& rotation);
    void setQuaternion(const glm::quat& quaternion);
    void setRotationX(float rotationX);
    void setRotationY(float rotationY);
    void setRotationZ(float rotationZ);
    void rotate(const glm::vec3& rotation);

    void lookAt(const glm::vec3& point);
    glm::vec3 right() const;
    glm::vec3 up() const;
    glm::vec3 front() const;

private:
    glm::vec3 position;
    glm::quat rotation;
    glm::vec3 scale_;
};
