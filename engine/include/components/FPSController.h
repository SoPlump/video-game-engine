#pragma once

#include "ecm/Component.h"
#include "ecm/Entity.h"
#include "components/Transform3D.h"
#include <memory>

class FPSController : public Component {
public:
    FPSController(
        Entity* parent,
        float baseSpeed = 3.0f,
        float speedMultiplier = 1.0f,
        float sensitivity = 0.05f
    );

    ~FPSController() override;

    void update(double dt) override;
    void render() const override;

    bool enabled;

private:
    std::shared_ptr<Transform3D> transform;
    float pitch = 0.0f;
    float yaw = -90.0f;
    float baseSpeed;
    float speedMultiplier;
    float sensitivity;
};
