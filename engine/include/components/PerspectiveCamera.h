#pragma once

#include "Transform3D.h"
#include "Camera.h"
#include <glm/glm.hpp>

class PerspectiveCamera : public Camera {
public:
    PerspectiveCamera(Entity* const parent, float fovy, float zNear, float zFar);
    ~PerspectiveCamera() override;

    void update(double dt) override;

    const glm::mat4& getProjectionMatrix() const override;

private:
    glm::mat4 projection;
    float aspectRatio;
    float fovy;
    float zNear;
    float zFar;
};
