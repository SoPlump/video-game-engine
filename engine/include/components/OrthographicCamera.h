#pragma once

#include "Camera.h"
#include <glm/glm.hpp>

class OrthographicCamera : public Camera {
public:
    OrthographicCamera(Entity* const parent, float viewDistance, float zNear, float zFar);

    ~OrthographicCamera() override;

    void update(double dt) override;

    const glm::mat4& getProjectionMatrix() const override;

private:
    glm::mat4 projection;
    float aspectRatio;
    float viewDistance;
    float zNear;
    float zFar;
};


