#pragma once

#include "../system/physics.h"
#include "../ecm/Component.h"

class BodyDimensions : public Component {

public:

    enum Geometry {
        CUBE,
        CYLINDER
    };

    BodyDimensions(Entity* const parent, Geometry geometry, engine::physics::Vector3r dimensions);

    void update(double dt) override;

    void render() const override;

    Geometry getGeometry() {
        return geometry;
    }

    engine::physics::Vector3r getDimensions()
    {
        return dimensions;
    }

private:

    Geometry geometry;

    engine::physics::Vector3r dimensions;

};
