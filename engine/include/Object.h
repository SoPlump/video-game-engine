#pragma once
#include "Vector.h"
#include "system/Primitive.h"

class Object : public Component {

	engine::physics::Vector3r center;
public :
	std::vector<std::shared_ptr<engine::physics::Primitive>> primitives;

public:
	Object(Entity* const, engine::physics::Vector3r const);
	void addPrimitive(std::shared_ptr<engine::physics::Primitive>);
	std::vector< std::shared_ptr<engine::physics::Primitive>> getPrimitives();
	void update(double dt);
	void render() const;
	void integrate(double);

};