#pragma once

#include <system/Particle.h>
#include <system/physics.h>
#include "Vector.h"
#include <vector>
#include <memory>

/** 
* A force generator can be asked to add a force to one or more 
* particles.
*/
class ParticleForceGenerator
{
	public:

		/**
		* Overload this in implementations of the interface to calculate
		* and update the force applied to the given particle
		*/
		virtual void updateForce(std::shared_ptr<engine::physics::Particle> particle, engine::physics::real duration) = 0;
};

/** 
* Holds all the force generators and the particles that they apply to. 
*/
class ParticleForceRegistry
{
	protected: 

		/**
		* Keeps track of one force generator and the particle ir applies to
		*/
		struct ParticleForceRegistration
		{
			std::shared_ptr<engine::physics::Particle> particle;
			std::shared_ptr<ParticleForceGenerator> fg;

			bool operator == (const ParticleForceRegistration& reg) {
				return (particle == reg.particle) && (fg == reg.fg);
			}
		};


		/**
		* Holds the list of registrations
		*/
		typedef std::vector<ParticleForceRegistration> Registry;
		Registry registrations;

	public:

		ParticleForceRegistry();

		/**
		* Registers the given force generator to apply to the given particle
		*/
		void add(std::shared_ptr<engine::physics::Particle> particle, std::shared_ptr<ParticleForceGenerator> fg);

		/**
		* Removes the given registered pair from the registry.
		* If the pair is not registered, this method will have no effect.
		*/
		void remove(std::shared_ptr<engine::physics::Particle> particle, std::shared_ptr<ParticleForceGenerator> fg);

		/**
		* Clears all registrations from the registry. 
		*This will not delete the particles or the force generators themselves, just the records of their connection.
		*/
		void clear();

		/**
		* Calls all the force generators to update the forces of their corresponding particles
		*/
		void updateForces(engine::physics::real duration);
};

class ParticleGravity : public ParticleForceGenerator {
	/** Holds the acceleration due to gravity. */
	engine::physics::Vector3r gravity;

public:

	/** Creates the generator with the given acceleration. */
	ParticleGravity(const engine::physics::Vector3r& gravity);

	/** Applies the gravitational force to the given particle */
	virtual void updateForce(std::shared_ptr<engine::physics::Particle> particle, engine::physics::real duration);
};

class ParticleDrag : public ParticleForceGenerator {
	/** Holds the velocity drag coefficient */
	engine::physics::real k1;

	/** Holds the velocity squared drag coefficient */
	engine::physics::real k2;

public:

	/**Creates the generator with the given coefficient */
	ParticleDrag(engine::physics::real k1, engine::physics::real k2);

	/** Applies  the drag force to the given particule */
	virtual void updateForce(std::shared_ptr<engine::physics::Particle> particle, engine::physics::real duration);
};

class ParticleSpring : public ParticleForceGenerator
{
	/** The particle at the other end of the spring.*/
	std::shared_ptr<engine::physics::Particle> other;

	/** Holds the spring constant. */
	engine::physics::real springConstant;

	/** Holds the rest length of the spring. */
	engine::physics::real restLength;

public:
	/** Creates a new spring with the given parameters. */
	ParticleSpring(std::shared_ptr<engine::physics::Particle> other,
		engine::physics::real springConstant, engine::physics::real restLength);

	/** Applies the spring force to the given particle. */
	virtual void updateForce(std::shared_ptr<engine::physics::Particle> particle, engine::physics::real duration);
};

class ParticleSoftSpring : public ParticleForceGenerator
{
	/** The particle at the other end of the spring.*/
	std::shared_ptr<engine::physics::Particle> other;

	/** Holds the spring constant. */
	engine::physics::real springConstant;

	/** Holds the rest length of the spring. */
	engine::physics::real restLength;

	/** Holds the rest length of the spring. */
	engine::physics::real maxLength;

public:
	/** Creates a new spring with the given parameters. */
	ParticleSoftSpring(std::shared_ptr<engine::physics::Particle> other,
		engine::physics::real springConstant, engine::physics::real restLength, engine::physics::real maxLength);

	/** Applies the spring force to the given particle. */
	virtual void updateForce(std::shared_ptr<engine::physics::Particle> particle, engine::physics::real duration);
};

/**
* A force generator that applies a spring force, where
* one end is attached to a fixed point in space.
*/
class ParticleAnchoredSpring : public ParticleForceGenerator
{
protected:
	/** The location of the anchored end of the spring. */
	std::shared_ptr<engine::physics::Vector3r> anchor;
	/** Holds the spring constant. */
	engine::physics::real springConstant;
	/** Holds the rest length of the spring. */
	engine::physics::real restLength;

public:
	/** Creates a new spring with the given parameters. */
	ParticleAnchoredSpring(std::shared_ptr<engine::physics::Vector3r> anchor,
		engine::physics::real springConstant,
		engine::physics::real restLength);

	/** Applies the spring force to the given particle. */
	virtual void updateForce(std::shared_ptr < engine::physics::Particle> particle, engine::physics::real duration);
};