#pragma once

#include <iostream>
#include <cassert>
#include <cmath>
#include <sstream>



/* This library provides elementary functions to manipulate 3 dimensional vectors. 
* The generic type T is used to populate the vectors and can hence represent integers, floats , doubles etc..
*/

namespace engine::physics {

	template<typename T, size_t size> struct Vector;

	template<typename T>
	struct Vector<T, 3>
	{
	public:
		explicit Vector(T x = 0, T y = 0, T z = 0) : x(x), y(y), z(z) {}

		Vector(const Vector<T, 3>& vec) : x(vec.x), y(vec.y), z(vec.z) {}

		// methods

		/**
		* Returns the square root of the sum of the squared vector components.
		*/
		float magnitude() const {
			return std::sqrt(x * x + y * y + z * z);
		}

		//Returns the sum of the squared vector components.
		float squareMagnitude() const {
			return x * x + y * y + z * z;
		}
		/**
		* Divides the vector by its norm
		*/
		void normalize() {
			float mag = magnitude();
			if (mag > 0) {
				(*this) = (*this) / mag;
			}
		}

		/**
		*  Add vec * scale to the current vector
		*/
		void addScaledVector(const Vector<T, 3>& vec, T scale) {
			x += vec.x * scale;
			y += vec.y * scale;
			z += vec.z * scale;
		}

		/**
		* Calculates and returns a component-wise product of this
		* vector with the given vector.
		*/
		Vector<T, 3> componentProduct(const Vector<T, 3>& vec) const {
			return Vector<T, 3>(x * vec.x, y * vec.y, z * vec.z);
		}

		/**
		* Performs a component-wise product with the given vector and
		* sets this vector to its result.
		*/
		void componentProductUpdate(const Vector<T, 3>& vec)	{
			x *= vec.x;
			y *= vec.y;
			z *= vec.z;
		}

		/**
		* Calculates and returns the scalar product of this vector
		* with the given vector.
		*/
		double dotProduct(const Vector<T, 3>& vec) const	{
			return x * vec.x + y * vec.y + z * vec.z;
		}

		/**
		* Calculates and returns the vector product of this vector
		* with the given vector.
		*/
		Vector<T, 3> crossProduct(const Vector<T, 3>& vec) const
		{
			return Vector<T, 3>(
				y * vec.z - z * vec.y,
				z * vec.x - x * vec.z,
				x * vec.y - y * vec.x);
		}

		std::string toString() {
			std::stringstream result;
			result << "(" << x << "," << y << "," << z << ")" << "\n";
			return result.str();
		}

		void clear() {
			x = y = z = 0;
		}

		union {
			T x;
			T r;
			T i;
		};

		union {
			T y;
			T g;
			T j;
		};

		union {
			T z;
			T b;
			T k;
		};
	};

	typedef Vector<float, 2> Vector2f;
	typedef Vector<float, 3> Vector3f;
}

//Vector print

template<typename T>
std::ostream& operator<<(std::ostream& str, const engine::physics::Vector<T, 3>& vec) {
	return std::cout << '(' << vec.x << ", " << vec.y << ", " << vec.z << ')';
}

// Vector multiplication by a scalar

template<typename T>
engine::physics::Vector<T, 3> operator * (const engine::physics::Vector<T, 3>& vec, const T coeff) {
	return engine::physics::Vector<T, 3>(vec.x * coeff, vec.y * coeff, vec.z * coeff);
}

template<typename T>
engine::physics::Vector<T, 3> operator * (const T coeff, const engine::physics::Vector<T, 3>& vec) {
	return engine::physics::Vector<T, 3>(vec.x * coeff, vec.y * coeff, vec.z * coeff);
}

template<typename T>
void operator *= (engine::physics::Vector<T, 3>& vec, const T coeff) {
	vec = vec * coeff;
}

// Vector division by a scalar

template<typename T>
engine::physics::Vector<T, 3> operator / (const engine::physics::Vector<T, 3>& vec, const T coeff) {
	assert(coeff != 0);
	return engine::physics::Vector<T, 3>(vec.x / coeff, vec.y / coeff, vec.z / coeff);
}

template<typename T>
void operator /= (engine::physics::Vector<T, 3>& vec, const T coeff) {
	assert(coeff != 0);
	vec = vec / coeff;
}

/**
* Calculates and returns the scalar product of this vector
* with the given vector.
*/
template<typename T>
double operator *(const engine::physics::Vector<T, 3>& vec1, const engine::physics::Vector<T, 3>& vec2) { // TODO: really the operation we want for * operator?
	return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
}

template<typename T>
engine::physics::Vector<T, 3> operator + (const engine::physics::Vector<T, 3>& vec1, const engine::physics::Vector<T, 3>& vec2) {
	return engine::physics::Vector<T, 3>(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
}

template<typename T>
engine::physics::Vector<T, 3> operator - (const engine::physics::Vector<T, 3>& vec1, const engine::physics::Vector<T, 3>& vec2) {
	return engine::physics::Vector<T, 3>(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
}

template<typename T>
bool operator == (const engine::physics::Vector<T, 3>& vec1, const engine::physics::Vector<T, 3>& vec2) {
	return (vec1.x == vec2.x) && (vec1.y == vec2.y) && (vec1.z == vec2.z);
}

template<typename T>
bool operator != (const engine::physics::Vector<T, 3>& vec1, const engine::physics::Vector<T, 3>& vec2) {
	return (vec1.x != vec2.x) || (vec1.y != vec2.y) || (vec1.z != vec2.z);
}