#pragma once
#include "Vector.h"
#include "system/physics.h"
#include "system/Primitive.h"
#include "system/RigidBody.h"

class Contact {
public:
	engine::physics::Vector3r contactPoint;
	engine::physics::Vector3r contactNormal;
	std::shared_ptr < engine::physics::RigidBody> rb1;
	std::shared_ptr<engine::physics::RigidBody> rb2;
	engine::physics::real penetration;

	void setBodyData(std::shared_ptr<engine::physics::RigidBody> one , std::shared_ptr<engine::physics::RigidBody> two);

	std::string toString();
};