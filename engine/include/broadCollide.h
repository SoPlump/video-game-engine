#pragma once

//System includes

//Personal includes
#include "system/physics.h"
#include "system/RigidBody.h"

namespace engine::physics {
	
	struct PotentialContact {
		RigidBody* bodies[2];
	};

	template<class BoundingVolumeClass>
	class BVHNode {
	public:
		
		//The two child nodes
		BVHNode* children[2];

		//The volume of the structure
		BoundingVolumeClass volume;

		//The unique rigidBody inside the Node (value is null if the node is not a leaf -> more than 1 RigidBody inside the node=
		RigidBody* body;

		//The unique parent Node
		BVHNode* parent;

		//Node constructor
		BVHNode<BoundingVolumeClass>(BVHNode* parent, BoundingVolumeClass volume, RigidBody* body);

		//Returns whether the node is a leaf or not
		bool isLeaf() const {
			return (body != nullptr);
		}

		//Checks whether this Node overlaps the other node
		bool overlaps(const std::shared_ptr<BVHNode<BoundingVolumeClass>> other) const;

		//Gets the list of potential contacts between the node's children
		unsigned getPotentialContacts(PotentialContact* contacts, unsigned limit) const;

		//Gets the potential collisions between the current node and the node given as parameter
		unsigned getPotentialContacts(const std::shared_ptr<BVHNode<BoundingVolumeClass>> other, PotentialContact* contacts, unsigned limit) const;

		//Recalculate the volume of the node
		void recalculateBoundingVolume();

		//Returns the growth of the node when encapsulating the newVolume
		real getGrowth(BoundingVolumeClass newVolume);

		//Insert a new RigidBody in the node
		void insert(std::shared_ptr<RigidBody> body, const BoundingVolumeClass& volume);

		//Remove the node and its children from the tree
		~BVHNode();
	};

	template<class BoundingVolumeClass>
	BVHNode<BoundingVolumeClass>::BVHNode(BVHNode* parent, BoundingVolumeClass volume, RigidBody* body) {
		this->parent = parent;
		this->volume = volume;
		this->body = body;
	}

	//Checks whether this Node overlaps the other node
	template<class BoundingVolumeClass>
	bool BVHNode<BoundingVolumeClass>::overlaps(const std::shared_ptr <BVHNode<BoundingVolumeClass>> other) const {
		return volume->overlaps(other->volume);
	}

	//Gets the potential collisions inside the current node
	template<class BoundingVolumeClass>
	unsigned BVHNode<BoundingVolumeClass>::getPotentialContacts(PotentialContact* contacts, unsigned limit) const {
		//If the current node is a leaf (smallest sphere containing a single RigidBody) there are no possible collisions within its children (it doesn't have any)
		if (isLeaf() || limit == 0)
			return 0;

		//If the current node has children, check the potential collisions of these children
		return children[0]->getPotentialContactsWith(children[1], contacts, limit);
	}

	//Gets the potential collisions between the current node and the node given as parameter
	template<class BoundingVolumeClass>
	unsigned BVHNode<BoundingVolumeClass>::getPotentialContacts(const std::shared_ptr <BVHNode<BoundingVolumeClass>> other, PotentialContact* contacts, unsigned limit) const {
		//If the nodes don't overlap, there can be no collision between the two or their children
		if (!overlaps(other) || limit == 0)
			return 0;

		//If the nodes overlap and both are leaves (smallest sphere containing a single RigidBody) there is a potential collision between the two RigidBodies they encapsulate
		if (isLeaf() && other->isLeaf()) {
			contacts->bodies[0] = body;
			contacts->bodies[1] = other->body;
			return 1;
		}

		//If the nodes overlap and at exactly one of them is not a leaf, get the potential collisions between the leaf node and the children of the other node
		//If the nodes overlap and neither of them is leaf, get the potential collisions between the biggest node's children and the small node
		if (other->isLeaf() || (!isLeaf() && volume->getSize() >= other->volume->getSize())) {
			unsigned count = children[0]->getPotentialContactsWith(other, contacts, limit);
			if (limit > count) {
				return count + children[1]->getPotentialContactsWith(other, contacts + count, limit - count);
			}
			else {
				return count;
			}
		}
		else {
			unsigned count = getPotentialContactsWith(other->children[0], contacts, limit);
			if (limit > count) {
				return count + getPotentialContactsWith(other->children[1], contacts + count, limit - count);
			}
			else {
				return count;
			}
		}
	}

	//Insert a new RigidBody in the node
	template<class BoundingVolumeClass>
	void BVHNode<BoundingVolumeClass>::insert(std::shared_ptr <RigidBody> newBody, const BoundingVolumeClass& newVolume) {
		//If the node is a leaf, assign it two children being his old self and the new RigidBody added. This node is no longer a leaf.
		//We also need to recalculate its new volume encapsulating both children
		if (isLeaf()) {
			children[0] = new BVHNode<BoundingVolumeClass>(this, volume, body);
			children[1] = new BVHNode<BoundingVolumeClass>(this, newVolume, newBody);
			this->body = NULL;
			recalculateBoundingVolume();
		}
		//If this node is not a leaf, insert the RigidBody in the child closest to the RigidBody's position to minimise the node's growth
		else {
			if (children[0]->volume.getGrowth(newVolume) < children[1]->volume.getGrowth(newVolume)) {
				children[0]->insert(newBody, newVolume);
			}
			else {
				children[1]->insert(newBody, newVolume);
			}
		}
	}

	//Remove the node and its children from the tree
	template<class BoundingVolumeClass>
	BVHNode<BoundingVolumeClass>::~BVHNode() {
		//If the node has a parent, identify his sibling and copy the sibling's values inside the parent.
		//This will remove the current node from the tree
		if (parent) {
			BVHNode<BoundingVolumeClass>* sibling;
			if (parent->children[0] == this) {
				sibling = parent->children[1];
			}
			else {
				sibling = parent->children[0];
			}

			parent->volume = sibling->volume;
			parent->body = sibling->body;
			parent->children[0] = sibling->children[0];
			parent->children[1] = sibling->children[1];

			sibling->parent = NULL;
			sibling->Body = NULL;
			sibling->children[0] = NULL;
			sibling->children[1] = NULL;
			delete sibling;

			parent->recalculateBoundingVolume();
		}

		//If the current node had children, delete then from the memory
		if (children[0]) {
			children[0]->parent = NULL;
			delete children[0];
		}
		if (children[1]) {
			children[1]->parent = NULL;
			delete children[1];
		}
	}

	//Recalculate the volume of the node
	template<class BoundingVolumeClass>
	void BVHNode<BoundingVolumeClass>::recalculateBoundingVolume() {
		if (!isLeaf()) {
			this->volume = new BoundingVolumeClass(children[0], children[1]);
		}
		else {
			if (children[0]) {
				this->volume = children[0]->volume;
			}
			if (children[1]) {
				this->volume = children[1]->volume;
			}
		}
	}

	//Returns the growth of the node when encapsulating the newVolume
	template<class BoundingVolumeClass>
	real BVHNode<BoundingVolumeClass>::getGrowth(BoundingVolumeClass newVolume) {
		BoundingVolumeClass bigVolume = new BoundingVolumeClass(this->volume, newVolume);
		return bigVolume->volume.getRadius() - this->volume.getRadius();
	}


	/*
	* Sphere volume that completely encapsulates an object
	* but is also the smallest possible
	*/
	struct BoundingSphere {

		//Holds the center point of the sphere
		Vector3r center;

		//Holds the radius of the sphere
		real radius;

	public:

		//Constructor of a sphere from a center and a radius
		BoundingSphere(const Vector3r& center, real radius);

		//Constructor of the smallest sphere encapsulating two spheres 
		BoundingSphere(const BoundingSphere& one, const BoundingSphere& two);

		//Checks whether two spheres overlap or not
		bool overlaps(const BoundingSphere* other) const;

		real getRadius() const;
	};

	//Constructor of a sphere from a center and a radius
	BoundingSphere::BoundingSphere(const Vector3r& center, real radius) {
		this->center = center;
		this->radius = radius;
	}

	BoundingSphere::BoundingSphere(const BoundingSphere& one, const BoundingSphere& two) {
		
		//Calculate the vector representing the difference between the position of centers of the two spheres
		Vector3r centerOffset = two.center - one.center;
		//Calculate the squared distance between the two centers of the spheres
		real distance = centerOffset.squareMagnitude();
		//Calculate the difference of the radii of the spheres
		real radiusDiff = two.radius - one.radius;

		//Check if one sphere contains the other
		if (radiusDiff * radiusDiff >= distance) {
			//If one sphere contains the other, the bigger sphere is already the smallest sphere encapsulating both spheres
			if (one.radius > two.radius) {
				center = one.center;
				radius = one.radius;
			}
			else {
				center = two.center;
				radius = two.radius;
			}
		}
		//else calculate the center and radius of the new sphere
		else {
			distance = sqrt(distance);
			radius = (distance + one.radius + two.radius) * ((real)0.5);
			center = one.center;
			if (distance > 0) {
				center = center + centerOffset * ((radius - one.radius) / distance);
			}
		}
	}

	//Checks whether two spheres overlap or not
	bool BoundingSphere::overlaps(const BoundingSphere* other) const {
		real distanceSquared = (center - other->center).squareMagnitude();
		return distanceSquared < (radius + other->radius)*(radius + other->radius);
	}

	real BoundingSphere::getRadius() const {
		return this->radius;
	}
}

