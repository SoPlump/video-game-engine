#include <vector>
#include "pcontact.h"
#include "system/Particle.h"
#include "system/physics.h"
#include "pfgen.h"

/**
* Keeps track of a set of particles, and provides the means to
* update them all.
*/
class ParticleWorld
{
public:
	typedef std::vector<std::shared_ptr<engine::physics::Particle>> Particles;

protected:
	/**
	* Holds the particles.
	*/
	Particles particles;

	unsigned numberOfContacts = 0;
public:
	/**
	* Creates a new particle simulator that can handle up to the
	* given number of contacts per frame. You can also optionally
	* give a number of contact-resolution iterations to use. If you
	* don�t give a number of iterations, then twice the number of
	* contacts will be used.
	*/
	ParticleWorld(unsigned maxContacts = 1, unsigned iterations = 0);

	/**
	* Initializes the world for a simulation frame. This clears
	* the force accumulators for particles in the world. After
	* calling this, the particles can have their forces for this
	* frame added.
	*/
	void startFrame();

	typedef std::vector<std::shared_ptr<ParticleContactGenerator>> ContactGenerators;
	/**
	* Holds the force generators for the particles in this world.
	*/
	ParticleForceRegistry registry;
	/**
	* Holds the resolver for contacts.
	*/
	ParticleContactResolver resolver;
	/**
	* Contact generators.
	*/
	ContactGenerators contactGenerators;
	/**
	* Holds the list of contacts.
	*/
	ParticleContact* contacts;
	/**
	* Holds the maximum number of contacts allowed (i.e., the
	* size of the contacts array).
	*/
	unsigned maxContacts = 40;

	/**
	* Calls each of the registered contact generators to report
	* their contacts. Returns the number of generated contacts.
	*/
	unsigned generateContacts();
	/**
	* Integrates all the particles in this world forward in time
	* 
	* * by the given duration.
	*/
	void update(double duration);
	/**
	* Processes all the physics for the particle world.
	*/
	void runPhysics(double duration);
	/*
	* Add particle to world
	*/
	void addParticle(std::shared_ptr<engine::physics::Particle> particle);

	Particles& getParticles();

	//void addContact(std::shared_ptr<ParticleContact> contact);
};
