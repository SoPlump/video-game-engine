#pragma once
//System includes

//Personal includes
#include "Contact.h"
#include "system/Primitive.h"
#include <sstream>



class CollisionData {
public:
	std::vector<std::shared_ptr<Contact>> contacts;
	int contactsRestants = 10;

	CollisionData() {
		contactsRestants = 10;
	}

	void addContacts(int nbrContacts) {
		contactsRestants -= nbrContacts;
	}

	std::string toString() {
		std::stringstream ss;
		for (auto& contact : contacts) {
			ss << contact->toString() << "\n\n";
		}
		ss << "contactsRestants " << contactsRestants << "\n";
		return ss.str();
	}
} ;

class CollisionDetector {

	public:
		//unsigned generateCollision(const engine::physics::Sphere& one, const engine::physics::Sphere& two, CollisionData* data); //sphereAndSphere
		//unsigned generateCollision(const engine::physics::Sphere& sphere, const engine::physics::Plane& plane, CollisionData* data); //sphereAndHalfSpace
		//unsigned generateCollision(const engine::physics::Sphere& sphere, const engine::physics::Plane& plane, CollisionData* data); //sphereAndTruePlane
		unsigned generateCollision(const engine::physics::Box& box, const engine::physics::Plane& plane, std::shared_ptr<CollisionData> data); //boxAndTruePlane
		//unsigned generateCollision(const engine::physics::Box& box, const engine::physics::Sphere& sphere, CollisionData* data); //boxAndSphere
};



