#pragma once

#include "Vector.h"
#include <cmath>
#include <limits>

namespace engine::physics {
    typedef float real;
    extern const real REAL_MAX;
    typedef engine::physics::Vector<real, 3> Vector3r;
#define powr powf
}
