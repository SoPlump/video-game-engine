#pragma once

//System includes

//Personal includes
#include "ecm/Component.h"
#include "physics.h"
#include "Matrix.h"
#include "Quaternion.h"
#include "components/Transform3D.h"

namespace engine::physics {
	class RigidBody : public Component {
	public:
		RigidBody(Entity* const parent);

		void addForceAtPoint(Vector3r force, Vector3r point);
		void addForceAtBodyPoint(Vector3r force, Vector3r point);
		void addForce(Vector3r force);
		void calculateDerivedData();
		void clearAccumulators();
		void integrate(real dt);
		void update(double dt) override;
		void render() const override;
		std::string toString();
		static inline void calculateTransformMatrix(Matrix4r& transformMatrix, const Vector3r& position, const Quaternion& orientation);
		static inline void transformInertiaTensor(Matrix3r& inverseInertiaTensorWorld, const Quaternion& q, const Matrix3r& inverseInertiaTensorBody, const Matrix4r& rotationMatrix);


		void setMass(real mass);
		void setInverseMass(real inverseMass);
		void setLinearDamping(real linearDamping);
		void setAngularDamping(real angularDamping);
		void setPosition(Vector3r position);
		void setVelocity(Vector3r velocity);
		void setOrientation(Quaternion orientation);
		void setAngularVelocity(Vector3r angularVelocity);
		void setTransformMatrix(Matrix4r transformMatrix);
		void setInertiaTensor(Matrix3r inertiaTensor);
		void setInverseInertiaTensor(Matrix3r inverseInertiaTensor);
		Vector3r getPointInWorldCoords(Vector3r pointInLocalCoords);

		bool hasFiniteMass();

		real getMass();
		real getInverseMass();
		real getLinearDamping();
		real getAngularDamping();
		Vector3r getPosition();
		Vector3r getVelocity();
		Quaternion getOrientation();
		Vector3r getAngularVelocity();
		Matrix4r getTransformMatrix();
		Matrix3r getInverseInertiaTensor();

	private:
		// Inverse mass of the rigidbody component
		real inverseMass;
		
		// Dampings used to remove energy added through numerical instabilities in the integrator
		real linearDamping;
		real angularDamping;

		// The position of the rigidbody in world coordinates
		Vector3r position;

		// The velocity and angular velocity of the rigidbody in world coordinates
		Vector3r velocity;
		Vector3r angularVelocity;

		// The angular orientation of the rigidbody in world coordinates
		Quaternion orientation;

		// The matrix used to convert rigidbody space into world space and the other way around
		Matrix4r transformMatrix;

		// Inertia tensor of the rigidbody in local coordinates and in world coordinates
		Matrix3r inverseInertiaTensor;
		Matrix3r inverseInertiaTensorWorld;

		// Force accumlator of the rigidbody
		Vector3r forceAccumulator;

		// Torque accumulator of the rigidbody
		Vector3r torqueAccumulator;

		// current and previous acceleration of the rigidbody
		Vector3r acceleration;
		Vector3r lastFrameAcceleration;

		//Transform
		std::shared_ptr<Transform3D> transform;
	};
}