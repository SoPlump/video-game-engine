#pragma once

#include "system/RigidBody.h"
#include "../Matrix.h"
#include "ecm/Component.h"

enum primitivesTypes {
    PLANE, BOX, SPHERE
};

namespace engine::physics {
    class Primitive : public Component {

    public:
        std::shared_ptr<RigidBody> body;
        Matrix4r offset;

        Primitive(Entity* const entity, Matrix4r offset);

        inline std::shared_ptr<RigidBody> getBody() { return body; }

        virtual void update(double dt) override = 0;

        virtual void render() const override = 0;

        void integrate(double dt);

        virtual primitivesTypes getType() = 0;

    };

    class Plane : public Primitive {
    public:

        Vector3r normal;
        float planeOffset;

        Plane(Entity* const entity, Matrix4r offset, Vector3r normal, float planeOffset);

        float distance(Vector3r point) const;

        void update(double dt) override;

        void render() const override;

        primitivesTypes getType() override;
    };

    class Box : public Primitive {
    public:
        Vector3r halfsizes; //Contient la longueur des 3 demi-c�t�s

        //Box(Entity* const entity);
        Box(Entity* const entity, Matrix4r offset, Vector3r halfsizes);

        Vector3r* generateVertices() const;

        void update(double dt) override;

        void render() const override;

        primitivesTypes getType() override;

    };

    class Sphere : public Primitive {
    public:
        real radius;

        void update(double dt) override;

        void render() const override;

        primitivesTypes getType() override;

    };
}


