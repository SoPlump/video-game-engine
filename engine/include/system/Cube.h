#pragma once

#include "physics.h"
#include "ecm/Component.h"
#include <memory>
#include "components/Transform3D.h"

namespace engine::physics {
    class Cube : public Component {
    public:
        Cube(Entity* const parent);

        Cube(Entity* const parent, real origin, real x, real y, real z);

        void update(double dt) override;
        void render() const override;

        void setMass(real mass);
        void setInverseMass(real inverseMass);

        void setPosition(Vector3r position);
        void setVelocity(Vector3r velocity);
        void setAcceleration(Vector3r acceleration);

        /**
        * Adds the given force to the particle to be applied at the
        * next iteration only.
        */
        void addForce(const Vector3r& force);

        bool hasFiniteMass();

        Vector3r getPosition();
        Vector3r getVelocity();
        Vector3r getAcceleration();
        Vector3r getForceAccum();
        real getDamping();
        real getInverseMass();
        real getMass();

        Vector3r getCentre();
        real getX();
        real getY();
        real getZ();

        void setCentre(Vector3r origin);
        void setX(real x);
        void setY(real y);
        void setZ(real z);


    private:
        Vector3r position;
        Vector3r velocity;
        Vector3r acceleration;
        Vector3r forceAccum;
        real damping;
        real inverseMass;

        // Geometric attributes
        Vector3r centre;
        real xLength;
        real yLength;
        real zLength;

        std::shared_ptr<Transform3D> transform;

        void clearAccumulator();
    };
}