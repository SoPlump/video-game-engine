#pragma once

#include "physics.h"
#include "ecm/Component.h"
#include <memory>
#include "components/Transform3D.h"

namespace engine::physics {
    class Particle : public Component {
    public:
        Particle(Entity* const parent);

        void update(double dt) override;
        void render() const override;

        void setMass(real mass);
        void setInverseMass(real inverseMass);

        void setPosition(Vector3r position);
        void setVelocity(Vector3r velocity);
        void setAcceleration(Vector3r acceleration);
        void setRadius(real radius);

        /**
        * Adds the given force to the particle to be applied at the
        * next iteration only.
        */
        void addForce(const Vector3r& force);

        bool hasFiniteMass();

        Vector3r getPosition();
        Vector3r getVelocity();
        Vector3r getAcceleration();
        Vector3r getForceAccum();
        real getDamping();
        real getInverseMass();
        real getMass();
        real getRadius();


    private:
        Vector3r position;
        Vector3r velocity;
        Vector3r acceleration;
        Vector3r forceAccum;
        real damping;
        real inverseMass;
        real radius;
        std::shared_ptr<Transform3D> transform;

        void clearAccumulator();
    };
}
