#pragma once

#include <stdexcept>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/utility/manipulators/add_value.hpp>
#include <boost/log/trivial.hpp>

typedef boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level> logger_t;

BOOST_LOG_GLOBAL_LOGGER(logger, logger_t)

#define SEVERITY_THRESHOLD boost::log::trivial::trace

// when defined, log records are colored with ansi escape sequences depending on severity
// undefine to disable colors
#define LOG_COLORS

#define EMPTY_MESSAGE ""

#define TRACE   BOOST_LOG_SEV(logger::get(), boost::log::trivial::trace)
#define DEBUG   BOOST_LOG_SEV(logger::get(), boost::log::trivial::debug)
#define INFO    BOOST_LOG_SEV(logger::get(), boost::log::trivial::info)

#define WARNING BOOST_LOG_SEV(logger::get(), boost::log::trivial::warning)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

#define ERROR   BOOST_LOG_SEV(logger::get(), boost::log::trivial::error)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

#define FATAL   BOOST_LOG_SEV(logger::get(), boost::log::trivial::fatal)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

// add opengl dedicated logger in debug build
#ifndef NDEBUG
#define GL_INFO    BOOST_LOG_SEV(logger::get(), boost::log::trivial::info)\
    << boost::log::add_value("OpenGL", true)

#define GL_WARNING BOOST_LOG_SEV(logger::get(), boost::log::trivial::warning)\
    << boost::log::add_value("OpenGL", true)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

#define GL_ERROR   BOOST_LOG_SEV(logger::get(), boost::log::trivial::error)\
    << boost::log::add_value("OpenGL", true)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

#define GL_FATAL   BOOST_LOG_SEV(logger::get(), boost::log::trivial::fatal)\
    << boost::log::add_value("OpenGL", true)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)
#endif