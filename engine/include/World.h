#pragma once

//System includes
#include <vector>

//Personal includes
#include "pcontact.h"
#include "system/RigidBody.h"
#include "system/physics.h"
#include "fgen.h"
#include "Object.h"

#include "NarrowCollide.h"

/**
* Keeps track of a set of particles, and provides the means to
* update them all.
*/
class World
{
public:
	std::shared_ptr<CollisionData> collisionData;
	typedef std::vector<std::shared_ptr<Object>> Objects;
	std::shared_ptr < CollisionDetector> collisionDetector;

protected:
	/**
	* Holds the particles.
	*/
	Objects objects;

	unsigned numberOfContacts = 0;
public:
	/**
	* Creates a new particle simulator that can handle up to the
	* given number of contacts per frame. You can also optionally
	* give a number of contact-resolution iterations to use. If you
	* don�t give a number of iterations, then twice the number of
	* contacts will be used.
	*/
	World(unsigned maxContacts = 1, unsigned iterations = 0);

	/**
	* Initializes the world for a simulation frame. This clears
	* the force accumulators for particles in the world. After
	* calling this, the particles can have their forces for this
	* frame added.
	*/
	void startFrame();

	/**
	* Holds the force generators for the particles in this world.
	*/
	ForceRegistry registry;

	void update(double dt);

	/**
	* Processes all the physics for the rigidbodies in the world.
	*/
	void runPhysics(double dt);

	void integrate(double dt);

	void addObject(std::shared_ptr<Object> object);

	void generateContacts();
};
