#pragma once

#include <system/physics.h>
#include "system/Particle.h"

/**
* A contact represents two objects in contact (in this case
* ParticleContact representing two particles). Resolving a
* contact removes their interpenetration, and applies sufficient
* impulse to keep them apart. Colliding bodies may also rebound.
*
* The contact has no callable functions, it just holds the
* contact details. To resolve a set of contacts, use the particle
* contact resolver class.
*/
class ParticleContact {

	friend class ParticleContactResolver;
public:
	ParticleContact() {}

	ParticleContact(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2,
		engine::physics::real restitution);
	/**
	* Holds the particles that are involved in the contact. The
	* second of these can be NULL for contacts with the scenery.
	*/
	std::shared_ptr<engine::physics::Particle> particles[2];
	/**
	* Holds the normal restitution coefficient at the contact.
	*/
	engine::physics::real restitution;
	/**
	* Holds the direction of the contact in world coordinates.
	*/
	engine::physics::Vector3r contactNormal;

	/**
	* Holds the depth of penetration at the contact
	*/
	engine::physics::real penetration = 0.0f;

protected:
	/**
	* Resolves this contact for both velocity and interpenetration.
	*/
	void resolve(engine::physics::real duration);
	/**
	* Calculates the separating velocity at this contact.
	*/
	engine::physics::real calculateSeparatingVelocity() const;

private:
	/**
	* Handles the impulse calculations for this collision.
	*/
	void resolveVelocity(engine::physics::real duration);

	/**
	* Handles the interpenetration resolution for this contact.
	*/
	void resolveInterpenetration(engine::physics::real duration);

	/**
	* Returns the total inverseMass of present particles.
	*/
	engine::physics::real getTotalInverseMass() const;
};

/**
* The contact resolution routine for particle contacts. One
* resolver instance can be shared for the entire simulation.
*/
class ParticleContactResolver {
public:
	/**
	* Creates a new contact resolver.
	*/
	ParticleContactResolver(unsigned iterations = 1);

	/**
	* Sets the number of iterations that can be used.
	*/
	void setIterations(unsigned iterations);
	/**
	* Resolves a set of particle contacts for both penetration
	* and velocity.
	*/
	void resolveContacts(ParticleContact* contactArray,
		unsigned numContacts,
		engine::physics::real duration);

protected:
	/**
	* Holds the number of iterations allowed.
	*/
	unsigned iterations;
	/**
	* This is a performance tracking value; we keep a record
	* of the actual number of iterations used.
	*/
	unsigned iterationsUsed = 0;
};

/**
* This is the basic polymorphic interface for contact generators
* applying to particles.
*/
class ParticleContactGenerator
{
public:
	ParticleContactGenerator() {}
	/**
	* Fills the given contact structure with the generated
	* contact. The contact pointer should point to the first
	* available contact in a contact array, where limit is the
	* maximum number of contacts in the array that can be written
	* to. The method returns the number of contacts that have
	* been written.
	*/
	virtual unsigned addContact(ParticleContact* contact,
		unsigned limit) const = 0;
};
