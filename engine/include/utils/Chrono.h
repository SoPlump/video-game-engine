#pragma once

#include <chrono>

/**
 * A class used to keep track of elapsed time since its creation
 */
class Chrono {
public:

    /**
     * Creates a new Chrono using the current time as starting time.
     */
    Chrono();

    /**
     * Return the number of seconds since the Chrono's starting time
     * @return the number of seconds since the Chrono's starting time
     */
    double time() const;

    /**
     * Return the number of seconds since the last call to this method
     * @return the number of seconds since the last call to this method
     */
    double delta();

    /**
     * Resets both the starting time and previous time of the Chrono to the current time.
     */
    void reset();

private:
    std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> start;
    std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> previous;
};
