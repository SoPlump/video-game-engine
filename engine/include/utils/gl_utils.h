#pragma once

#include <iostream>
#include <string>
#include <GL/glew.h>

/**
 * Convert GLenum values to their string representation.
 * Unsupported GLenum values will be converted to a default string
 * @param glEnum the GLenum to convert
 * @return the string representation of the given GLenum
 */
std::string glEnumToString(GLenum glEnum);

/**
 * Loads and compiles a shader from a file
 * @param path the path of the file to read the shader from
 * @param type the type of shader to create
 * (GL_VERTEX_SHADER, GL_FRAGMENT_SHADER ...)
 * @return a non-zero shader id
 */
GLuint loadShaderFromFile(const std::string& path, GLenum type);

/**
 * Creates a shader program from a vertex and fragment shader
 * @param vertexShader the id of the vertex shader
 * @param fragmentShader the id of the fragment shader
 * @return a non-zero program id
 */
GLuint createProgram(GLuint vertexShader, GLuint fragmentShader);

/**
 * Loads a texture from a file into GPU memory (the texture is not kept in main memory)
 * @param path the path of the file to read the texture from
 * @param type the type of texture to create
 * (GL_TEXTURE_1D, GL_TEXTURE_2D, GL_TEXTURE_3D, ...)
 * @param mipmapLevel the mipmap level of the texture, use -1 to automatically generate mipmap levels
 * @return
 */
GLuint loadTextureFromFile(const std::string& path, GLenum type, int mipmapLevel = -1);
