#pragma once

#include "renderer/Mesh.h"
#include <algorithm>
#include <cctype>
#include <memory>
#include <vector>
#include <string>
#include <Vector.h>
#include <Matrix.h>

/**
 * Trims leading whitespaces from the given string.
 * @param s the string to modify
 */
inline void ltrim(std::string& s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int c) {
        return !std::isspace(c);
    }));
}

/**
 * Trims the trailing whitespaces from the given string.
 * @param s the string to modify
 */
inline void rtrim(std::string& s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int c) {
        return !std::isspace(c);
    }).base(), s.end());
}

/**
 * Trims the leading and trailing whitespaces from the given string
 * @param s the string to modify
 */
inline void trim(std::string& s) {
    ltrim(s);
    rtrim(s);
}

/**
 * Splits the given string on each occurrence of the delimiter
 * @param s the string to split
 * @param delimiter the delimiter on which the split is to be perform
 * @return A vector containing the split string.
 * <ul>
 * </ul>
 */
std::vector<std::string> split(std::string& s, char delimiter);

/**
 * Reads an entire file into an string.
 * Bigger file should be handled differently.
 *
 * @param path the path of the file to read
 * @return The content of the file as a string.
 * The string is dynamically allocated and must be freed
 * to avoid memory leaks.
 * @throws std::ios_base::failure
 */
std::shared_ptr<std::string> readFile(const std::string& path);

//TODO :ici ?
/*
* Transform a given world vector to a local vector
*/
engine::physics::Vector3r localToWorld(const engine::physics::Vector3r& local, const engine::physics::Matrix4r& transformMatrix);

/*
* Transform a given local vector to a world vector
*/
engine::physics::Vector3r worldToLocal(const engine::physics::Vector3r& world, const engine::physics::Matrix4r& transformMatrix);