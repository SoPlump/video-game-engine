#pragma once

#include <exception>
#include <GL/glew.h>

class ShaderLinkError : public std::exception {
public:
    const GLuint program;

    explicit ShaderLinkError(GLuint program);
    ~ShaderLinkError() override;
    const char* what() const noexcept override;
private:
    char* message;
};
