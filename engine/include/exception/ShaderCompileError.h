#pragma once

#include <exception>
#include <GL/glew.h>

class ShaderCompileError : public std::exception {
public:
    const GLuint shader;

    explicit ShaderCompileError(GLuint shader);
    ~ShaderCompileError() override;
    const char* what() const noexcept override;
private:
    char* message;
};
