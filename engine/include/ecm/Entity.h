#pragma once

#include <memory>
#include <stdexcept>
#include <vector>

class Component;

class Entity {
public:
    Entity();
    virtual ~Entity();

    /**
    * Deletes components that are flagged for deletion and updates those that
    * are alive.
    *
    * dt should be the time elapsed since the last update call.
    */
    virtual void update(double dt);
    /**
    * Calls the render function of all visible components
    *
    */
    virtual void render() const;

    // Getters and setters for the three boolean flags.
    bool isAlive() const;
    void setAlive(bool alive);

    bool isVisible() const;
    void setVisible(bool visible);

    bool isForDeletion() const;
    void setForDeletion();

    // Adds a component to the Component vector of this entity
    template<typename T, typename... Targs>
    std::shared_ptr<T> addComponent(Targs... params) {
        static_assert(std::is_base_of<Component, T>::value, "Given type is not a Component");
        std::shared_ptr<T> component = std::make_shared<T>(this, params...);
        components.push_back(component);
        return component;
    }

    // Returns a Component of a given type (passed in parameter) if the entity does contain one of this component.
    template<typename T>
    std::shared_ptr<T> getComponent() {
        static_assert(std::is_base_of<Component, T>::value, "Given type is not a Component");
        for(const auto& component: components) {
            std::shared_ptr<T> castPtr = std::dynamic_pointer_cast<T>(component);
            if(castPtr && castPtr.use_count() != 0)
                return castPtr;
        }
        throw std::runtime_error("This Entity has no Component matching the required type");
    }

    // Returns the list of components contained in this entity.
    template<typename T>
    std::vector<std::shared_ptr<T>> getAllComponents() {
        static_assert(std::is_base_of<Component, T>::value, "Given type is not a Component");
        std::vector<std::shared_ptr<T>> components;
        for(const auto& component : this->components) {
            std::shared_ptr<T> castPtr = std::dynamic_pointer_cast<T>(component);
            if(castPtr && castPtr.use_count() != 0)
                components.push_back(castPtr);
        }
        if(components.empty())
            throw std::runtime_error("This Entity has no Component matching the required type");
        return components;
    }

protected:
    // Vector of Components contained in this entity.
    std::vector<std::shared_ptr<Component>> components;
    // If alive is true, update will be callable.
    bool alive;
    // If visible is true, render will be callable.
    bool visible;
    // If forDeletion is true, this entity will be destroyed.
    bool forDeletion;
};
