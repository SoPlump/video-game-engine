#pragma once

#include "Entity.h"

class Component {
public:
    Component() = delete;
    virtual ~Component();

    /**
    * Launches the different actions that affects the state of the component.
    * Called by the parent entity.
    *
    * dt should be the time elapsed since the last call to update
    */
    virtual void update(double dt) = 0;

    /**
    * Launches the different actions that will end up in a graphical update.
    * Called by the parent entity.
    *
    */
    virtual void render() const = 0;

    // Getters and setters for the three boolean attributes.
    bool isAlive() const;
    void setAlive(bool alive);

    bool isVisible() const;
    void setVisible(bool visible);

    bool isForDeletion() const;
    void setForDeletion();

protected:
    // The entity this component is from
    Entity* const parent;
    // If alive is true, the entity will call the update of this component.
    bool alive;
    // If visible is true, the entity will call the render of this component.
    bool visible;
    // If forDeletion is true, the entity will delete this component.
    bool forDeletion;

    explicit Component(Entity* const parent);
};


