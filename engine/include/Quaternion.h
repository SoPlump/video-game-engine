#pragma once

#include <system/physics.h>

namespace engine::physics {

	/**
	* Holds a three-degrees-of-freedom orientation + axe
	* Quaternion = [scalar (v1, v2, v3)]
	*/
	class Quaternion
	{
	public:
		real scalar;

		Vector3r axis;

		// Methods:

		Quaternion();

		Quaternion(real, Vector3r);

		void normalize();

		void rotateByVector(const Vector3r&);

		void addScaledVector(const Vector3r& vector, real delta);

		void operator *=(const Quaternion& multiplier);

		bool operator ==(const Quaternion& q);
	};

}