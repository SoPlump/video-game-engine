#pragma once

#include <Vector.h>
#include<algorithm>
#include <system/physics.h>
#include <Quaternion.h>

namespace engine::physics {

	template<typename T, size_t col, size_t lign> struct Matrix;

	// Matrix 4

	template<typename T>
	struct Matrix<T, 4, 3>
	{
	private:

	public:
		T* values;

		Matrix() {
			values = new T[12]{ 1,0,0,0,0,1,0,0,0,0,1,0 };
		}

		Matrix(const Matrix<T, 4, 3>& mat) {
			values = new T[12];
			for (int i = 0; i < 12; ++i) {
				values[i] = mat.values[i];
			}
		}

		~Matrix() {
			delete[] values;
		}

		//methods

		T*& GetValues() {
			return values;
		}

		/**
		* Transform the given vector by this matrix
		*/
		Vector<T, 3> transform(const Vector<T, 3>& vec) const {
			return (*this) * vec;
		}

		/*
		* Returns the determinant of the matrix
		*/
		T getDeterminant() const {
			return	values[8] * values[5] * values[2] +
				values[4] * values[9] * values[2] +
				values[8] * values[1] * values[6] -
				values[0] * values[9] * values[6] -
				values[4] * values[1] * values[10] +
				values[0] * values[5] * values[10];
		}

		/**
		* Sets the matrix to be the inverse of the given matrix
		*/
		void invert() {
			Matrix<T, 4, 3> mat(*this);
			// Make sure the determinant is non-zero
			T det = getDeterminant();
			if (det == 0) return;
			det = ((T)1.0f) / det;

			values[0] = (-mat.values[9] * mat.values[6] + mat.values[5] * mat.values[10]) * det;
			values[1] = (mat.values[9] * mat.values[2] - mat.values[1] * mat.values[10]) * det;
			values[2] = (-mat.values[5] * mat.values[2] + mat.values[1] * mat.values[6]) * det;
			values[3] = (mat.values[9] * mat.values[6] * mat.values[3]
				- mat.values[5] * mat.values[10] * mat.values[3]
				- mat.values[9] * mat.values[2] * mat.values[7]
				+ mat.values[1] * mat.values[10] * mat.values[7]
				+ mat.values[5] * mat.values[2] * mat.values[11]
				- mat.values[1] * mat.values[6] * mat.values[11]) * det;
			values[4] = (mat.values[8] * mat.values[6] - mat.values[4] * mat.values[10]) * det;
			values[5] = (-mat.values[8] * mat.values[2] + mat.values[0] * mat.values[10]) * det;
			values[6] = (mat.values[4] * mat.values[2] - mat.values[0] * mat.values[6]) * det;
			values[7] = (-mat.values[8] * mat.values[6] * mat.values[3]
				+ mat.values[4] * mat.values[10] * mat.values[3]
				+ mat.values[8] * mat.values[2] * mat.values[7]
				- mat.values[0] * mat.values[10] * mat.values[7]
				- mat.values[4] * mat.values[2] * mat.values[11]
				+ mat.values[0] * mat.values[6] * mat.values[11]) * det;
			values[8] = (-mat.values[8] * mat.values[5] + mat.values[4] * mat.values[9]) * det;
			values[9] = (mat.values[8] * mat.values[1] - mat.values[0] * mat.values[9]) * det;
			values[10] = (-mat.values[4] * mat.values[1] + mat.values[0] * mat.values[5]) * det;
			values[11] = (mat.values[8] * mat.values[5] * mat.values[3]
				- mat.values[4] * mat.values[9] * mat.values[3]
				- mat.values[8] * mat.values[1] * mat.values[7]
				+ mat.values[0] * mat.values[9] * mat.values[7]
				+ mat.values[4] * mat.values[1] * mat.values[11]
				- mat.values[0] * mat.values[5] * mat.values[11]) * det;
		}

		/**
		* Returns a new matrix containing the inverse of this matrix
		*/
		Matrix<T, 4, 3> inverse() const {
			Matrix<T, 4, 3> result(*this);
			result.invert();
			return result;
		}
		/**
		* Sets this matrix to be the rotation matrix corresponding to
		* the given quaternion.
		*/
		void setOrientationAndPos(const Quaternion& q, const Vector<T, 3>& pos)
		{
			values[0] = 1 - (2 * q.axis.j * q.axis.j + 2 * q.axis.k * q.axis.k);
			values[1] = 2 * q.axis.i * q.axis.j + 2 * q.axis.k * q.scalar;
			values[2] = 2 * q.axis.i * q.axis.k - 2 * q.axis.j * q.scalar;
			values[3] = pos.x;
			values[4] = 2 * q.axis.i * q.axis.j - 2 * q.axis.k * q.scalar;
			values[5] = 1 - (2 * q.axis.i * q.axis.i + 2 * q.axis.k * q.axis.k);
			values[6] = 2 * q.axis.j * q.axis.k + 2 * q.axis.i * q.scalar;
			values[7] = pos.y;
			values[8] = 2 * q.axis.i * q.axis.k + 2 * q.axis.j * q.scalar; 
			values[9] = 2 * q.axis.j * q.axis.k - 2 * q.axis.i * q.scalar;
			values[10] = 1 - (2 * q.axis.i * q.axis.i + 2 * q.axis.j * q.axis.j);
			values[11] = pos.z;
		}

		/* Operators */

		/**
		* Multiply a matrix by a vector
		*/
		Vector<T, 3> operator * (const Vector<T, 3>& vec) const {
			return Vector<T, 3>(
				vec.x * values[0] + vec.y * values[1] + vec.z * values[2] + values[3],
				vec.x * values[4] + vec.y * values[5] + vec.z * values[6] + values[7],
				vec.x * values[8] + vec.y * values[9] + vec.z * values[10] + values[11]
				);
		}

		/**
		* Returns a matrix, which is the one multiplied by the other given matrix
		*/
		Matrix<T, 4, 3> operator *(const Matrix<T, 4, 3>& mat) const {
			Matrix<T, 4, 3> result;
			result[0] = values[0] * mat.values[0] + values[1] * mat.values[4] + values[2] * mat.values[8] + values[3];
			result[1] = values[0] * mat.values[1] + values[1] * mat.values[5] + values[2] * mat.values[9] + values[3];
			result[2] = values[0] * mat.values[2] + values[1] * mat.values[6] + values[2] * mat.values[10] + values[3];
			result[3] = values[0] * mat.values[3] + values[1] * mat.values[7] + values[2] * mat.values[11] + values[3];
			result[4] = values[4] * mat.values[0] + values[5] * mat.values[4] + values[6] * mat.values[8] + values[7];
			result[5] = values[4] * mat.values[1] + values[5] * mat.values[5] + values[6] * mat.values[9] + values[7];
			result[6] = values[4] * mat.values[2] + values[5] * mat.values[6] + values[6] * mat.values[10] + values[7];
			result[7] = values[4] * mat.values[3] + values[5] * mat.values[7] + values[6] * mat.values[11] + values[7];
			result[8] = values[8] * mat.values[0] + values[9] * mat.values[4] + values[7] * mat.values[8] + values[11];
			result[9] = values[8] * mat.values[1] + values[9] * mat.values[5] + values[7] * mat.values[9] + values[11];
			result[10] = values[8] * mat.values[2] + values[9] * mat.values[6] + values[7] * mat.values[10] + values[11];
			result[11] = values[8] * mat.values[3] + values[9] * mat.values[7] + values[7] * mat.values[11] + values[11];

			return result;
		}

		bool operator == (const Matrix<T, 4, 3>& mat2) const {
			for (int i = 0; i < 12; ++i) {
				if (values[i] != mat2.values[i]) {
					return false;
				}
			}
			return true;
		}

		bool operator != (const Matrix<T, 4, 3>& mat2) const {
			for (int i = 0; i < 12; ++i) {
				if (values[i] != mat2.values[i]) {
					return true;
				}
			}
			return false;
		}

		/*
		* Return a given row
		*/
		T* operator[] (int index) {
			assert(index >= 0);
			assert(index < 3);
			return &values[index * 4];
		}

		const T* operator[] (int index) const {
			assert(index >= 0);
			assert(index < 3);
			return &values[index * 4];
		}

		/**
		*
		*/
	};

	typedef Matrix<real, 4, 3> Matrix4r;

	// Matrix 3

	template<typename T>
	struct Matrix<T, 3, 3>
	{
	private:

	public:
		T* values;

		Matrix() {
			values = new T[9]{ 1,0,0,0,1,0,0,0,1 };
		}

		Matrix(const Matrix<T, 3, 3>& mat) {
			values = new T[9];
			for (int i = 0; i < 9; ++i) {
				values[i] = mat.values[i];
			}
		}

		~Matrix() {
			delete[] values;
		}

		//methods

		T*& GetValues() {
			return values;
		}

		/**
		* Transform the given vector by this matrix
		*/
		Vector<T, 3> transform(const Vector<T, 3>& vec) const {
			return (*this) * vec;
		}

		/**
		* Sets the matrix to its inverse.
		*/
		void invert() {
			Matrix<T, 3, 3> mat(*this);
			real t1 = mat.values[0] * mat.values[4];
			real t2 = mat.values[0] * mat.values[5];
			real t3 = mat.values[1] * mat.values[3];
			real t4 = mat.values[2] * mat.values[3];
			real t5 = mat.values[1] * mat.values[6];
			real t6 = mat.values[2] * mat.values[6];

			// Calculate the determinant.
			real det = (t1 * mat.values[8] - t2 * mat.values[7] - t3 * mat.values[8] +
				t4 * mat.values[7] + t5 * mat.values[5] - t6 * mat.values[4]);

			// Make sure the determinant is non-zero.
			if (det == (T)0)
				return;

			real invd = (real)1.0f / det;

			values[0] = (mat.values[4] * mat.values[8] - mat.values[5] * mat.values[7]) * invd;
			values[1] = -(mat.values[1] * mat.values[8] - mat.values[2] * mat.values[7]) * invd;
			values[2] = (mat.values[1] * mat.values[5] - mat.values[2] * mat.values[4]) * invd;
			values[3] = -(mat.values[3] * mat.values[8] - mat.values[5] * mat.values[6]) * invd;
			values[4] = (mat.values[0] * mat.values[8] - t6) * invd;
			values[5] = -(t2 - t4) * invd;
			values[6] = (mat.values[3] * mat.values[7] - mat.values[4] * mat.values[6]) * invd;
			values[7] = -(mat.values[0] * mat.values[7] - t5) * invd;
			values[8] = (t1 - t3) * invd;
		}

		/** Returns a new matrix containing the inverse of this matrix. */
		Matrix<T, 3, 3> inverse() const {
			Matrix<T, 3, 3> result(*this);
			result.invert();
			return result;
		}


		/**
		* Sets this matrix to be the rotation matrix corresponding to
		* the given quaternion.
		*/
		// /!\ column major
		void setOrientation(const Quaternion& q)
		{
			values[0] = 1 - (2 * q.axis.j * q.axis.j + 2 * q.axis.k * q.axis.k);
			values[1] = 2 * q.axis.i * q.axis.j + 2 * q.axis.k * q.scalar;
			values[2] = 2 * q.axis.i * q.axis.k - 2 * q.axis.j * q.scalar;
			values[3] = 2 * q.axis.i * q.axis.j - 2 * q.axis.k * q.scalar;
			values[4] = 1 - (2 * q.axis.i * q.axis.i + 2 * q.axis.k * q.axis.k);
			values[5] = 2 * q.axis.j * q.axis.k + 2 * q.axis.i * q.scalar;
			values[6] = 2 * q.axis.i * q.axis.k + 2 * q.axis.j * q.scalar;
			values[7] = 2 * q.axis.j * q.axis.k - 2 * q.axis.i * q.scalar;
			values[8] = 1 - (2 * q.axis.i * q.axis.i + 2 * q.axis.j * q.axis.j);
		}

		/* Operators */

		/**
		* Transform the given vector by this matrix.
		*/
		Vector < T, 3> operator*(const Vector < T, 3 >& vec) const
		{
			return Vector < T, 3>(
				vec.x * values[0] + vec.y * values[1] + vec.z * values[2],
				vec.x * values[3] + vec.y * values[4] + vec.z * values[5],
				vec.x * values[6] + vec.y * values[7] + vec.z * values[8]
				);
		}

		/**
		* Returns a matrix, which is this one multiplied by the other given matrix.
		*/
		Matrix<T, 3, 3> operator*(const Matrix<T, 3, 3>& mat) const
		{
			Matrix<T, 3, 3> result;
			result[0] = values[0] * mat.values[0] + values[1] * mat.values[3] + values[2] * mat.values[6];
			result[1] = values[0] * mat.values[1] + values[1] * mat.values[4] + values[2] * mat.values[7];
			result[2] = values[0] * mat.values[2] + values[1] * mat.values[5] + values[2] * mat.values[8];
			result[3] = values[3] * mat.values[0] + values[4] * mat.values[3] + values[5] * mat.values[6];
			result[4] = values[3] * mat.values[1] + values[4] * mat.values[4] + values[5] * mat.values[7];
			result[5] = values[3] * mat.values[2] + values[4] * mat.values[5] + values[5] * mat.values[8];
			result[6] = values[6] * mat.values[0] + values[7] * mat.values[3] + values[8] * mat.values[6];
			result[7] = values[6] * mat.values[1] + values[7] * mat.values[4] + values[8] * mat.values[7];
			result[8] = values[6] * mat.values[2] + values[7] * mat.values[5] + values[8] * mat.values[8];

			return result;
		}

		/**
		* Multiplies this matrix in place by the other given matrix.
		*/
		void operator*=(const Matrix<T, 3, 3>& mat) {
			real t1, t2, t3;
			t1 = values[0] * mat.values[0] + values[1] * mat.values[3] + values[2] * mat.values[6];
			t2 = values[0] * mat.values[1] + values[1] * mat.values[4] + values[2] * mat.values[7];
			t3 = values[0] * mat.values[2] + values[1] * mat.values[5] + values[2] * mat.values[8];
			values[0] = t1;
			values[1] = t2;
			values[2] = t3;
			t1 = values[3] * mat.values[0] + values[4] * mat.values[3] + values[5] * mat.values[6];
			t2 = values[3] * mat.values[1] + values[4] * mat.values[4] + values[5] * mat.values[7];
			t3 = values[3] * mat.values[2] + values[4] * mat.values[5] + values[5] * mat.values[8];
			values[3] = t1;
			values[4] = t2;
			values[5] = t3;
			t1 = values[6] * mat.values[0] + values[7] * mat.values[3] + values[8] * mat.values[6];
			t2 = values[6] * mat.values[1] + values[7] * mat.values[4] + values[8] * mat.values[7];
			t3 = values[6] * mat.values[2] + values[7] * mat.values[5] + values[8] * mat.values[8];
			values[6] = t1;
			values[7] = t2;
			values[8] = t3;
		}


		bool operator == (const Matrix<T, 3, 3>& mat2) const {
			for (int i = 0; i < 9; ++i) {
				if (values[i] != mat2.values[i]) {
					return false;
				}
			}
			return true;
		}

		bool operator != (const Matrix<T, 3, 3>& mat2) const {
			for (int i = 0; i < 9; ++i) {
				if (values[i] != mat2.values[i]) {
					return true;
				}
			}
			return false;
		}

		/*
		* Return a given column /!\
		*/
		T* operator[] (int index) {
			assert(index >= 0);
			assert(index < 3);
			return &values[index * 3];
		}

		const T* operator[] (int index) const {
			assert(index >= 0);
			assert(index < 3);
			return &values[index * 3];
		}

		/**
		*
		*/
	};

	typedef Matrix<real, 3, 3> Matrix3r;
}
