#pragma once

#include <system/physics.h>
#include "pcontact.h"
#include "system/Particle.h"

/**
* Links connect two particles together, generating a contact if
* they violate the constraints of their link. It is used as a
* base class for cables and rods, and could be used as a base
* class for springs with a limit to their extension.
*/
class ParticleLink : public ParticleContactGenerator {
public:
	ParticleLink(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2);
	/**
	* Holds the pair of particles that are connected by this link.
	*/
	std::shared_ptr<engine::physics::Particle> particles[2];

	/**
	* Generates the contacts to keep this link from being
	* violated. This class can only ever generate a single
	* contact, so the pointer can be a pointer to a single
	* element, the limit parameter is assumed to be at least 1
	* (0 isn�t valid), and the return value is 0 if the
	* cable wasn�t overextended, or 1 if a contact was needed.
	*
	* NB: This method is declared in the same way (as pure
	* virtual) in the parent class, but is replicated here for
	* documentation purposes.
	*/
	virtual unsigned addContact(ParticleContact* contact,
			unsigned limit) const = 0;

protected:
	/**
	* Returns the current length of the link.
	*/
	engine::physics::real currentLength() const;
};

/**
* Cables link a pair of particles, generating a contact if they
* stray too far apart.
*/
class ParticleCable : public ParticleLink {
public:
	ParticleCable(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2,
		engine::physics::real maxLength, 
		engine::physics::real restitution);
	/**
	* Holds the maximum length of the cable.
	*/
	engine::physics::real maxLength;

	/**
	* Holds the restitution (bounciness) of the cable.
	*/
	engine::physics::real restitution;

	/**
	* Fills the given contact structure with the contact needed
	* to keep the cable from overextending.
	*/
	virtual unsigned addContact(ParticleContact* contact,
		unsigned limit) const;
};



/**
* Rods link a pair of particles, generating a contact if they
* stray too far apart or too close.
*/
class ParticleRod : public ParticleLink {
public:
	ParticleRod(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2,
		engine::physics::real length);
	/**
	* Holds the length of the rod.
	*/
	engine::physics::real length;

	/**
	* Fills the given contact structure with the contact needed
	* to keep the rod from extending or compressing.
	*/
	virtual unsigned addContact(ParticleContact* contact,
	unsigned limit) const;
};

class ParticleFloor : public ParticleLink {
public:
	ParticleFloor(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2, 
		engine::physics::real restitution = 1.0f);
	/**
	* Holds the restitution (bounciness) of the cable.
	*/
	engine::physics::real restitution;

	/**
	* Fills the given contact structure with the contact needed
	* to keep the cable from overextending.
	*/
	virtual unsigned addContact(ParticleContact* contact,
		unsigned limit = 0) const;
};

class ParticleLeftWall : public ParticleLink {
public:
	ParticleLeftWall(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2,
		engine::physics::real restitution = 1.0f);
	/**
	* Holds the restitution (bounciness) of the cable.
	*/
	engine::physics::real restitution;

	/**
	* Fills the given contact structure with the contact needed
	* to keep the cable from overextending.
	*/
	virtual unsigned addContact(ParticleContact* contact,
		unsigned limit = 0) const;
};

class ParticleRightWall : public ParticleLink {
public:
	ParticleRightWall(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2,
		engine::physics::real restitution = 1.0f);
	/**
	* Holds the restitution (bounciness) of the cable.
	*/
	engine::physics::real restitution;

	/**
	* Fills the given contact structure with the contact needed
	* to keep the cable from overextending.
	*/
	virtual unsigned addContact(ParticleContact* contact,
		unsigned limit = 0) const;
};

class ParticleSphere : public ParticleLink {
public:
	ParticleSphere(
		std::shared_ptr<engine::physics::Particle> particle1,
		std::shared_ptr<engine::physics::Particle> particle2,
		engine::physics::real restitution = 0.8f);
	/**
	* Holds the restitution (bounciness) of the other particle.
	*/
	engine::physics::real restitution;

	/**
	* Fills the given contact structure with the contact needed
	* to keep particles from going through one another.
	*/
	virtual unsigned addContact(ParticleContact* contact,
		unsigned limit = 0) const;
};