#pragma once

#include <glm/glm.hpp>
#include <SDL2/SDL.h>

namespace engine::system {
    /**
     * Init SDL, GLEW and OpenGL
     * @return true if init was successful
     */
    bool init();

    /**
     * Properly shutdown everything
     */
    void shutdown();

    /**
     * @return pointer to SDL_Window object,
     * null if init was not called,
     * don't delete
     */
    SDL_Window* window();

    /**
     * @return window width
     */
    int width();

    /**
     * @return window height
     */
    int height();

    /**
     * @return window aspect ratio
     */
    float aspectRatio();

    /**
     * process and handle SDL events
     * @return true if we can proceed,
     * false if the user requested to close the window (in which case we should quickly stop exit the program)
     */
    bool handleEvents();

    /**
     * @return mouse position relative to the window
     */
    const glm::i32vec2& getMousePosition();

    /**
     * @return mouse X position relative to the window
     */
    int getMouseX();

    /**
     * @return mouse Y position relative to the window
     */
    int getMouseY();

    /**
     * @return mouse relative position since last known position
     */
    const glm::i32vec2& getMouseRelative();

    /**
     * @return mouse relative X position since last known position
     */
    int getMouseRelativeX();

    /**
     * @return mouse relative Y position since last known position
     */
    int getMouseRelativeY();

    /**
     * @return true if the given key is pressed, false otherwise
     */
    bool isKeyPressed(SDL_Keycode keyCode);
}
