#include "system/Wall.h"
#include "system/physics.h"
#include <cassert>
#include <logging.h>
#include <system.h>
#include <components/Transform3D.h>
#include <components/MeshRenderer.h>
#include <renderer/Shader.h>
#include <renderer/Mesh.h>
#include <exception/ShaderLinkError.h>
#include <exception/ShaderCompileError.h>

using namespace engine::physics;
using namespace engine;
using namespace std;

Wall::Wall(Entity* const parent) : Component(parent) {
    transform = parent->getComponent<Transform3D>();
}

void Wall::update(double dt) {
}

void Wall::render() const {

}

void Wall::setMass(real mass) {
}

void Wall::setInverseMass(real inverseMass) {
    this->inverseMass = inverseMass;
}

void Wall::setPosition(Vector3r position) {
    this->position = position;
}

void Wall::setVelocity(Vector3r velocity) {
    this->velocity = velocity;
}

void Wall::setAcceleration(Vector3r acceleration) {
    this->acceleration = acceleration;
}

void Wall::clearAccumulator() {
    this->forceAccum.clear();
}

void Wall::addForce(const Vector3r& force) {
    this->forceAccum.addScaledVector(force, 1);
}

bool Wall::hasFiniteMass() {
    if (inverseMass != 0)return true;
    return false;
}

Vector3r Wall::getPosition() {
    return this->position;
}

Vector3r Wall::getVelocity() {
    return this->velocity;
}

Vector3r Wall::getAcceleration() {
    return this->acceleration;
}

Vector3r Wall::getForceAccum() {
    return this->forceAccum;
}

real Wall::getDamping() {
    return this->damping;
}

real Wall::getInverseMass() {
    return this->inverseMass;
}

real Wall::getMass() {
    if (hasFiniteMass()) return (1 / inverseMass);
    return -1;
}