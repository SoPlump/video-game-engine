#include "system/Cube.h"
#include "system/physics.h"
#include <cassert>
#include <logging.h>
#include <system.h>
#include <components/Transform3D.h>
#include <components/MeshRenderer.h>
#include <renderer/Shader.h>
#include <renderer/Mesh.h>
#include <exception/ShaderLinkError.h>
#include <exception/ShaderCompileError.h>
#include "Vector.h"

using namespace engine::physics;
using namespace engine;
using namespace std;

Cube::Cube(Entity* const parent) : Component(parent) {
    transform = parent->getComponent<Transform3D>();
    damping = 1.0f;

    centre = Vector3r(0.0f, 0.0f, 0.0f);

    xLength = 1.0f;
    yLength = 1.0f;
    zLength = 1.0f;
}

void Cube::update(double dt) {
    // ignore infinite mass
    if (inverseMass <= 0.0f)
        return;

    assert(dt > 0.0);

    // Update linear position
    position.addScaledVector(velocity, dt);

    // Work out the acceleration from the force
    Vector3r newAcceleration = acceleration;
    newAcceleration.addScaledVector(forceAccum, inverseMass);

    // Update linear velocity from the acceleration
    velocity.addScaledVector(newAcceleration, dt);

    // Impose drag
    velocity *= powr(damping, dt);

    // Clear the forces
    clearAccumulator();

    transform->setPosition({ position.x, position.y, position.z });

    std::cout << position << std::endl;
}

void Cube::render() const {

}

void Cube::setMass(real mass) {
    assert(mass != 0);
    inverseMass = 1 / mass;
}

void Cube::setInverseMass(real inverseMass) {
    this->inverseMass = inverseMass;
}

void Cube::setPosition(Vector3r position) {
    this->position = position;
}

void Cube::setVelocity(Vector3r velocity) {
    this->velocity = velocity;
}

void Cube::setAcceleration(Vector3r acceleration) {
    this->acceleration = acceleration;
}

void Cube::clearAccumulator() {
    this->forceAccum.clear();
}

void Cube::addForce(const Vector3r& force) {
    this->forceAccum.addScaledVector(force, 1);
}

bool Cube::hasFiniteMass() {
    if (inverseMass != 0)return true;
    return false;
}

Vector3r Cube::getPosition() {
    return this->position;
}

Vector3r Cube::getVelocity() {
    return this->velocity;
}

Vector3r Cube::getAcceleration() {
    return this->acceleration;
}

Vector3r Cube::getForceAccum() {
    return this->forceAccum;
}

real Cube::getDamping() {
    return this->damping;
}

real Cube::getInverseMass() {
    return this->inverseMass;
}

real Cube::getMass() {
    if (hasFiniteMass()) return (1 / inverseMass);
    return -1;
}

Vector3r Cube::getCentre() {
    return centre;
}

real Cube::getX() {
    return xLength;
}

real Cube::getY() {
    return yLength;
}

real Cube::getZ() {
    return zLength;
}

void Cube::setCentre(Vector3r newCentre) {
    centre = newCentre;
}

void Cube::setX(real x) {
    xLength = x;
}

void Cube::setY(real y) {
    yLength = y;
}

void Cube::setZ(real z) {
    zLength = z;
}