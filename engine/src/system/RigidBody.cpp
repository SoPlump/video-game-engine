//System includes

//Personal includes
#include "system/RigidBody.h"
#include "system/physics.h"
#include <sstream>


using namespace engine::physics;
using namespace engine;
using namespace std;

RigidBody::RigidBody(Entity* const parent) : Component(parent) {
	transform = parent->getComponent<Transform3D>();
	linearDamping = 0.7f;
	angularDamping = 0.7f;
}

/*
* Calculate the transformation matrix of the rigidbody at each frame
*/
inline void RigidBody::calculateTransformMatrix(Matrix4r& transformMatrix, const Vector3r& position, const Quaternion& orientation) {
	transformMatrix.values[0] = 1.0f - 2.0f * (orientation.axis.j * orientation.axis.j + orientation.axis.k * orientation.axis.k);
	transformMatrix.values[1] = 2.0f * (orientation.axis.i * orientation.axis.j - orientation.scalar * orientation.axis.k);
	transformMatrix.values[2] = 2.0f * (orientation.axis.i * orientation.axis.k + orientation.scalar * orientation.axis.j);
	transformMatrix.values[3] = position.x;
	transformMatrix.values[4] = 2.0f * (orientation.axis.i * orientation.axis.j + orientation.scalar * orientation.axis.k);
	transformMatrix.values[5] = 1.0f - 2 * (orientation.axis.i * orientation.axis.i - orientation.axis.k * orientation.axis.k);
	transformMatrix.values[6] = 2.0f * (orientation.axis.j * orientation.axis.k - orientation.scalar * orientation.axis.i);
	transformMatrix.values[7] = position.y;
	transformMatrix.values[8] = 2.0f * (orientation.axis.i * orientation.axis.k - orientation.scalar * orientation.axis.j);
	transformMatrix.values[9] = 2.0f * (orientation.axis.j * orientation.axis.k + orientation.scalar * orientation.axis.i);
	transformMatrix.values[10] = 1.0f - 2.0f * (orientation.axis.i * orientation.axis.i - orientation.axis.j * orientation.axis.j);
	transformMatrix.values[11] = position.z;
}

/*
* calculate the inverse of the inertia tensor of the rigidbody in world coordinates
*/
inline void RigidBody::transformInertiaTensor(Matrix3r& inverseInertiaTensorWorld, const Quaternion& q, const Matrix3r& inverseInertiaTensorBody, const Matrix4r& rotationMatrix) {
	real t4 = rotationMatrix.values[0] * inverseInertiaTensorBody.values[0] + rotationMatrix.values[1] * inverseInertiaTensorBody.values[3] + rotationMatrix.values[2] * inverseInertiaTensorBody.values[6];
	real t9 = rotationMatrix.values[0] * inverseInertiaTensorBody.values[1] + rotationMatrix.values[1] * inverseInertiaTensorBody.values[4] + rotationMatrix.values[2] * inverseInertiaTensorBody.values[7];
	real t14 = rotationMatrix.values[0] * inverseInertiaTensorBody.values[2] + rotationMatrix.values[1] * inverseInertiaTensorBody.values[5] + rotationMatrix.values[2] * inverseInertiaTensorBody.values[8];
	real t28 = rotationMatrix.values[4] * inverseInertiaTensorBody.values[0] + rotationMatrix.values[5] * inverseInertiaTensorBody.values[3] + rotationMatrix.values[6] * inverseInertiaTensorBody.values[6];
	real t33 = rotationMatrix.values[4] * inverseInertiaTensorBody.values[1] + rotationMatrix.values[5] * inverseInertiaTensorBody.values[4] + rotationMatrix.values[6] * inverseInertiaTensorBody.values[7];
	real t38 = rotationMatrix.values[4] * inverseInertiaTensorBody.values[2] + rotationMatrix.values[5] * inverseInertiaTensorBody.values[5] + rotationMatrix.values[6] * inverseInertiaTensorBody.values[8];
	real t52 = rotationMatrix.values[8] * inverseInertiaTensorBody.values[0] + rotationMatrix.values[9] * inverseInertiaTensorBody.values[3] + rotationMatrix.values[10] * inverseInertiaTensorBody.values[6];
	real t57 = rotationMatrix.values[8] * inverseInertiaTensorBody.values[1] + rotationMatrix.values[9] * inverseInertiaTensorBody.values[4] + rotationMatrix.values[10] * inverseInertiaTensorBody.values[7];
	real t62 = rotationMatrix.values[8] * inverseInertiaTensorBody.values[2] + rotationMatrix.values[9] * inverseInertiaTensorBody.values[5] + rotationMatrix.values[10] * inverseInertiaTensorBody.values[8];

	inverseInertiaTensorWorld.values[0] = t4 * rotationMatrix.values[0] + t9 * rotationMatrix.values[1] + t14 * rotationMatrix.values[2];
	inverseInertiaTensorWorld.values[1] = t4 * rotationMatrix.values[4] + t9 * rotationMatrix.values[5] + t14 * rotationMatrix.values[6];
	inverseInertiaTensorWorld.values[2] = t4 * rotationMatrix.values[8] + t9 * rotationMatrix.values[9] + t14 * rotationMatrix.values[10];
	inverseInertiaTensorWorld.values[3] = t28 * rotationMatrix.values[0] + t33 * rotationMatrix.values[1] + t38 * rotationMatrix.values[2];
	inverseInertiaTensorWorld.values[4] = t28 * rotationMatrix.values[4] + t33 * rotationMatrix.values[5] + t38 * rotationMatrix.values[6];
	inverseInertiaTensorWorld.values[5] = t28 * rotationMatrix.values[8] + t33 * rotationMatrix.values[9] + t38 * rotationMatrix.values[10];
	inverseInertiaTensorWorld.values[6] = t52 * rotationMatrix.values[0] + t57 * rotationMatrix.values[1] + t62 * rotationMatrix.values[2];
	inverseInertiaTensorWorld.values[7] = t52 * rotationMatrix.values[4] + t57 * rotationMatrix.values[5] + t62 * rotationMatrix.values[6];
	inverseInertiaTensorWorld.values[8] = t52 * rotationMatrix.values[8] + t57 * rotationMatrix.values[9] + t62 * rotationMatrix.values[10];
}

/*
* Calculates data that derives from other existing varibales but that need to be accessed frequently and quickly.
* Do not use this method if the application is too memory consumming as this only stores data that already exists but isn't yet calculated.
*/
void RigidBody::calculateDerivedData() {
	orientation.normalize();
	calculateTransformMatrix(transformMatrix, position, orientation);
	transformInertiaTensor(inverseInertiaTensorWorld, orientation, inverseInertiaTensor, transformMatrix);
}

/*
* Add a force to the rigidbody at a certain point in world coordinates
*/
void RigidBody::addForceAtPoint(Vector3r force, Vector3r point) {
	Vector3r pt = point;
	pt = pt - this->position;

	this->forceAccumulator = this->forceAccumulator + force;
	this->torqueAccumulator = this->torqueAccumulator + (pt.crossProduct(force));
}

/*
* Add a force to the rigidbody that applies at a certain point in local coordinates
*/
void RigidBody::addForceAtBodyPoint(Vector3r force, Vector3r point) {
	Vector3r pt = getPointInWorldCoords(point);
	addForceAtPoint(force, pt);
}

/*
* Add a force to the rigidbody that aplies to the center of mass and doesn't create a rotation
*/
void RigidBody::addForce(Vector3r force) {
	forceAccumulator = forceAccumulator + force;
}

/*
* Rigidbody Intergrator
*/
void RigidBody::integrate(real dt) {
	lastFrameAcceleration = acceleration;
	lastFrameAcceleration.addScaledVector(forceAccumulator, inverseMass);

	Vector3r angularAcceleration = inverseInertiaTensorWorld.transform(torqueAccumulator);

	//Adjust velocities
	velocity.addScaledVector(lastFrameAcceleration, dt);
	angularVelocity.addScaledVector(angularAcceleration, dt);

	//Impose drag forces
	velocity *= powr(linearDamping, dt);
	angularVelocity *= powr(angularDamping, dt);

	//Adjust positions
	position.addScaledVector(velocity, dt);
	orientation.addScaledVector(angularVelocity, dt);

	//cout << "Velocity Z: " << velocity.z << endl;

	calculateDerivedData();

	clearAccumulators();
}

void RigidBody::update(double dt) {

	transform->setPosition({ position.x, position.y, position.z });
	transform->setQuaternion({ orientation.axis.i, orientation.axis.j, orientation.axis.k, orientation.scalar });

}

void RigidBody::render() const {

}

void RigidBody::clearAccumulators() {
	this->forceAccumulator.clear();
	this->torqueAccumulator.clear();
}

/*
* Returns the world coordinates of the point from local coordinates
*/
Vector3r RigidBody::getPointInWorldCoords(Vector3r pointInLocalCoords) {
	Vector3r pointInWorldCoords = transformMatrix.transform(pointInLocalCoords);
	return pointInWorldCoords;
}

/*
* Getters and setters
*/
void RigidBody::setMass(real mass) {
	assert(mass != 0);
	this->inverseMass = 1.0f / mass;
}

void RigidBody::setInverseMass(real inverseMass) {
	this->inverseMass = inverseMass;
}

void RigidBody::setLinearDamping(real linearDamping) {
	this->linearDamping = linearDamping;
}

void RigidBody::setAngularDamping(real angularDamping) {
	this->angularDamping = angularDamping;
}

void RigidBody::setPosition(Vector3r position) {
	this->position = position;
}

void RigidBody::setVelocity(Vector3r velocity) {
	this->velocity = velocity;
}

void RigidBody::setOrientation(Quaternion orientation) {
	this->orientation = orientation;
}

void RigidBody::setAngularVelocity(Vector3r angularVelocity) {
	this->angularVelocity = angularVelocity;
}

void RigidBody::setTransformMatrix(Matrix4r transformMatrix) {
	this->transformMatrix = transformMatrix;
}

void RigidBody::setInertiaTensor(Matrix3r inertiaTensor) {
	this->inverseInertiaTensor = inertiaTensor.inverse();
}

void RigidBody::setInverseInertiaTensor(Matrix3r inverseInertiaTensor) {
	this->inverseInertiaTensor = inverseInertiaTensor;
}

bool RigidBody::hasFiniteMass() {
	return inverseMass != 0;
}

real RigidBody::getMass() {
	if (hasFiniteMass())
		return (1.0f / inverseMass);
	return -1;
}

real RigidBody::getInverseMass() {
	return this->inverseMass;
}

real RigidBody::getLinearDamping() {
	return this->linearDamping;
}

real RigidBody::getAngularDamping() {
	return this->angularDamping;
}

Vector3r RigidBody::getPosition() {
	return this->position;
}

Vector3r RigidBody::getVelocity() {
	return this->velocity;
}

Quaternion RigidBody::getOrientation() {
	return this->orientation;
}

Vector3r RigidBody::getAngularVelocity() {
	return this->angularVelocity;
}

Matrix4r RigidBody::getTransformMatrix() {
	return this->transformMatrix;
}

Matrix3r RigidBody::getInverseInertiaTensor() {
	return this->inverseInertiaTensor;
}

std::string RigidBody::toString() {
	stringstream ss;
	ss << "position " << position.toString() << "\n";
	ss << "masse " << getMass() << "\n";

	return ss.str();
}
