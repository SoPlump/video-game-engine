#include "system/Particle.h"
#include "system/physics.h"
#include <cassert>
#include <logging.h>
#include <system.h>
#include <components/Transform3D.h>
#include <components/MeshRenderer.h>
#include <renderer/Shader.h>
#include <renderer/Mesh.h>
#include <exception/ShaderLinkError.h>
#include <exception/ShaderCompileError.h>

using namespace engine::physics;
using namespace engine;
using namespace std;

Particle::Particle(Entity* const parent) : Component(parent) {
    transform = parent->getComponent<Transform3D>();
    damping = 1.0f;
    radius = 1.0f;
}

void Particle::update(double dt) {
    // ignore infinite mass
    if(inverseMass <= 0.0f)
        return;

    assert(dt > 0.0);
    
    // Update linear position
    position.addScaledVector(velocity, dt);

    // Work out the acceleration from the force
    Vector3r newAcceleration = acceleration;
    newAcceleration.addScaledVector(forceAccum, inverseMass);

    // Update linear velocity from the acceleration
    velocity.addScaledVector(newAcceleration, dt);

    // Impose drag
    velocity *= powr(damping, dt);

    // Clear the forces
    clearAccumulator();
    
    transform->setPosition({position.x, position.y, position.z});

    std::cout << position << std::endl;
}

void Particle::render() const {
    
}

void Particle::setMass(real mass) {
    assert(mass != 0);
    inverseMass = 1 / mass;
}

void Particle::setInverseMass(real inverseMass) {
    this->inverseMass = inverseMass;
}

void Particle::setPosition(Vector3r position) {
    this->position = position;
}

void Particle::setVelocity(Vector3r velocity) {
    this->velocity = velocity;
}

void Particle::setAcceleration(Vector3r acceleration) {
    this->acceleration = acceleration;
}

void Particle::setRadius(real radius) {
    this->radius = radius;
}

void Particle::clearAccumulator() {
    this->forceAccum.clear();
}

void Particle::addForce(const Vector3r& force) {
    this->forceAccum.addScaledVector(force, 1);
}

bool Particle::hasFiniteMass() {
    return inverseMass != 0;
}

Vector3r Particle::getPosition() {
    return this->position;
}

Vector3r Particle::getVelocity() {
    return this->velocity;
}

Vector3r Particle::getAcceleration() {
    return this->acceleration;
}

Vector3r Particle::getForceAccum() {
    return this->forceAccum;
}

real Particle::getDamping() {
    return this->damping;
}

real Particle::getInverseMass() {
    return this->inverseMass;
}

real Particle::getMass() {
    if (hasFiniteMass()) return (1 / inverseMass);
    return -1;
}

real Particle::getRadius() {
    return this->radius;
}