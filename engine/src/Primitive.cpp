#include "system/Primitive.h"

using namespace engine::physics;

/****** PRIMITIVE  *******/


Primitive::Primitive(Entity* const entity, Matrix4r offset) : Component(entity) {
	this->body = entity->getComponent<RigidBody>();
}

void Primitive::integrate(double dt) {
	body->integrate(dt);
}



/****** PLANE  *******/

Plane::Plane(Entity* const entity, Matrix4r offset, Vector3r normal, float planeOffset) : Primitive(entity, offset) {

	this->normal = normal;
	this->planeOffset = planeOffset;

}

float Plane::distance(Vector3r point) const {

	return point.dotProduct(normal) + planeOffset;

}

void Plane::render() const {}
void Plane::update(double dt) {}
primitivesTypes Plane::getType() {
	return primitivesTypes::PLANE;
}



/****** BOX  *******/

/*Box::Box(Entity* const entity) : Primitive(entity,Matrix4r()){

}*/

Box::Box(Entity* const entity, Matrix4r offset, Vector3r halfsizes) : Primitive(entity, offset) {
	this->halfsizes = halfsizes;

}

void Box::render() const {}
void Box::update(double dt) {}
primitivesTypes Box::getType() {
	return primitivesTypes::BOX;
}

Vector3r* Box::generateVertices() const {
    auto position = body->getPosition();
	Vector3r* results = new Vector3r[8];
	//results[0] = body->getPosition() - Vector3r(halfsizes.x, halfsizes.y, halfsizes.z); //Si un truc plante,  �a vient de cette ligne 

	results[0] = Vector3r(-halfsizes.x, -halfsizes.y, -halfsizes.z) + position;
	results[1] = Vector3r(halfsizes.x, -halfsizes.y, -halfsizes.z) + position;
	results[2] = Vector3r(-halfsizes.x, halfsizes.y, -halfsizes.z) + position;
	results[3] = Vector3r(-halfsizes.x, -halfsizes.y, halfsizes.z) + position;
	results[4] = Vector3r(halfsizes.x, halfsizes.y, -halfsizes.z) + position;
	results[5] = Vector3r(halfsizes.x, -halfsizes.y, halfsizes.z) + position;
	results[6] = Vector3r(-halfsizes.x, halfsizes.y, halfsizes.z) + position;
	results[7] = Vector3r(halfsizes.x, halfsizes.y, halfsizes.z) + position;


	for (int i = 0; i < 8; i++) {
		results[i] = offset.transform(results[i]);
	}

	return results;
}


void Sphere::render() const {}
void Sphere::update(double dt) {}
primitivesTypes Sphere::getType() {
	return primitivesTypes::SPHERE;
}