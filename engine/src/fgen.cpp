//System includes

//Personal includes
#include "fgen.h"

using namespace engine::physics;
using namespace std;

ForceRegistry::ForceRegistry() {}

void ForceRegistry::updateForces(real duration) {
	Registry::iterator it = registrations.begin();
	for (; it != registrations.end(); it++) {
		it->fg->updateForce(it->rigidbody, duration);
	}
}

void ForceRegistry::add(shared_ptr<RigidBody> rigidbody, shared_ptr<ForceGenerator> fg) {
	registrations.push_back({ rigidbody, fg });
}

void ForceRegistry::remove(shared_ptr<RigidBody> rigidbody, shared_ptr<ForceGenerator> fg) {
	ForceRegistration RegToRem = { rigidbody, fg };
	std::remove(registrations.begin(), registrations.end(), RegToRem);
}

void ForceRegistry::clear() {
	registrations.clear();
}

Gravity::Gravity(const Vector3r& gravity) {
	this->gravity = gravity;
}

void Gravity::updateForce(shared_ptr<RigidBody> rigidBody, real duration) {
	if (!rigidBody.get()->hasFiniteMass())
		return;
	rigidBody.get()->addForce(gravity * rigidBody.get()->getMass());
}

Spring::Spring(const Vector3r& localConnectionPoint, RigidBody* otherRigidBody, const Vector3r& otherConnectionPoint, real springConstant, real restLength) {
	this->connectionPoint = localConnectionPoint;
	this->otherConnectionPoint = otherConnectionPoint;
	this->otherEnd = otherRigidBody;
	this->springConstant = springConstant;
	this->restLength = restLength;
}

void Spring::updateForce(shared_ptr<RigidBody> rigidBody, real duration) {
	Vector3r firstEndInWorldSpace = rigidBody.get()->getPointInWorldCoords(connectionPoint);
	Vector3r secondEndInWorldSpace = otherEnd->getPointInWorldCoords(otherConnectionPoint);

	Vector3r force = firstEndInWorldSpace - secondEndInWorldSpace;

	real magnitude = force.magnitude();
	magnitude = magnitude - restLength;
	magnitude *= springConstant;

	force.normalize();
	force *= magnitude;
	rigidBody.get()->addForceAtPoint(force, firstEndInWorldSpace);
}

Buoyancy::Buoyancy(const Vector3r& centerOfBuoyancy, real maxDepth, real volume, real waterHeight, real liquidDensity) {
	this->maxDepth = maxDepth;
	this->volume = volume;
	this->waterHeight = waterHeight;
	this->liquidDensity = liquidDensity;
	this->centerOfBuoyancy = centerOfBuoyancy;
}

void Buoyancy::updateForce(shared_ptr<RigidBody> rigidBody, real duration) {

}
