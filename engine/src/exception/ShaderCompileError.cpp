#include "exception/ShaderCompileError.h"

ShaderCompileError::ShaderCompileError(GLuint shader) : shader(shader) {
    GLint logLength = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

    message = new char[logLength];
    glGetShaderInfoLog(shader, logLength, nullptr, message);
}

ShaderCompileError::~ShaderCompileError() {
    delete[] message;
}

const char* ShaderCompileError::what() const noexcept {
    return message;
}
