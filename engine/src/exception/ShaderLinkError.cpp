#include "exception/ShaderLinkError.h"

ShaderLinkError::ShaderLinkError(GLuint program) : program(program) {
    GLint logLength = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);

    message = new char[logLength];
    glGetProgramInfoLog(program, logLength, nullptr, message);
}

ShaderLinkError::~ShaderLinkError() {
    delete[] message;
}

const char* ShaderLinkError::what() const noexcept {
    return message;
}
