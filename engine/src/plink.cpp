#include <plink.h>

using namespace std;
using namespace engine::physics;

ParticleLink::ParticleLink(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2) {
	this->particles[0] = particle1;
	this->particles[1] = particle2;
}

real ParticleLink::currentLength() const {
	Vector3r relativePos = particles[0]->getPosition() - particles[1]->getPosition();
	return relativePos.magnitude();
}

ParticleCable::ParticleCable(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2, real maxLength, real restitution) : 
	ParticleLink(particle1, particle2) {
	
	this->maxLength = maxLength;
	this->restitution = restitution;
}

unsigned ParticleCable::addContact(ParticleContact* contact,
	unsigned limit) const {
	// Find the length of the cable.
	real length = currentLength();

	// Check if we�re overextended.

	/*
	if (length < maxLength && length > 0.04f)
	{
		return 0;
	}
	*/
	if (length < maxLength)
	{
		return 0;
	}

	// Otherwise, return the contact.
	contact->particles[0] = particles[0];
	contact->particles[1] = particles[1];

	// Calculate the normal.
	Vector3r normal =
		particles[1]->getPosition() - particles[0]->getPosition();
	normal.normalize();
	contact->contactNormal = normal;
	/*
	if (length > maxLength) {
		contact->penetration = length - maxLength;
	}
	else {
		contact->penetration = length;
	}
	*/
	contact->penetration = length - maxLength;
	contact->restitution = restitution;
	return 1;
}

ParticleRod::ParticleRod(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2, real length):
	ParticleLink(particle1, particle2) {
	this->length = length;
}

unsigned ParticleRod::addContact(ParticleContact* contact,
	unsigned limit) const {
	// Find the length of the rod.
	real currentLen = currentLength();

	// Check if we�re overextended.
	if (currentLen == length)
	{
		return 0;
	}

	// Otherwise, return the contact.
	contact->particles[0] = particles[0];
	contact->particles[1] = particles[1];

	// Calculate the normal.
	Vector3r normal =
		particles[1]->getPosition() - particles[0]->getPosition();
	normal.normalize();

	// The contact normal depends on whether we�re extending or
	// compressing.
		if (currentLen > length) {
			contact->contactNormal = normal;
			contact->penetration = currentLen - length;
		}
		else {
			contact->contactNormal = -1.0f * normal ;
			contact->penetration = length - currentLen;
		}
	// Always use zero restitution (no bounciness).
	contact->restitution = 0;
	return 1;
}


ParticleFloor::ParticleFloor(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2, real restitution) :
	ParticleLink(particle1, particle2) {
	this->restitution = restitution;
}

unsigned ParticleFloor::addContact(ParticleContact* contact,
	unsigned limit) const {
	// Check if we�re over the floor.
	if (particles[0]->getPosition().y > 0.0f+particles[0]->getRadius())
	{
		return 0;
	}

	// Otherwise, return the contact.
	contact->particles[0] = particles[0];
	contact->particles[1] = particles[1];

	// Calculate the normal.
	Vector3r normal = Vector3r(0.0f, 1.0f, 0.0f);

	contact->contactNormal = normal;
	contact->penetration = particles[0]->getRadius() - particles[0]->getPosition().y;

	contact->restitution = restitution;
	return 1;
}


ParticleLeftWall::ParticleLeftWall(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2, real restitution) :
	ParticleLink(particle1, particle2) {
	this->restitution = restitution;
}

unsigned ParticleLeftWall::addContact(ParticleContact* contact,
	unsigned limit) const {
	// Check if we�re in contact with the left wall.
	if (particles[0]->getPosition().x > -20.0f + particles[0]->getRadius())
	{
		return 0;
	}

	// Otherwise, return the contact.
	contact->particles[0] = particles[0];
	contact->particles[1] = particles[1];

	// Calculate the normal.
	Vector3r normal = Vector3r(1.0f, 0.0f, 0.0f);

	contact->contactNormal = normal;
	contact->penetration = (particles[0]->getRadius() - particles[0]->getPosition().x) - 20.0f;

	contact->restitution = restitution;
	return 1;
}

ParticleRightWall::ParticleRightWall(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2, real restitution) :
	ParticleLink(particle1, particle2) {
	this->restitution = restitution;
}

unsigned ParticleRightWall::addContact(ParticleContact* contact,
	unsigned limit) const {
	// Check if we�re in contact with the right wall.
	if (particles[0]->getPosition().x < 20.0f - particles[0]->getRadius())
	{
		return 0;
	}

	// Otherwise, return the contact.
	contact->particles[0] = particles[0];
	contact->particles[1] = particles[1];

	// Calculate the normal.
	Vector3r normal = Vector3r(-1.0f, 0.0f, 0.0f);

	contact->contactNormal = normal;
	contact->penetration = (particles[0]->getRadius() + particles[0]->getPosition().x) - 20.0f;

	contact->restitution = restitution;
	return 1;
}

ParticleSphere::ParticleSphere(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2, real restitution) :
	ParticleLink(particle1, particle2) {
	this->restitution = restitution;
}

unsigned ParticleSphere::addContact(ParticleContact* contact,
	unsigned limit) const {
	// Check if a sphere particle is in contact with another sphere particle.
	Vector3r sphereDistance = particles[0]->getPosition() - particles[1]->getPosition();
	if ( sphereDistance.squareMagnitude() > (particles[0]->getRadius() + particles[1]->getRadius())*(particles[0]->getRadius() + particles[1]->getRadius()))
	{
		return 0;
	}

	// Otherwise, return the contact.
	contact->particles[0] = particles[0];
	contact->particles[1] = particles[1];

	// Calculate the normal.
	Vector3r normal = particles[0]->getPosition() - particles[1]->getPosition();
	normal.normalize();
	//Vector3r(-1.0f, 0.0f, 0.0f);

	contact->contactNormal = normal;
	contact->penetration = (particles[0]->getRadius() + particles[1]->getRadius()) - sphereDistance.magnitude();

	contact->restitution = restitution;
	return 1;
}