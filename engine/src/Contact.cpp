#include "Contact.h"
#include <sstream>

using namespace engine::physics;
using namespace std;

void Contact::setBodyData(shared_ptr<RigidBody> one, shared_ptr<RigidBody> two) {
	rb1 = one;
	rb2 = two;
}

std::string Contact::toString() {
	stringstream ss;
	ss << "contactPoint " << contactPoint.toString() << "\n";
	ss << "contactNormal " << contactNormal.toString() << "\n";
	ss << "penetration " << penetration << "\n";
	ss << "rb1 " << rb1->toString() << "\n";
	if(rb2)
		ss << "rb2 " << rb2->toString() << "\n";

	return ss.str();

}
