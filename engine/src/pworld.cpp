#include <pworld.h>
#include "plink.h"

using namespace std;
using namespace engine::physics;

ParticleWorld::ParticleWorld(unsigned maxContacts, unsigned iterations) {
	registry = ParticleForceRegistry();
	resolver = ParticleContactResolver(iterations);
	contacts = new ParticleContact[maxContacts];
}

unsigned ParticleWorld::generateContacts() {
	unsigned limit = maxContacts;
	ParticleContact* nextContact = contacts;
	for (ContactGenerators::iterator g = contactGenerators.begin();
		g != contactGenerators.end();
		g++) {
		unsigned used = (*g)->addContact(nextContact, limit);
		limit -= used;
		nextContact += used;
		// We�ve run out of contacts to fill. This means we�re missing
		// contacts.
		if (limit <= 0) {
			break;
		}
	}
	// Return the number of contacts used.
	return maxContacts - limit;
}

void ParticleWorld::update(double duration) {
	for (Particles::iterator p = particles.begin();
		p != particles.end();
		p++) {
		// Integrate the particle by the given duration.
		p->get()->update(duration);
	}
}

void ParticleWorld::runPhysics(double duration) {
	// First, apply the force generators.
	registry.updateForces(duration);

	// Then integrate the objects.
	update(duration);

	// Generate contacts.
	unsigned usedContacts = generateContacts();

	// And process them.
	if (usedContacts) {
		/* No idea where this boolean comes from
		if (calculateIterations) {
			resolver.setIterations(usedContacts * 2);
		}
		*/
		resolver.setIterations(usedContacts * 2);
		resolver.resolveContacts(contacts, usedContacts, duration);
	}
}

void ParticleWorld::addParticle(shared_ptr<Particle> particle) {
	particles.push_back(particle);
}

ParticleWorld::Particles& ParticleWorld::getParticles() {
	return particles;
}

//void ParticleWorld::addContact(shared_ptr<ParticleContact> contact) {
//	if (numberOfContacts < maxContacts) {
//		contacts[numberOfContacts++] = *contact;
//	}
//}
