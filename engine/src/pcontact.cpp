#include <pcontact.h>
#include <vector>

using namespace std;
using namespace engine::physics;

ParticleContact::ParticleContact(shared_ptr<Particle> particle1, shared_ptr<Particle> particle2, real restitution)
{
	this->particles[0] = particle1;
	this->particles[1] = particle2;

	this->restitution = restitution;
}

void ParticleContact::resolve(real duration) {
	resolveVelocity(duration);
	resolveInterpenetration(duration);
}

real ParticleContact::calculateSeparatingVelocity() const {
	Vector3r relativeVelocity = particles[0]->getVelocity();
	if (particles[1]) relativeVelocity = relativeVelocity - particles[1]->getVelocity();
	return relativeVelocity * contactNormal;
}

real ParticleContact::getTotalInverseMass() const {
	real totalInverseMass = particles[0]->getInverseMass();
	if (particles[1]) {
		totalInverseMass += particles[1]->getInverseMass();
	}
	return totalInverseMass;
}


void ParticleContact::resolveVelocity(real duration) {
	// Find the velocity in the direction of the contact.
	real separatingVelocity = calculateSeparatingVelocity();

	// Check if it needs to be resolved.
	if (separatingVelocity > 0)
	{
		// The contact is either separating, or stationary;
		// no impulse is required.
		return;
	}

	// Calculate the new separating velocity.
	real newSepVelocity = -1 * separatingVelocity * restitution;

	// Check the velocity buildup due to acceleration only.
	Vector3r accCausedVelocity = particles[0]->getAcceleration();
	if (particles[1]) {
		accCausedVelocity = accCausedVelocity - particles[1]->getAcceleration();
	}
	real accCausedSepVelocity = accCausedVelocity * contactNormal * duration;

	// If we�ve got a closing velocity due to aceleration buildup,
	// remove it from the new separating velocity.
	if (accCausedSepVelocity < 0) {
		newSepVelocity += restitution * accCausedSepVelocity;
		// Make sure we haven�t removed more than was
		// there to remove.
		if (newSepVelocity < 0) {
			newSepVelocity = 0;
		}
	}


	real deltaVelocity = newSepVelocity - separatingVelocity;

	// We apply the change in velocity to each object in proportion to
	// their inverse mass (i.e., those with lower inverse mass [higher
	// actual mass] get less change in velocity).
	real totalInverseMass = getTotalInverseMass();

	// If all particles have infinite mass, then impulses have no effect.
	if (totalInverseMass <= 0) {
		return;
	}

	// Calculate the impulse to apply.
	real impulse = deltaVelocity / totalInverseMass;

	// Find the amount of impulse per unit of inverse mass.
	Vector3r impulsePerIMass = contactNormal * impulse;
	// Apply impulses: they are applied in the direction of the contact,
	// and are proportional to the inverse mass.
	particles[0]->setVelocity(particles[0]->getVelocity() +
		impulsePerIMass * particles[0]->getInverseMass());

	if (particles[1])
	{
		// Particle 1 goes in the opposite direction
		particles[1]->setVelocity(particles[1]->getVelocity() +
			impulsePerIMass * -1.0f * particles[1]->getInverseMass()
		);
	}
}

void ParticleContact::resolveInterpenetration(real duration) {

	vector<Vector3r> particleMovements;
	// If we don�t have any penetration, skip this step.
	if (penetration <= 0) return;

	// The movement of each object is based on their inverse mass,
	// so total that.
	real totalInverseMass = getTotalInverseMass();

	// If all particles have infinite mass, then we do nothing.
	if (totalInverseMass <= 0) {
		return;
	}

	// Find the amount of penetration resolution per unit
	// of inverse mass.
	Vector3r movePerIMass =
		contactNormal * (penetration / totalInverseMass);

	// Calculate the movement amounts.
	particleMovements.push_back(movePerIMass * particles[0]->getInverseMass());
	if (particles[1]) {
		particleMovements.push_back(movePerIMass * -1.0f * particles[1]->getInverseMass());
	}

	// Apply the penetration resolution.
	particles[0]->setPosition(particles[0]->getPosition() + particleMovements[0]);
	if (particles[1]) {
		particles[1]->setPosition(particles[1]->getPosition() + particleMovements[1]);
	}
}

ParticleContactResolver::ParticleContactResolver(unsigned iterations) {
	this->iterations = iterations;
}

void ParticleContactResolver::setIterations(unsigned iterations) {
	this->iterations = iterations;
}

void ParticleContactResolver::resolveContacts(ParticleContact* contactArray, unsigned numContacts, real duration) {
	unsigned i;
	iterationsUsed = 0;
	while (iterationsUsed < iterations)
	{
		// Find the contact with the largest closing velocity.
		real max = REAL_MAX;
		unsigned maxIndex = numContacts;
		for (i = 0; i < numContacts; i++)
		{
			real sepVel = contactArray[i].calculateSeparatingVelocity();
			if (sepVel < max &&
				(sepVel < 0 || contactArray[i].penetration > 0)) {
				max = sepVel;
				maxIndex = i;
			}
		}
		// Do we have anything worth resolving?
		if (maxIndex == numContacts) {
			break;
		}
		// Resolve this contact.
		contactArray[maxIndex].resolve(duration);
		iterationsUsed++;
	}
}
