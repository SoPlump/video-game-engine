#include "system.h"
#include "logging.h"
#include <string>
#include <unordered_map>
#include <GL/glew.h>
#include <ctime>
#include <cstdlib>

using namespace std;

static struct {
    glm::i32vec2 position;
    glm::i32vec2 relative;

    void setPosition(int x, int y, bool updateRelative = true) {
        if(updateRelative) {
            relative.x = x - position.x;
            relative.y = y - position.y;
        }
        position.x = x;
        position.y = y;
    }

    void setRelative(int relX, int relY) {
        relative.x = relX;
        relative.y = relY;
    }
} mouse;

static std::unordered_map<SDL_Keycode, bool> keyboardState;

static SDL_Window* window_;
static int width_;
static int height_;
static float aspectRatio_;

// ***  ***  *** //
// *** Event handling system *** //
// ***  ***  *** //

static void handleWindowEvent(SDL_Event& event) {
    if(event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
        SDL_GetWindowSize(window_, &width_, &height_);
        aspectRatio_ = float(width_) / float(height_);
        glViewport(0, 0, width_, height_);
    }
}

static void handleKeyEvent(SDL_Event& event) {
    keyboardState[event.key.keysym.sym] = event.type == SDL_KEYDOWN;
}

static void handleMouseEvent(SDL_Event& event) {
    mouse.setPosition(event.motion.x, event.motion.y);
}

bool engine::system::handleEvents() {
    SDL_Event event;
    bool mouseMotionUpdate = false;

    while(SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_QUIT:
                return false;
            case SDL_WINDOWEVENT:
                handleWindowEvent(event);
                break;
            case SDL_KEYUP:
            case SDL_KEYDOWN:
                handleKeyEvent(event);
                break;
            case SDL_MOUSEMOTION:
                mouseMotionUpdate = true;
                handleMouseEvent(event);
                break;
            default:
                break;
        }
    }

    if(!mouseMotionUpdate)
        mouse.setRelative(0, 0);

    return true;
}

// ***  ***  *** //
// *** Input handling system *** //
// ***  ***  *** //

const glm::i32vec2& engine::system::getMousePosition() {
    return mouse.position;
}

int engine::system::getMouseX() {
    return mouse.position.x;
}

int engine::system::getMouseY() {
    return mouse.position.y;
}

const glm::i32vec2& engine::system::getMouseRelative() {
    return mouse.relative;
}

int engine::system::getMouseRelativeX() {
    return mouse.relative.x;
}

int engine::system::getMouseRelativeY() {
    return mouse.relative.y;
}

bool engine::system::isKeyPressed(SDL_Keycode keyCode) {
    auto search = keyboardState.find(keyCode);
    if(search == keyboardState.end()) {
        keyboardState[keyCode] = false;
        return false;
    }
    return search->second;
}

// ***  ***  *** //
// *** Window managing system *** //
// ***  ***  *** //

static bool initSDL() {
    INFO << "Initializing SDL";

    SDL_Init(SDL_INIT_VIDEO);
    window_ = SDL_CreateWindow(
        "Sample",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        640, 480,
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL
    );

    if(window_ == nullptr) {
        FATAL << "Can't create window : " << SDL_GetError();
        return false;
    }

    SDL_GetWindowSize(window_, &width_, &height_);
    aspectRatio_ = float(width_) / float(height_);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 1);

    // request opengl debug context on debug build
#ifndef NDEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    if(SDL_GL_CreateContext(window_) == nullptr) {
        FATAL << "SDL_GL_CreateContext : " << SDL_GetError();
        return false;
    }

    INFO << "SDL successfully initialized";
    return true;
}

static bool initGLEW() {
    INFO << "Initializing GLEW";

    GLenum glewStatus = glewInit();
    if(glewStatus != GLEW_OK) {
        FATAL << "glewInit : " << glewGetErrorString(glewStatus);
        return false;
    }

    if(!GLEW_VERSION_3_3) {
        FATAL << "Your graphic card does not support OpenGL 3.3";
        return false;
    }

    INFO << "GLEW successfully initialized";
    return true;
}

#ifndef NDEBUG
static void glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
    if(id == 131169 || id == 131185 || id == 131218 || id == 131204)
        return;

    string sourceStr;
    switch(source) {
        case GL_DEBUG_SOURCE_API:
            sourceStr = "API";
            break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            sourceStr = "Window System";
            break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            sourceStr = "Shader Compiler";
            break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            sourceStr = "Third Party";
            break;
        case GL_DEBUG_SOURCE_APPLICATION:
            sourceStr = "Application";
            break;
        case GL_DEBUG_SOURCE_OTHER:
            sourceStr = "Other";
            break;
        default:
            sourceStr = "Unknown";
            break;
    }

    string typeStr;
    switch (type) {
        case GL_DEBUG_TYPE_ERROR:
            typeStr = "Error";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            typeStr = "Deprecated Behaviour";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            typeStr = "Undefined Behaviour";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            typeStr = "Portability";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            typeStr = "Performance";
            break;
        case GL_DEBUG_TYPE_MARKER:
            typeStr = "Marker";
            break;
        case GL_DEBUG_TYPE_PUSH_GROUP:
            typeStr = "Push Group";
            break;
        case GL_DEBUG_TYPE_POP_GROUP:
            typeStr = "Pop Group";
            break;
        case GL_DEBUG_TYPE_OTHER:
            typeStr = "Other";
            break;
        default:
            typeStr = "Unknown";
            break;
    }

    namespace log = boost::log;

    string severityStr;
    switch(severity) {
        case GL_DEBUG_SEVERITY_HIGH:
            GL_FATAL << log::add_value("source", sourceStr)
                     << log::add_value("type", typeStr)
                     << log::add_value("gl_id", id)
                     << message;
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            GL_ERROR << log::add_value("source", sourceStr)
                     << log::add_value("type", typeStr)
                     << log::add_value("gl_id", id)
                     << message;
            break;
        case GL_DEBUG_SEVERITY_LOW:
            GL_WARNING << log::add_value("source", sourceStr)
                       << log::add_value("type", typeStr)
                       << log::add_value("gl_id", id)
                       << message;
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            GL_INFO << log::add_value("source", sourceStr)
                    << log::add_value("type", typeStr)
                    << log::add_value("gl_id", id)
                    << message;
            break;
        default:
            break;
    }
}
#endif

static bool initOpenGL() {
    INFO << "Initializing OpenGL context";
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glPointSize(7.0f);

    // enable opengl debugging on debug builds
#ifndef NDEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(glDebugOutput, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
#endif

    INFO << "OpenGL context successfully initialized";
    return true;
}

static void shutdownSDL() {
    INFO << "Shutting down SDL";
    if(window_ != nullptr)
        SDL_DestroyWindow(window_);
    SDL_Quit();
    INFO << "SDL shutdown";
}

bool engine::system::init() {
    srand(static_cast <unsigned> (time(0)));
    if(!initSDL() || !initGLEW() || !initOpenGL()) {
        shutdown();
        return false;
    }
    return true;
}

void engine::system::shutdown() {
    shutdownSDL();
}

SDL_Window* engine::system::window() {
    return window_;
}

int engine::system::width() {
    return width_;
}

int engine::system::height() {
    return height_;
}

float engine::system::aspectRatio() {
    return aspectRatio_;
}
