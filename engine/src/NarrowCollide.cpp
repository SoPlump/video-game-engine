//System includes

//Personal includes
#include "NarrowCollide.h"
#include "system/physics.h"
#include "Contact.h"
#include "system/Primitive.h"

using namespace engine::physics;
using namespace std;

/*static inline real penetrationOnAxis(const CollisionBox& one, const CollisionBox& two, const Vector3r& axis, const Vector3r& toCenter) {
	real oneProject = transformToAxis(one, axis);
	real twoProject = transformToAxis(two, axis);

	real distance = abs(toCenter * axis);

	return oneProject + twoProject - distance;
}*/
/*
unsigned CollisionDetector::generateCollision(const Sphere& one, const Sphere& two, CollisionData* data) {
	if (data->contactsRestants <= 0)
		return 0;

	Vector3r positionOne = one.getAxis(3);
	Vector3r positionTwo = two.getAxis(3);

	Vector3r midline = positionOne - positionTwo;
	real size = midline.magnitude();

	if (size <= 0.0f || size >= one.radius + two.radius) {
		return 0;
	}

	Vector3r normal = midline * (((real)1.0) / size);

	Contact* contact = data->contacts;
	contact->contactNormal = normal;
	contact->contactPoint = positionOne + midline * (real)0.5;
	contact->penetration = (one.radius + two.radius - size);
	contact->setBodyData(one.body, two.body);

	data->addContacts(1);
	return 1;
}
*/
/*
unsigned CollisionDetector::generateCollision(const Sphere& sphere, const Plane& plane, CollisionData* data) {
	if (data->contactsRestants <= 0)
		return 0;

	Vector3r position = sphere.getAxis(3);

	real ballDistance = plane.normal * position - sphere.radius - plane.planeOffset;

	if (ballDistance >= 0)
		return 0;

	Contact* contact = data->contacts;
	contact->contactNormal = plane.normal;
	contact->penetration = -ballDistance;
	contact->contactPoint = position - plane.normal * (ballDistance + sphere.radius);
	contact->setBodyData(sphere.body, NULL);

	data->addContacts(1);
	return 1;
}
*/
/*
unsigned CollisionDetector::generateCollision(const Sphere& sphere, const Plane& plane, CollisionData* data) {
	if (data->contactsRestants <= 0)
		return 0;

	Vector3r position = sphere.getAxis(3);

	real centerDistance = plane.normal * position - plane.offset;

	if (centerDistance * centerDistance > sphere.radius * sphere.radius)
		return 0;

	Vector3r normal = plane.normal;
	real penetration = -centerDistance;
	if (centerDistance < 0) {
		normal *= -1;
		penetration = -penetration;
	}
	penetration += sphere.radius;

	Contact* contact = data->contacts;
	contact->contactNormal = normal;
	contact->penetration = penetration;
	contact->contactPoint = position - plane.direction * centerDistance;
	contact->setBodyData(sphere.body, NULL, data->friction, data->restitution);

	data->addContacts(1);
	return 1;
}
*/

unsigned CollisionDetector::generateCollision(const Box& box, const Plane& plane, shared_ptr<CollisionData> data) {
	if (data->contactsRestants <= 0)
		return 0;


	//Test contacts
	auto vertices = box.generateVertices();
	bool isContact = false;
	for (int i = 0; i < 8; i++) {
		if (plane.distance(vertices[i]) <= 0) {
			isContact = true;
			break;
		}
	}
	if (!isContact)return 0;

	static real mults[8][3] = { {1,1,1},{-1,1,1}, {1,-1,1}, {1,1,-1}, {-1,-1,1}, {-1,1,-1}, {1,-1,-1}, {-1,-1,-1} };

	//shared_ptr<Contact> contact = data->contacts;
	//shared_ptr<Contact> contact = make_shared<Contact>(Contact());
	unsigned contactsUsed = 0;
	for (unsigned i = 0; i < 8; i++) {
		Vector3r vertexPos(mults[i][0], mults[i][1], mults[i][2]);
		vertexPos.componentProductUpdate(box.halfsizes);
		vertexPos = box.offset.transform(vertexPos);

		real vertexDistance = vertexPos * plane.normal;
		
		if (vertexDistance <= plane.planeOffset) {
			auto contact = make_shared< Contact>();
			contact->contactPoint = plane.normal * (vertexDistance - plane.planeOffset) + vertexPos;
			contact->contactNormal = plane.normal;
			contact->penetration = plane.planeOffset - vertexDistance;
			contact->setBodyData(box.body, nullptr); //Write the appropriate data C PA KLR
			data->contacts.push_back(contact);
			contactsUsed++;
			if (contactsUsed == data->contactsRestants)
				return contactsUsed;
		}
	}

	data->addContacts(contactsUsed);
	return 1;
}
/*
unsigned CollisionDetector::generateCollision(const Box& box, const Sphere& sphere, CollisionData* data) {
	Vector3r center = sphere.getAxis(3);
	Vector3r relCenter = box.transform.transformInverse(center);

	if (abs(relCenter.x) - sphere.radius > box.halfsizes.x || abs(relCenter.y) - sphere.radius > box.halfsizes.y || abs(relCenter.z) - sphere.radius > box.halfsizes.z) {
		return 0;
	}

	Vector3r closestPt(0, 0, 0);
	real distance;

	distance = relCenter.x;
	if (distance > box.halfsizes.x)
		distance = box.halfsizes.x;
	if (distance < -box.halfsizes.x)
		distance = -box.halfsizes.x;
	closestPt.x = distance;

	distance = relCenter.y;
	if (distance > box.halfsizes.y)
		distance = box.halfsizes.y;
	if (distance < -box.halfsizes.y)
		distance = -box.halfsizes.y;
	closestPt.y = distance;

	distance = relCenter.z;
	if (distance > box.halfsizes.z)
		distance = box.halfsizes.z;
	if (distance < -box.halfsizes.z)
		distance = -box.halfsizes.z;
	closestPt.z = distance;

	distance = (closestPt - relCenter).squareMagnitude();
	if (distance > sphere.radius * sphere.radius)
		return 0;

	Vector3r closestPtWorld = box.offset.transform(closestPt);

	Contact* contact = data->contacts;
	contact->contactNormal = (closestPtWorld - center);
	contact->contactNormal.normalize();
	contact->contactPoint = closestPtWorld;
	contact->penetration = sphere.radius - sqrt(distance);
	contact->setBodyData(box.body, sphere.body);

	data->addContacts(1);
	return 1;
}
*/
/*unsigned CollisionDetector::generateCollision(const CollisionBox& boxOne, const CollisionSphere& boxTwo, CollisionData* data) {
	float bestOverlap = FLOAT_MAX;
	unsigned bestCase;

	Vector3r axes[15] = { boxOne.getAxis(0), boxOne.getAxis(1), boxOne.getAxis(2),boxTwo.getAxis(0),boxTwo.getAxis(1),boxTwo.getAxis(2),boxOne.getAxis(0) % boxTwo.getAxis(0),boxOne.getAxis(0) % boxTwo.getAxis(1),boxOne.getAxis(0) % boxTwo.getAxis(2),boxOne.getAxis(1) % boxTwo.getAxis(0),boxOne.getAxis(1) % boxTwo.getAxis(1),boxOne.getAxis(1) % boxTwo.getAxis(2),boxOne.getAxis(2) % boxTwo.getAxis(0),boxOne.getAxis(2) % boxTwo.getAxis(1),boxOne.getAxis(2) % boxTwo.getAxis(2) };


	for (unsigned index = 0; index < 15; index++) {
		Vector3r* axis = axes[index];
		real toCenter = boxTwo.center - boxOne.center;

		if (axis.squareMagnitude() < 0.001)
			continue;
		axis.normalize();

		real overlap = penetrationOnAxis(boxOne, boxTwo, axis, toCenter);
		if (overlap < 0)
			return;
		if (overlap < bestOverlap) {
			bestOverlap = overlap;
			bestCase = index;
		}
	}

	Vector3r* axis = axes[bestCase];

	if (axis * toCenter > 0) {
		axis *= -1.0f;
	}

	Vector3r vertex = boxTwo.halfsizes;
	if (boxTwo.getAxis(0) * noraml < 0)
		vertex.x = -vertex.x;
	if (boxTwo.getAxis(1) * normal < 0)
		vertex.y = -vertex.y;
	if (boxTwo.getAxis(3) * normal < 0)
		vertex.z = -vertex.z;

	vertex = otherBox.getTransform() * vertex;

	contact->contactNormal = axis;
	contact->penetration = bestOverlap;
	contact->contactPoint = vertex;
}*/