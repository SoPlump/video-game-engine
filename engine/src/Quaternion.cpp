#include "Quaternion.h"
#include "Vector.h"

using namespace engine::physics;

Quaternion::Quaternion() {
	scalar = 1.0f;
	axis = Vector3r();
}

Quaternion::Quaternion(real scalar, Vector3r axis) : scalar(scalar), axis(axis) {}

/**
* Normalizes the quaternion to unit length, making it a valid
* orientation quaternion.
*/
void Quaternion::normalize() {

	real d = scalar * scalar + axis.i * axis.i + axis.j * axis.j + axis.k * axis.k;

	// Check for zero-length quaternion, and use the no-rotation
	// quaternion in that case.
	if (d == 0) {
		scalar = 1;
		return;
	}

	// Normalize 

	d = (1.0f) / sqrt(d);
	scalar *= d;
	axis.i *= d;
	axis.j *= d;
	axis.k *= d;
}

void Quaternion::rotateByVector(const Vector3r& vector) {
	Quaternion q(0.0f, vector);
	(*this) *= q;
}

/** 
* Updated the orientation quaternion by the angular velocity and a time
**/
void Quaternion::addScaledVector(const Vector3r& vector, real delta) {
	Quaternion q(0, delta * vector);
	q *= *this;
	scalar += q.scalar * 0.5f;
	axis.i += q.axis.i * 0.5f;
	axis.j += q.axis.j * 0.5f;
	axis.k += q.axis.k * 0.5f;
}

/**
* Multiplies the quaternion by the given quaternion.
*/
void Quaternion::operator *=(const Quaternion& multiplier) {
	Quaternion q = *this;
	scalar = q.scalar * multiplier.scalar - q.axis.i * multiplier.axis.i -
		q.axis.j * multiplier.axis.j - q.axis.k * multiplier.axis.k;
	axis.i = q.scalar * multiplier.axis.i + q.axis.i * multiplier.scalar +
		q.axis.j * multiplier.axis.k - q.axis.k * multiplier.axis.j;
	axis.j = q.scalar * multiplier.axis.j + q.axis.j * multiplier.scalar +
		q.axis.k * multiplier.axis.i - q.axis.i * multiplier.axis.k;
	axis.k = q.scalar * multiplier.axis.k + q.axis.k * multiplier.scalar +
		q.axis.i * multiplier.axis.j - q.axis.j * multiplier.axis.i;
}

bool Quaternion::operator ==(const Quaternion& q) {
	return ((scalar == q.scalar) && (axis == q.axis));
}