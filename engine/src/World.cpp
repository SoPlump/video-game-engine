//System includes

//Personal includes
#include "World.h"

using namespace std;
using namespace engine::physics;

World::World(unsigned maxContacts, unsigned iterations) {
    registry = ForceRegistry();
    collisionData = make_shared<CollisionData>();
    collisionDetector = make_shared<CollisionDetector>();
    /*resolver = RigidBodyContactResolver(iterations);
    contacts = new RigidBodyContact[maxContacts];*/
}

void World::startFrame() {
    collisionData->contacts.clear();
    collisionData->contactsRestants = 10;
    for(auto& obj : objects) {
        auto primitives = obj->getPrimitives();
        for(auto& prim : primitives) {

            auto rb = prim->getBody();
            rb->clearAccumulators();
            rb->calculateDerivedData();
        }
    }
}


void World::runPhysics(double dt) {
    registry.updateForces(dt);
    integrate(dt);
    generateContacts();
    cout << "Contacts restants " << collisionData->contactsRestants << endl;
    if(collisionData.get()->contactsRestants < 10) {
        cout << "Contact ! " << collisionData->toString() << endl;
    }
}


void World::integrate(double dt) {

    for(Objects::iterator obj = objects.begin(); obj != objects.end(); obj++) {
        obj->get()->integrate(dt);
    }
}


void World::addObject(shared_ptr<Object> object) {
    objects.push_back(object);
}

void World::generateContacts() {

    cout << "[objects] " << objects.size() << endl;
    for(auto& object1 : objects) {
        for(auto& object2 : objects) {
            if(object1 != object2) {
                for(auto& prim1 : object1->getPrimitives()) {
                    for(auto& prim2 : object2->getPrimitives()) {
                        auto type1 = prim1->getType();
                        auto type2 = prim2->getType();

                        cout << " type 1 " << type1 << endl;
                        cout << " type 2 " << type2 << endl;

                        if(type1 == primitivesTypes::BOX && type2 == primitivesTypes::PLANE) {
                            auto cast1 = std::dynamic_pointer_cast<Box>(prim1);
                            auto cast2 = std::dynamic_pointer_cast<Plane>(prim2);
                            cout << " cas 1 before " << endl;

                            collisionDetector->generateCollision(*cast1, *cast2, collisionData);
                            cout << " cas 1 after " << endl;
                        } else if(type1 == primitivesTypes::PLANE && type2 == primitivesTypes::BOX) {
                            auto cast1 = std::dynamic_pointer_cast<Plane>(prim1);
                            auto cast2 = std::dynamic_pointer_cast<Box>(prim2);
                            cout << " cas 2 before " << endl;

                            collisionDetector->generateCollision(*cast2, *cast1, collisionData);
                            cout << " cas 2 after " << endl;

                        } else {
                            cout << " cas 3 " << endl;

                            //Not implemented yet
                        }
                    }
                }
            }
        }
    }
}

