#include "ecm/Entity.h"
#include "ecm/Component.h"

Entity::Entity() : components(), alive(true), visible(true), forDeletion(false) {}

Entity::~Entity() = default;

void Entity::update(double dt) {
    auto it = components.begin();
    while(it != components.end()) {
        auto& component = *it;
        if(component->isForDeletion())
            it = components.erase(it);
        else {
            if(component->isAlive())
                component->update(dt);
            ++it;
        }
    }
}

void Entity::render() const {
    for(const auto& component : components) {
        if(component->isVisible())
            component->render();
    }
}

bool Entity::isAlive() const {
    return alive;
}

void Entity::setAlive(bool alive) {
    this->alive = alive;
}

bool Entity::isVisible() const {
    return visible;
}

void Entity::setVisible(bool visible) {
    this->visible = visible;
}

bool Entity::isForDeletion() const {
    return forDeletion;
}

void Entity::setForDeletion() {
    this->forDeletion = true;
}
