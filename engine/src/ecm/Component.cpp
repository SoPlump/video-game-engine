#include "ecm/Component.h"

Component::~Component() = default;

bool Component::isAlive() const {
    return alive;
}

void Component::setAlive(bool alive) {
    this->alive = alive;
}

bool Component::isVisible() const {
    return visible;
}

void Component::setVisible(bool visible) {
    this->visible = visible;
}

bool Component::isForDeletion() const {
    return forDeletion;
}

void Component::setForDeletion() {
    this->forDeletion = true;
}

Component::Component(Entity* const parent) : parent(parent), alive(true), visible(true), forDeletion(false) {}
