#include "Object.h"
#include <vector>
#include "system/Primitive.h"



using namespace engine::physics;
using namespace std;

Object::Object(Entity* const entity, Vector3r const center) : Component (entity)
{
	this->center = center;
}

void Object::addPrimitive(std::shared_ptr<Primitive> primitive) {
	primitives.push_back(primitive);
}

std::vector< std::shared_ptr<Primitive>> Object::getPrimitives() {
	return primitives;
}

void Object::update(double dt) {
	for (vector<shared_ptr<Primitive>>::iterator prim = primitives.begin();
		prim != primitives.end(); prim++) {
		prim->get()->getBody()->update(dt);
	}
}

void Object::render() const {

}

void Object::integrate(double dt) {
	for (vector<shared_ptr<Primitive>>::iterator prim = primitives.begin();
		prim != primitives.end(); prim++) {
		prim->get()->getBody()->integrate(dt);
	}
}

