#include "utils/Chrono.h"
#include <chrono>

using namespace std;

Chrono::Chrono() {
    const auto now = chrono::system_clock::now();
    start = now;
    previous = now;
}

double Chrono::time() const {
    auto now = chrono::system_clock::now();
    return chrono::duration_cast<chrono::duration<double>>(now - start).count();
}

double Chrono::delta() {
    auto now = chrono::system_clock::now();
    double dt = chrono::duration_cast<chrono::duration<double>>(now - previous).count();
    previous = now;
    return dt;
}

void Chrono::reset() {
    auto now = chrono::system_clock::now();
    start = now;
    previous = now;
}
