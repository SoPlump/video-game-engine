#include "utils/utils.h"
#include "logging.h"
#include <string>
#include <fstream>
#include <sstream>
#include <memory>
#include <vector>

using namespace std;
using namespace engine::physics;

vector<string> split(std::string& s, char delimiter) {
    vector<string> result;
    string token;
    for(char c : s) {
        if(c == delimiter) {
            if(!token.empty()) {
                result.push_back(token);
                token.clear();
            }
        }
        else
            token += c;
    }

    if(!token.empty())
        result.push_back(token);
    return result;
}

shared_ptr<string> readFile(const string& path) {
    auto res = make_shared<string>();
    ifstream in(path);

    if(!in.is_open()) {
        string message = "Could not read file " + path + ' ';
        throw ios_base::failure(message);
    }

    in.exceptions(ifstream::failbit | ifstream::badbit);

    stringstream inStream;
    inStream << in.rdbuf();
    in.close();
    res->assign(inStream.str());

    return res;
}

//TODO :ici ?
/*
* Transform a given world vector to a local vector
*/
Vector3r localToWorld(const Vector3r& local, const Matrix4r& transformMatrix)
{
    return transformMatrix.transform(local);
}

/*
* Transform a given local vector to a world vector
*/
Vector3r worldToLocal(const Vector3r& world, const Matrix4r& transformMatrix)
{
    Matrix4r inverseTransform;
    inverseTransform = transformMatrix.inverse();
    return inverseTransform.transform(world);
}