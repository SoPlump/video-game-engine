#include "utils/Tensors.h"


// Returns a cube inertia tensor
engine::physics::Matrix3r getCube(engine::physics::real mass, engine::physics::Vector3r dims) {

	engine::physics::Matrix3r result;
	result[0][0] = (1.0f / 12.0f) * mass * (dims.y * dims.y + dims.z * dims.z);
	result[1][1] = (1.0f / 12.0f) * mass * (dims.x * dims.x + dims.z * dims.z);
	result[2][2] = (1.0f / 12.0f) * mass * (dims.x * dims.x + dims.y * dims.y);

	return result;
}

// Returns a cylinder inertia tensor
engine::physics::Matrix3r getCylinder(engine::physics::real mass, engine::physics::real height, engine::physics::real radius) {

	engine::physics::Matrix3r result;
	result[0][0] = (1.0f / 12.0f) * mass * (height * height) + (1.0f / 4.0f) * mass * (radius * radius);
	result[1][1] = (1.0f / 2.0f) * mass * (radius * radius);
	result[2][2] = (1.0f / 12.0f) * mass * (height * height) + (1.0f / 4.0f) * mass * (radius * radius);

	return result;
}

