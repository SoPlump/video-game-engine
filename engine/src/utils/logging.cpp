#include "logging.h"
#include "utils/utils.h"
#include <iostream>
#include <stdexcept>
#include <boost/log/core/core.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/utility/exception_handler.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;

// define boost log attributes shorcuts
BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime)

// a set of macros for ansi escape sequences
#define CSI "\u001b["
#define BOLD "1"
#define GREEN "32"
#define YELLOW "33"
#define MAGENTA "35"
#define CYAN "36"
#define BRIGHT_BLACK "90"
#define BRIGHT_RED "91"
#define RESET CSI"0m"

// a set of macros for formatting log records depending on severity
#define TRACE_COLOR CSI BRIGHT_BLACK "m"
#define DEBUG_COLOR CSI CYAN "m"
#define INFO_COLOR CSI GREEN "m"
#define WARNING_COLOR CSI YELLOW "m"
#define ERROR_COLOR CSI BRIGHT_RED "m"
#define OPENGL_COLOR CSI MAGENTA "m"
#define FATAL_COLOR CSI BRIGHT_RED ";" BOLD "m"

static const auto dateTimeFormatter = expr::stream << expr::format_date_time(timestamp, "%Y-%m-%d %H:%M:%S");

// Function object used to handle internal logging exception
struct ExceptionHandler {
    void operator()(const std::runtime_error& e) const {
        std::cerr << "Runtime error : " << e.what() << std::endl;
    }

    void operator()(const std::logic_error& e) const {
        std::cerr << "Logic error : " << e.what() << std::endl;
    }

    void operator()() const {
        std::cerr << "Unknown exception" << std::endl;
    }
};

// Function used to format log records
void formatRecord(const logging::record_view& rec, logging::formatting_ostream& stream) {
    auto severity = rec[logging::trivial::severity];

#ifndef NDEBUG
    auto opengl = logging::extract<bool>("OpenGL", rec);
#endif

#if defined(__linux__) && defined(LOG_COLORS)
    if(severity) {
        switch(severity.get()) {
            case logging::trivial::trace:
                stream << TRACE_COLOR;
                break;
            case logging::trivial::debug:
                stream << DEBUG_COLOR;
                break;
            case logging::trivial::info:
#ifndef NDEBUG
                if(opengl)
                    stream << OPENGL_COLOR;
                else
#endif
                stream << INFO_COLOR;
                break;
            case logging::trivial::warning:
                stream << WARNING_COLOR;
                break;
            case logging::trivial::error:
                stream << ERROR_COLOR;
                break;
            case logging::trivial::fatal:
                stream << FATAL_COLOR;
                break;
        }
    }
#endif

#ifndef NDEBUG
    if(opengl) {
        auto source = logging::extract<std::string>("source", rec);
        auto type = logging::extract<std::string>("type", rec);
        stream << "[OpenGL - " << source << " - " << type << "] ";
    }
    else {
#endif
        stream << '[' << severity << "] ";
#ifndef NDEBUG
    }
#endif

    dateTimeFormatter(rec, stream);
    stream << " - ";

    auto file = logging::extract<std::string>("File", rec);
    auto line = logging::extract<int>("Line", rec);

    if(file) {
        int idx = file.get().rfind('/') + 1;
        stream << '(' << (file.get().c_str() + idx);
        if(line)
            stream << ':' << line;
        stream << ") - ";
    }

    auto message = logging::extract<std::string>("Message", rec).get();
    trim(message);

    stream << message;

#if defined(__linux__) && defined(LOG_COLORS)
    if(severity)
        stream << RESET;
#endif
}

BOOST_LOG_GLOBAL_LOGGER_INIT(logger, logger_t) {
    // create logger and add attributes
    logger_t lg;
    lg.add_attribute("TimeStamp", attrs::local_clock());

    // create a text sink
    typedef sinks::synchronous_sink<sinks::text_ostream_backend> text_sink;
    auto sink = boost::make_shared<text_sink>();
    sink->locked_backend()->auto_flush(true);

    // create a stream to send logs to standard output
    boost::shared_ptr<std::ostream> stream(&std::cout, boost::null_deleter());
    sink->locked_backend()->add_stream(stream);

    // register record formatter
    sink->set_formatter(&formatRecord);

    // register sink
    logging::core::get()->add_sink(sink);

    // add severity threshold filter
    logging::core::get()->set_filter(logging::trivial::severity >= SEVERITY_THRESHOLD);

    // register exception handler
    auto exceptionHandler = logging::make_exception_handler<std::runtime_error, std::logic_error>(ExceptionHandler());
    logging::core::get()->set_exception_handler(exceptionHandler);

    return lg;
}
