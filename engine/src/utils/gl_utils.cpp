#include "utils/gl_utils.h"
#include "utils/utils.h"
#include "logging.h"
#include "exception/ShaderCompileError.h"
#include "exception/ShaderLinkError.h"
#include <memory>
#include <string>

using namespace std;

string glEnumToString(GLenum glEnum) {
    switch(glEnum) {
        case GL_VERTEX_SHADER:
            return "GL_VERTEX_SHADER";
        case GL_FRAGMENT_SHADER:
            return "GL_FRAGMENT_SHADER";
        case GL_TEXTURE_1D:
            return "GL_TEXTURE_1D";
        case GL_TEXTURE_2D:
            return "GL_TEXTURE_2D";
        case GL_TEXTURE_3D:
            return "GL_TEXTURE_3D";
        case GL_TEXTURE_1D_ARRAY:
            return "GL_TEXTURE_1D_ARRAY";
        case GL_TEXTURE_2D_ARRAY:
            return "GL_TEXTURE_2D_ARRAY";
        case GL_TEXTURE_RECTANGLE:
            return "GL_TEXTURE_RECTANGLE";
        case GL_TEXTURE_CUBE_MAP:
            return "GL_TEXTURE_CUBE_MAP";
        case GL_TEXTURE_CUBE_MAP_ARRAY:
            return "GL_TEXTURE_CUBE_MAP_ARRAY";
        case GL_TEXTURE_BUFFER:
            return "GL_TEXTURE_BUFFER";
        case GL_TEXTURE_2D_MULTISAMPLE:
            return "GL_TEXTURE_2D_MULTISAMPLE";
        case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
            return "GL_TEXTURE_2D_MULTISAMPLE_ARRAY";
        default:
            return "Unimplemented GLEnumToStr value " + std::to_string(glEnum);
    }
}

GLuint loadShaderFromFile(const std::string& path, GLenum type) {
    DEBUG << "Loading " << glEnumToString(type) << " from file : " << path;

    // read shader source from file
    auto source = readFile(path);
    const char* cSource = source->c_str();

    TRACE << "Shader source :\n" << cSource;

    // create and compile shader
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &cSource, nullptr);
    glCompileShader(shader);

    // check shader compile status
    GLint compileOk = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileOk);

    // throw exception if compilation failed
    if(!compileOk)
        throw ShaderCompileError(shader);

    DEBUG << "Loaded " << glEnumToString(type) << " from file : " << path << ", id=" << shader;

    return shader;
}

GLuint createProgram(GLuint vertexShader, GLuint fragmentShader) {
    DEBUG << "Creating shader program, vs=" << vertexShader << ", fs=" << fragmentShader;

    // create program
    GLuint program = glCreateProgram();

    // attach shaders
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    // link program
    glLinkProgram(program);

    // check program link status
    GLint linkOk = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linkOk);

    // print error message if link failed
    if(!linkOk)
        throw ShaderLinkError(program);

    DEBUG << "Created shader program, id=" << program;

    return program;
}
