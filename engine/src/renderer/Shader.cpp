#include "renderer/Shader.h"
#include "utils/gl_utils.h"

using namespace engine;

renderer::Shader::Shader(const std::string& vertexPath, const std::string& fragmentPath) {
    GLuint vertexShader = loadShaderFromFile(vertexPath, GL_VERTEX_SHADER);
    GLuint fragmentShader = loadShaderFromFile(fragmentPath, GL_FRAGMENT_SHADER);
    GLuint program = createProgram(vertexShader, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    id = program;
}

renderer::Shader::~Shader() {
    glDeleteProgram(id);
}

void renderer::Shader::use() const {
    glUseProgram(id);
}

GLuint renderer::Shader::programId() const {
    return id;
}

void renderer::Shader::setUniform(const std::string& name, float value) const {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniform1f(location, value);
}

void renderer::Shader::setUniform(const std::string& name, bool value) const {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniform1i(location, int(value));
}

void renderer::Shader::setUniform(const std::string& name, int value) const {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniform1i(location, value);
}

template<>
void renderer::Shader::setUniformVector<2>(const std::string& name, const float* value, int count) {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniform2fv(location, count, value);
}

template<>
void renderer::Shader::setUniformVector<3>(const std::string& name, const float* value, int count) {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniform3fv(location, count, value);
}

template<>
void renderer::Shader::setUniformVector<4>(const std::string& name, const float* value, int count) {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniform4fv(location, count, value);
}

template<>
void renderer::Shader::setUniformMatrix<2>(const std::string& name, const float* value, int count) {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniformMatrix2fv(location, count, GL_FALSE, value);
}

template<>
void renderer::Shader::setUniformMatrix<3>(const std::string& name, const float* value, int count) {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniformMatrix3fv(location, count, GL_FALSE, value);
}

template<>
void renderer::Shader::setUniformMatrix<4>(const std::string& name, const float* value, int count) {
    GLuint location = glGetUniformLocation(id, name.c_str());
    glUniformMatrix4fv(location, count, GL_FALSE, value);
}
