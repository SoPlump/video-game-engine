#include "renderer/GLState.h"
#include <GL/glew.h>

using namespace engine;

renderer::GLState::GLState() = default;

renderer::GLState::~GLState() = default;

void renderer::GLState::setActiveVao(GLuint vao) {
    if(vao != activeVao) {
        activeVao = vao;
        glBindVertexArray(vao);
    }
}

void renderer::GLState::setActiveShader(GLuint id) {
    if(id != activeShaderId) {
        activeShaderId = id;
        glUseProgram(id);
    }
}
