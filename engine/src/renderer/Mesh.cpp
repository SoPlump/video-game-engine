#include "renderer/Mesh.h"
#include <GL/glew.h>

using namespace std;
using namespace engine;

static constexpr int POSITION_SIZE = 3;
static constexpr int UV_SIZE = 2;
static constexpr int NORMAL_SIZE = 3;

enum {
    MESH_POSITION = 0,
    MESH_UV,
    MESH_NORMAL
};

renderer::Mesh::Mesh(GLenum primitive) :
    vao(0),
    primitive(primitive),
    indexed(false)
    {}

renderer::Mesh::~Mesh() {
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ebo);
}

void renderer::Mesh::setPositions(const vector<float>& data) {
    positions.clear();
    const size_t size = data.size();
    positions.reserve(size / POSITION_SIZE);
    for(size_t i = 0; i < size; i += POSITION_SIZE)
        positions.emplace_back(data[i], data[i + 1], data[i + 2]);
}

void renderer::Mesh::setUVs(const std::vector<float>& data){
    uvs.clear();
    const size_t size = data.size();
    uvs.reserve(size / UV_SIZE);
    for(size_t i = 0; i < size; i += UV_SIZE)
        uvs.emplace_back(data[i], data[i + 1]);
}

void renderer::Mesh::setNormals(const std::vector<float>& data){
    normals.clear();
    const size_t size = data.size();
    normals.reserve(size / NORMAL_SIZE);
    for(size_t i = 0; i < size; i += NORMAL_SIZE)
        normals.emplace_back(data[i], data[i + 1], data[i + 2]);
}

void renderer::Mesh::setIndices(const vector<unsigned>& data) {
    indexed = true;
    indices = data;
}

void renderer::Mesh::finalize() {
    // create interleaved data array
    vector<float> data;
    for(size_t i = 0; i < positions.size(); i++) {
        data.push_back(positions[i].x);
        data.push_back(positions[i].y);
        data.push_back(positions[i].z);

        if(!uvs.empty()) {
            data.push_back(uvs[i].x);
            data.push_back(uvs[i].y);
        }

        if(!normals.empty()) {
            data.push_back(normals[i].x);
            data.push_back(normals[i].y);
            data.push_back(normals[i].z);
        }
    }

    // create vao and vbo
    if(vao == 0) {
        glGenVertexArrays(1, &vao);
        glGenBuffers(1, &vbo);
    }

    // bind
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    // put data
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);

    // configure attributes
    size_t stride = POSITION_SIZE * sizeof(float);
    if(!uvs.empty())
        stride += UV_SIZE * sizeof(float);
    if(!normals.empty())
        stride += NORMAL_SIZE * sizeof(float);

    size_t offset = 0;

    glVertexAttribPointer(MESH_POSITION, POSITION_SIZE, GL_FLOAT, GL_FALSE, stride, (void*)offset);
    glEnableVertexAttribArray(MESH_POSITION);
    offset += POSITION_SIZE * sizeof(float);

    if(!uvs.empty()) {
        glVertexAttribPointer(MESH_UV, UV_SIZE, GL_FLOAT, GL_FALSE, stride, (void*)offset);
        glEnableVertexAttribArray(MESH_UV);
        offset += UV_SIZE * sizeof(float);
    }

    if(!normals.empty()) {
        glVertexAttribPointer(MESH_NORMAL, NORMAL_SIZE, GL_FLOAT, GL_FALSE, stride, (void*)offset);
        glEnableVertexAttribArray(MESH_NORMAL);
        offset += NORMAL_SIZE * sizeof(float);
    }

    // configure ebo
    if(!indices.empty()) {
        glGenBuffers(1, &ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned), &indices[0], GL_STATIC_DRAW);
    }

    // unbind everything
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // set vertex count
    vertexCount = indices.empty() ? positions.size() : indices.size();

    // clear data from main memory
    positions.clear();
    uvs.clear();
    normals.clear();
    indices.clear();
}

GLuint renderer::Mesh::getVao() const {
    return vao;
}

unsigned renderer::Mesh::getVertexCount() const {
    return vertexCount;
}

bool renderer::Mesh::isIndexed() const {
    return indexed;
}

GLenum renderer::Mesh::getPrimitive() const {
    return primitive;
}
