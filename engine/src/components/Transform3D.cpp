#include "components/Transform3D.h"

const glm::vec3 Transform3D::AXIS_X(1.0f, 0.0f, 0.0f);
const glm::vec3 Transform3D::AXIS_Y(0.0f, 1.0f, 0.0f);
const glm::vec3 Transform3D::AXIS_Z(0.0f, 0.0f, 1.0f);

Transform3D::Transform3D(Entity* const parent) :
    Component(parent),
    position(0.0f),
    rotation(),
    scale_(1.0f)
    {}

Transform3D::~Transform3D() = default;

void Transform3D::update(double dt) {
    // do nothing
}

void Transform3D::render() const {
    // do nothing
}

glm::mat4 Transform3D::getTransform() {
    glm::mat4 scale = glm::scale(glm::mat4(1.0f), scale_);
    glm::mat4 translation = glm::translate(glm::mat4(1.0f), position);
    glm::mat4 orientation = glm::toMat4(rotation);
    return translation * orientation * scale * glm::mat4(1.0f);
}

glm::vec3 Transform3D::getPosition() const {
    return position;
}

float Transform3D::getPositionX() const {
    return position[0];
}

float Transform3D::getPositionY() const {
    return position[1];
}

float Transform3D::getPositionZ() const {
    return position[2];
}

void Transform3D::setPosition(const glm::vec3& position) {
    this->position[0] = position[0];
    this->position[1] = position[1];
    this->position[2] = position[2];
}

void Transform3D::setPositionX(float x) {
    this->position[0] = x;
}

void Transform3D::setPositionY(float y) {
    this->position[1] = y;
}

void Transform3D::setPositionZ(float z) {
    this->position[2] = z;
}

void Transform3D::move(const glm::vec3& movement) {
    position[0] += movement[0];
    position[1] += movement[1];
    position[2] += movement[2];
}

glm::vec3 Transform3D::getScale() const {
    return scale_;
}

float Transform3D::getScaleX() const {
    return scale_[0];
}

float Transform3D::getScaleY() const {
    return scale_[1];
}

float Transform3D::getScaleZ() const {
    return scale_[2];
}

void Transform3D::setScale(const glm::vec3& scale) {
    scale_[0] = scale[0];
    scale_[1] = scale[1];
    scale_[2] = scale[2];
}

void Transform3D::setScaleX(float scaleX) {
    scale_[0] = scaleX;
}

void Transform3D::setScaleY(float scaleY) {
    scale_[1] = scaleY;
}

void Transform3D::setScaleZ(float scaleZ) {
    scale_[2] = scaleZ;
}

void Transform3D::scale(const glm::vec3& scale) {
    scale_[0] *= scale[0];
    scale_[1] *= scale[1];
    scale_[2] *= scale[2];
}

glm::vec3 Transform3D::getRotation() const {
    return glm::eulerAngles(rotation);
}

float Transform3D::getRotationX() const {
    return glm::pitch(rotation);
}

float Transform3D::getRotationY() const {
    return glm::yaw(rotation);
}

float Transform3D::getRotationZ() const {
    return glm::roll(rotation);
}

glm::quat Transform3D::getQuaternion() const {
    return rotation;
}

void Transform3D::setRotation(const glm::vec3& rotation) {
    this->rotation = glm::quat(rotation);
}

void Transform3D::setRotationX(float rotationX) {
    rotation = glm::quat(glm::vec3(rotationX, glm::yaw(rotation), glm::roll(rotation)));
}

void Transform3D::setRotationY(float rotationY) {
    rotation = glm::quat(glm::vec3(glm::pitch(rotation), rotationY, glm::roll(rotation)));
}

void Transform3D::setRotationZ(float rotationZ) {
    rotation = glm::quat(glm::vec3(glm::pitch(rotation), glm::yaw(rotation), rotationZ));
}

void Transform3D::setQuaternion(const glm::quat& rotation) {
    this->rotation = rotation;
}

void Transform3D::rotate(const glm::vec3& rotation) {
    this->rotation = glm::normalize(glm::quat(rotation) * this->rotation);
}

void Transform3D::lookAt(const glm::vec3& point) {
    rotation = glm::quatLookAt(point, AXIS_Y);
}

glm::vec3 Transform3D::right() const {
    return rotation * AXIS_X;
}

glm::vec3 Transform3D::up() const {
    return rotation * AXIS_Y;
}

glm::vec3 Transform3D::front() const {
    return rotation * -AXIS_Z;
}
