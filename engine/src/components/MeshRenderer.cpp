#include "components/MeshRenderer.h"
#include "components/Camera.h"

using namespace std;
using namespace engine;

MeshRenderer::MeshRenderer(
    Entity* parent,
    shared_ptr<renderer::Mesh> mesh,
    shared_ptr<renderer::Shader> shader,
    shared_ptr<renderer::GLState> glState
) : Component(parent), mesh(std::move(mesh)), shader(std::move(shader)), glState(std::move(glState)) {
    transform = this->parent->getComponent<Transform3D>();
}

MeshRenderer::~MeshRenderer() = default;

void MeshRenderer::update(double dt) {
    // do nothing
}

void MeshRenderer::render() const {
    // get camera
    auto camera = Camera::mainCamera;
    assert(camera != nullptr);

    // enable shader
    glState->setActiveShader(shader->programId());

    // set mvp uniforms
    shader->setUniformMatrix<4>("projection", camera->getProjectionMatrix());
    shader->setUniformMatrix<4>("view", camera->getViewMatrix());
    shader->setUniformMatrix<4>("model", transform->getTransform());

    // enable and draw mesh
    glState->setActiveVao(mesh->getVao());
    if(mesh->isIndexed())
        glDrawElements(mesh->getPrimitive(), mesh->getVertexCount(), GL_UNSIGNED_INT, nullptr);
    else
        glDrawArrays(mesh->getPrimitive(), 0, mesh->getVertexCount());
}
