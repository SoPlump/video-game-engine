#include "components/OrthographicCamera.h"
#include "system.h"

using namespace engine;

OrthographicCamera::OrthographicCamera(Entity* const parent, float viewDistance, float zNear, float zFar) : Camera(parent),  viewDistance(viewDistance), zNear(zNear), zFar(zFar) {
    aspectRatio = system::aspectRatio();
    projection = glm::ortho(
        -aspectRatio * viewDistance,
        aspectRatio * viewDistance,
        -viewDistance,
        viewDistance,
        zNear,
        zFar
    );
}

OrthographicCamera::~OrthographicCamera() = default;

void OrthographicCamera::update(double dt) {
    Camera::update(dt);
    if(system::aspectRatio() != aspectRatio) {
        aspectRatio = system::aspectRatio();
        projection = glm::ortho(
            -aspectRatio * viewDistance,
            aspectRatio * viewDistance,
            -viewDistance,
            viewDistance,
            zNear,
            zFar
        );
    }
}

const glm::mat4& OrthographicCamera::getProjectionMatrix() const {
    return projection;
}
