#include "components/PerspectiveCamera.h"
#include "system.h"

using namespace engine;

PerspectiveCamera::PerspectiveCamera(Entity* const parent, float fovy, float zNear, float zFar) : Camera(parent), fovy(fovy), zNear(zNear), zFar(zFar) {
    aspectRatio = system::aspectRatio();
    projection = glm::perspective(fovy, system::aspectRatio(), zNear, zFar);
}

PerspectiveCamera::~PerspectiveCamera() = default;

void PerspectiveCamera::update(double dt) {
    Camera::update(dt);
    if(system::aspectRatio() != aspectRatio) {
        aspectRatio = system::aspectRatio();
        projection = glm::perspective(fovy, system::aspectRatio(), zNear, zFar);
    }
}

const glm::mat4& PerspectiveCamera::getProjectionMatrix() const {
    return projection;
}
