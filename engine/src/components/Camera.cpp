#include "components/Camera.h"

Camera* Camera::mainCamera = nullptr;

Camera::Camera(Entity* const parent) : Component(parent) {
    transform = parent->getComponent<Transform3D>();
    if(mainCamera == nullptr)
        mainCamera = this;
}

Camera::~Camera() = default;

void Camera::update(double dt) {

}

void Camera::render() const {

}

glm::mat4 Camera::getViewMatrix() const {
    glm::mat4 viewMatrix = glm::inverse(transform->getTransform());
    return viewMatrix;
}
