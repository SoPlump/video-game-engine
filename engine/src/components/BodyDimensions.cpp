#include "../../include/components/BodyDimensions.h"

using namespace std;
using namespace engine::physics;

BodyDimensions::BodyDimensions(Entity* const parent, BodyDimensions::Geometry geometry, Vector3r dimensions) : Component(parent) {
	this->geometry = geometry;
	this->dimensions = dimensions;
}

void BodyDimensions::update(double dt) {}

void BodyDimensions::render() const {}