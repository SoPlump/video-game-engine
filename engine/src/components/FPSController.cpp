#include "components/FPSController.h"

#include "system.h"
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

using namespace engine;

FPSController::FPSController(
    Entity* parent,
    float baseSpeed,
    float speedMultiplier,
    float sensitivity
) : Component(parent), baseSpeed(baseSpeed),
    speedMultiplier(speedMultiplier), sensitivity(sensitivity)
{
    transform = parent->getComponent<Transform3D>();
}

FPSController::~FPSController() = default;

void FPSController::update(double dt) {

    if (system::isKeyPressed(SDLK_l))
        enabled = !enabled;

    if (!enabled)
        return;

    glm::vec3 movement(0.0f);

    float speed = baseSpeed;

    if(system::isKeyPressed(SDLK_LSHIFT))
        speed *= speedMultiplier;

    if(system::isKeyPressed(SDLK_z))
        movement += float(speed * dt) * transform->front();

    if(system::isKeyPressed(SDLK_q))
        movement -= float(speed * dt) * glm::normalize(glm::cross(transform->front(), Transform3D::AXIS_Y));

    if(system::isKeyPressed(SDLK_s))
        movement -= float(speed * dt) * transform->front();

    if(system::isKeyPressed(SDLK_d))
        movement += float(speed * dt) * glm::normalize(glm::cross(transform->front(), Transform3D::AXIS_Y));

    if(system::isKeyPressed(SDLK_SPACE))
        movement += float(speed * dt) * Transform3D::AXIS_Y;

    if(system::isKeyPressed(SDLK_LCTRL))
        movement -= float(speed * dt) * Transform3D::AXIS_Y;

    transform->setPosition(transform->getPosition() + movement);

    int middleX = system::width() / 2;
    int middleY = system::height() / 2;
    float mouseOffsetX = float(system::getMouseX() - middleX) * sensitivity;
    float mouseOffsetY = -float(system::getMouseY() - middleY) * sensitivity;

    pitch += mouseOffsetY;
    yaw += mouseOffsetX;

    if(pitch < -89)
        pitch = -89;
    if(pitch > 89)
        pitch = 89;

    glm::vec3 direction(0.0f);

    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));

    transform->lookAt(glm::normalize(direction));
    SDL_WarpMouseInWindow(system::window(), system::width() / 2, system::height() / 2);
}

void FPSController::render() const {}
