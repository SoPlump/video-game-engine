#include <pfgen.h>
#include "Vector.h"
#include <cmath>
#include <algorithm>

using namespace engine::physics;
using namespace std;

//test

ParticleForceRegistry::ParticleForceRegistry() {}

//*****
void ParticleForceRegistry::updateForces(real duration) {
	Registry::iterator i = registrations.begin();
	for (; i != registrations.end(); i++) {
		i->fg->updateForce(i->particle, duration);
	}	
}

void ParticleForceRegistry::add(shared_ptr<Particle> particle, shared_ptr<ParticleForceGenerator> fg) {
	registrations.push_back({ particle, fg });
}

void ParticleForceRegistry::remove(shared_ptr<Particle> particle, shared_ptr<ParticleForceGenerator> fg) {
	ParticleForceRegistration RegToRem = { particle, fg };
	std::remove(registrations.begin(), registrations.end(), RegToRem);
}

void ParticleForceRegistry::clear() {
	registrations.clear();
}

ParticleGravity::ParticleGravity(const Vector3r& gravity) {
	this->gravity = gravity;
}

void ParticleGravity::updateForce(shared_ptr<Particle> particle, real duration) {
	//Check that we do not have infinite mass.
	if (!particle->hasFiniteMass()) return;

	//Apply the mass-scaled force to the particle.
	particle->addForce(gravity * particle->getMass());
}

ParticleDrag::ParticleDrag(real k1, real k2) {
	this->k1 = k1;
	this->k2 = k2;
}

void ParticleDrag::updateForce(shared_ptr<Particle> particle, real duration) {
	Vector3r force;
	force = particle->getVelocity();

	// Calculate the total drag coefficient 
	real dragCoeff = force.magnitude();
	dragCoeff = k1 * dragCoeff + k2 * dragCoeff * dragCoeff;

	// Calculate the final force to apply it
	force.normalize();
	force *= -dragCoeff;
	particle->addForce(force);
}

ParticleSpring::ParticleSpring(shared_ptr<Particle> other, real springConstant, real restLength) {
	this->other = other;
	this->springConstant = springConstant;
	this->restLength = restLength;
}

void ParticleSpring::updateForce(shared_ptr<Particle> particle, real duration) 
{
	// Calculate the vector of the spring.
	Vector3r force;
	force = particle->getPosition();
	force = force - other->getPosition();

	// Calculate the magnitude of the force.
	real magnitude = force.magnitude();
	magnitude = magnitude - restLength;
	magnitude *= springConstant;

	// Calculate the final force and apply it.
	force.normalize();
	force *= -magnitude;
	particle->addForce(force);
}
ParticleSoftSpring::ParticleSoftSpring(shared_ptr<Particle> other, real springConstant, real restLength, real maxLength) {
	this->other = other;
	this->springConstant = springConstant;
	this->restLength = restLength;
	this->maxLength = maxLength;
}

void ParticleSoftSpring::updateForce(shared_ptr<Particle> particle, real duration)
{
	// Calculate the vector of the spring.
	Vector3r force;
	force = particle->getPosition();
	force = force - other->getPosition();

	// Calculate the magnitude of the force.
	real magnitude = force.magnitude();

	// Handle max length of the rope -> no more force
	if (magnitude >= maxLength) {
		return;
	}

	magnitude = magnitude - restLength;
	magnitude *= springConstant;


	// Calculate the final force and apply it.
	force.normalize();
	force *= -magnitude;
	particle->addForce(force);
}

ParticleAnchoredSpring::ParticleAnchoredSpring(shared_ptr<Vector3r> anchor, real springConstant, real restLength) 
: anchor(anchor), springConstant(springConstant), restLength(restLength) {
}

void ParticleAnchoredSpring::updateForce(std::shared_ptr < engine::physics::Particle> particle, real
	duration)
{
	// Calculate the vector of the spring.
	Vector3r force;
	force = particle->getPosition();
	force = force - *anchor;

	// Calculate the magnitude of the force.
	real magnitude = force.magnitude();
	magnitude = (restLength - magnitude) * springConstant;

	// Calculate the final force and apply it.
	force.normalize();
	force *= magnitude;
	particle->addForce(force);
}