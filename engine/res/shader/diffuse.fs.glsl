#version 330 core

in vec3 fragPosition;
in vec3 normal;

uniform vec3 color;
uniform vec3 lightPosition;
uniform vec3 lightColor;

out vec4 fragColor;

void main() {
    float ambientStrength = 0.5;
    vec3 ambient = ambientStrength * lightColor;

    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPosition - fragPosition);

    float diff = max(dot(norm, lightDir), 0.0f) * 10.0f;
    vec3 diffuse = diff * vec3(1.0f, 1.0f, 1.0f);

    vec3 result = (ambient + diffuse) * color;
    fragColor = vec4(result, 1.0);
}
