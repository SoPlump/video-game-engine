#version 330 core

layout (location = 0) in vec3 aPosition;
layout (location = 2) in vec3 aNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 fragPosition;
out vec3 normal;

void main() {
    vec4 position = projection * view * model * vec4(aPosition, 1.0f);
    position.z = 1.0f;
    gl_Position = position;
    fragPosition = vec3(model * vec4(aPosition, 1.0f));
    normal = mat3(transpose(inverse(model))) * aNormal;
}
