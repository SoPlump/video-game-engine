#include "WeaponFactory.h"



WeaponFactory::WeaponFactory() = default;

constexpr float lifetime = 3.0f;

std::shared_ptr<Entity> WeaponFactory::makeBullet(WeaponType type) {
    switch (type) {
    case GUN: {
        auto entity = std::make_shared<Entity>();
        entity->addComponent<TimeoutKill>(lifetime);
        auto particle = entity->addComponent < engine::physics::Particle >();
        particle->setMass(5.0f);
        particle->setVelocity(engine::physics::Vector3f(0.0f, 0.0f, 30.0f));
        particle->setAcceleration(engine::physics::Vector3f(0.0f, -1.0f, 0.0f));
        particle->setPosition(engine::physics::Vector3f(0.0f, 0.5f, 0.0f));
        return entity;
    }
    case CANNON: {
        auto entity = std::make_shared<Entity>();
        entity->addComponent<TimeoutKill>(lifetime);
        auto particle = entity->addComponent < engine::physics::Particle >();
        particle->setMass(100.0f);
        particle->setVelocity(engine::physics::Vector3f(0.0f, 30.0f, 30.0f));
        particle->setAcceleration(engine::physics::Vector3f(0.0f, -15.0f, 0.0f));
        particle->setPosition(engine::physics::Vector3f(0.0f, 0.5f, 0.0f));
        return entity;
    }
    case LASER: {
        auto entity = std::make_shared<Entity>();
        entity->addComponent<TimeoutKill>(lifetime);
        auto particle = entity->addComponent < engine::physics::Particle >();
        particle->setMass(0.1f);
        particle->setVelocity(engine::physics::Vector3f(0.0f, 0.0f, 100.0f));
        particle->setAcceleration(engine::physics::Vector3f(0.0f, -1.0f, 0.0f));
        particle->setPosition(engine::physics::Vector3f(0.0f, 0.5f, 0.0f));
        return entity;
    }
    case TATA: {
        auto entity = std::make_shared<Entity>();
        entity->addComponent<TimeoutKill>(lifetime);
        auto particle = entity->addComponent < engine::physics::Particle >();
        particle->setMass(50);
        particle->setVelocity(engine::physics::Vector3f(0.0f, 0.0f, 35.0f));
        particle->setAcceleration(engine::physics::Vector3f(0.0f, -1.0f, 0.0f));
        particle->setPosition(engine::physics::Vector3f(0.0f, 0.5f, 0.0f));
        return entity;
    }
    default:
        return nullptr;
    }
}
