#include "../include/CrashSimulator.h"
#include "utils/Tensors.h" 
#include "../include/components/BodyDimensions.h" 
#include "system/RigidBody.h"

#include <renderer/Shader.h>
#include <renderer/Mesh.h>
#include <renderer/GLState.h>
#include <exception/ShaderCompileError.h>
#include <logging.h>
#include <exception/ShaderLinkError.h>
#include <components/Transform3D.h>
#include <components/FPSController.h>
#include <components/PerspectiveCamera.h>
#include <components/MeshRenderer.h>
#include "../include/TimeoutKill.h"
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <system.h>

#include <iostream>

using namespace std;
using namespace engine;
using namespace engine::physics;


static shared_ptr<renderer::Shader> shader = nullptr;
static shared_ptr<renderer::Mesh> mesh = nullptr;
static auto glState = make_shared<renderer::GLState>();

static shared_ptr<Entity> camera;

vector<shared_ptr<Entity>> cars;

constexpr float lifetime = 10.0f;

// How much time passed

// When should they collide
bool collisionHappened = false;

bool launched = false;


static void loadCubeMesh() {
	vector<unsigned> elements = {
		4, 2, 0,
		2, 7, 3,
		6, 5, 7,
		1, 7, 5,
		0, 3, 1,
		4, 1, 5,
		4, 6, 2,
		2, 6, 7,
		6, 4, 5,
		1, 3, 7,
		0, 2, 3,
		4, 0, 1
	};

	vector<float> position = {
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f,  1.0f,  -1.0f,
		-1.0f, 1.0f,  1.0f,
		1.0f,  -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f,  -1.0f,
		1.0f, 1.0f,  1.0f,
	};

	vector<float> texCoords = {
		1.0f, 1.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
	};

	mesh = make_shared<renderer::Mesh>();
	mesh->setPositions(position);
	mesh->setIndices(elements);
	mesh->finalize();
}

static void loadScene()
{
	camera = make_shared<Entity>();
	auto tr = camera->addComponent<Transform3D>();
	tr->setPosition({0.0f, 5.0f, 100.0f});
//	tr->setRotationX(M_PI / 4);
	camera->addComponent<PerspectiveCamera>(float(M_PI / 8.0f), 0.1f, 1000.0f);
	camera->addComponent<FPSController>(10.0f, 3.0f, 0.1f);
}

static bool loadDiffuseShader() {
	try {
		shader = make_shared<renderer::Shader>("shader/diffuse.vs.glsl", "shader/diffuse.fs.glsl");
		shader->use();
		shader->setUniformVector("color", glm::vec3(0.0f, 0.0f, 0.0f));
		shader->setUniformVector("lightPosition", glm::vec3(0.0f, 0.0f, 0.0f));
		shader->setUniformVector("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

		return true;
	}
	catch (const ShaderCompileError & e) {
		FATAL << "Shader compile error : " << e.what();
		glDeleteShader(e.shader);
	}
	catch (const ShaderLinkError & e) {
		FATAL << "Shader link error : " << e.what();
		glDeleteProgram(e.program);
	}
	catch (const exception & e) {
		FATAL << e.what();
	}

	return false;
}

void givePosition()
{
	auto rb1 = cars[0]->getComponent<RigidBody>();
	auto rb2 = cars[1]->getComponent<RigidBody>();


	// Initial physics of the cars' body
	rb1->setPosition(Vector3r(300.0f, 5.0f, 0.0f));

	rb2->setPosition(Vector3r(0.0f, 5.0f, 200.0f));

}

void giveVelocity()
{
	auto rb1 = cars[0]->getComponent<RigidBody>();
	auto rb2 = cars[1]->getComponent<RigidBody>();

	// Initial physics of the cars' body
	rb1->setVelocity(Vector3r(-30.0f, 0.0f, 0.0f));
	//rb1->addForceAtBodyPoint(Vector3r(0, 0, -80000.0f), Vector3r(-8000.0f, 4.0f, -5.0f));

	rb2->setVelocity(Vector3r(0.0f, 0.0f, -20.0f));

}

void crash() {
	//cars[0]->addComponent<TimeoutKill>(lifetime);
	cars[1]->addComponent<TimeoutKill>(lifetime);

	// Add forces to the cars
	auto rb1 = cars[0]->getComponent<RigidBody>();
	rb1->addForceAtBodyPoint(Vector3r(0, 50000.0f, -800000.0f), Vector3r(-0.001f, -2.0f, 0.0f));
	auto rb2 = cars[1]->getComponent<RigidBody>();
	rb2->addForceAtBodyPoint(Vector3r(0.0f, 0.0f, 600000.0f), Vector3r(0.0f, 0.0f, -0.1f));
	//rb1->setAngularVelocity(Vector3r(0.0f, M_PI, 0.0f));
}

void stayAboveGround() {
	for (shared_ptr<Entity> car : cars) {
		auto rb = car->getComponent<RigidBody>();
		Vector3r currentPos = rb->getPosition();
		if (currentPos.y < 0.0f) {
			rb->setPosition(Vector3r(currentPos.x, 0.0f, currentPos.z));
		}
	}
}

// Harocoded collision point
bool crashShouldHappen() {
	return (cars[1]->getComponent<RigidBody>()->getPosition().z <= 10.0f);
}

CrashSimulator::CrashSimulator(Entity* parent) : Component(parent) {
	loadCubeMesh();
	loadDiffuseShader();
	loadScene();
	world = make_shared<World>();
	createCars();
	givePosition();
}

void CrashSimulator::update(double dt) {
	// Update our world. 
	// world->startFrame();
	// Here we'll run our world update method
	camera->update(dt);

	//We should also implement a method to set the y position to 0 if it's inferior
	if (!cars.empty()) {
		stayAboveGround();
		if (!collisionHappened && crashShouldHappen())
		{
			// Methods that will apply forces to cars
			crash();
			collisionHappened = true;
		}
		if (!collisionHappened && launched) {
			giveVelocity();
		}
		// Here is for the different key event

		auto it = cars.begin();
		while (it != cars.end()) {
			auto& entity = *it;
			if (entity->isForDeletion())
				it = cars.erase(it);
			else {
				if (entity->isAlive()) {
					entity->update(dt);
					//cout << entity->getComponent<RigidBody>()->getVelocity() << endl;
					//cout << entity->getComponent<RigidBody>()->getPosition() << endl;
				}
				++it;
			}
		}
		// Fire a new projectile
		if (system::isKeyPressed(SDLK_f)) {
			launched = true;
		}
	}
	world->runPhysics(dt);
}

void CrashSimulator::render() const {
	camera->render();
	auto it = cars.begin();
	while (it != cars.end()) {
		auto& entity = *it;
		if (entity->isVisible()) {
			if (it == cars.begin()) {
				shader->setUniformVector("color", glm::vec3(1.0f, 0.0f, 1.0f));
			}
			else {
				shader->setUniformVector("color", glm::vec3(0.0f, 1.0f, 0.0f));
			}
			entity->render();
		}
		++it;
	}
}


void CrashSimulator::createCars() {

	//loadCubeMesh();
	makeCars();
	auto rb1 = cars[0]->getComponent<RigidBody>();
	auto rb2 = cars[1]->getComponent<RigidBody>();

	// Don't forget to add the cars to our world too !
	//world->addRigidBody(rb1);
	//world->addRigidBody(rb2);
}


void CrashSimulator::makeCars() {

	// Dimensions are fixed
	shared_ptr<Entity> car1 = make_shared<Entity>();
	Vector3r dimensions = Vector3r(10.0f, 5.0f, 5.0f);
	car1->addComponent<Transform3D>();
	car1->getComponent<Transform3D>()->setScale({ dimensions.x, dimensions.y, dimensions.z });
	car1->addComponent<MeshRenderer>(mesh, shader, glState);
	auto rb1 = car1->addComponent<RigidBody>();

	// Setting properties of car
	rb1->setMass(30.0f);
	rb1->setInertiaTensor(getCube(rb1->getMass(), dimensions));
	car1->addComponent<BodyDimensions>(BodyDimensions::Geometry::CUBE, dimensions);


	// Dimensions are fixed
	shared_ptr<Entity> car2 = make_shared<Entity>();
	dimensions = Vector3r(5.0f, 5.0f, 5.0f);
	car2->addComponent<Transform3D>();
	car2->getComponent<Transform3D>()->setScale({ dimensions.x, dimensions.y, dimensions.z });
	car2->addComponent<MeshRenderer>(mesh, shader, glState);
	auto rb2 = car2->addComponent<RigidBody>();

	// Setting properties of car
	rb2->setMass(30.0f);
	rb2->setInertiaTensor(getCube(rb2->getMass(), dimensions));

	//Add gravity
	auto gravity = make_shared<Gravity>(Vector3r(0.0f, -9.81f, 0.0f));
	world->registry.add(rb1, gravity);
	world->registry.add(rb2, gravity);

	// Add component for render
	car2->addComponent<BodyDimensions>(BodyDimensions::Geometry::CUBE, dimensions);

	cars.push_back(car1);
	cars.push_back(car2);
}

