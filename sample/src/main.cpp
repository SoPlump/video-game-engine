// standard headers
#include <cmath>
#include <fstream>
#include <vector>

// libraries headers
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <glm/glm.hpp>

// project headers
#include "system.h"
#include "exception/ShaderCompileError.h"
#include "exception/ShaderLinkError.h"
#include "logging.h"
#include "utils/gl_utils.h"
#include "utils/Chrono.h"
#include "components/PerspectiveCamera.h"
#include "components/OrthographicCamera.h"
#include "components/FPSController.h"
#include "ecm/Entity.h"
#include "renderer/Shader.h"
#include "renderer/Mesh.h"
#include "renderer/GLState.h"
#include "components/MeshRenderer.h"
#include <BallisticGame.h>
#include <ShootDemo.h>
#include <WeaponFactory.h>
#include <main.h>
#include <BlobGame.h>
#include <CrashSimulator.h>
#include <pfgen.h>
#include "CollisionDetection.h"

#include <thread>
using namespace std;
using namespace engine;

vector<shared_ptr<Entity>> bullets;

static Chrono timer;
Entity* gamemode;
shared_ptr<ParticleForceRegistry> pfrPointer;

float framePerSec = 60.0f;

void update(double dt) {
    gamemode->update(dt);
    /*auto it = bullets.begin();
    while (it != bullets.end()) {
        auto& bullet = *it;
        if (bullet->isForDeletion())
            it = bullets.erase(it);
        else {
            if(entity->isAlive())
                entity->update(dt);
            ++it;
        }
    }*/
    //auto it = blobs.begin();
    //while (it != blobs.end()) {
    //    auto& blob = *it;
    //    if (blob->isForDeletion())
    //        it = blobs.erase(it); 
    //    else {
    //        if (blob->isAlive())
    //            blob->update(dt);
    //        it++;
    //    }
    //}

    //pfrPointer->updateForces(dt);
}

void render() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   gamemode->render();

    SDL_GL_SwapWindow(system::window());
}

void openGLReport() {
    int nbAttributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nbAttributes);
    int nbTextureUnits;
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &nbTextureUnits);
    DEBUG << EMPTY_MESSAGE;
    DEBUG << "=== OpenGL Infos ===";
    DEBUG << "GL version   : " << glGetString(GL_VERSION);
    DEBUG << "GLSL version : " << glGetString(GL_SHADING_LANGUAGE_VERSION);
    DEBUG << "GL vendor    : " << glGetString(GL_VENDOR);
    DEBUG << "GL renderer  : " << glGetString(GL_RENDERER);
    DEBUG << "Max number of vertex attributes supported : " << nbAttributes;
    DEBUG << "Max number of texture units : " << nbTextureUnits;
    DEBUG << EMPTY_MESSAGE;
}

int main() {
    if(!system::init()) {
        return EXIT_FAILURE;
    }
    openGLReport();
    
    gamemode = new Entity();
    //auto ballisticGame = gamemode->addComponent<BallisticGame>();
    //auto blobGame = gamemode->addComponent<BlobGame>();
    auto collisionDemo = gamemode->addComponent<CollisionDetection>();
//    auto crashDemo = gamemode->addComponent<CrashSimulator>();

    //pfrPointer = std::make_shared<ParticleForceRegistry>(ParticleForceRegistry());

    timer.reset();
    while (system::handleEvents()) {
        double delta = timer.delta();

        /*
        if (delta < 1.0 / framePerSec) {
            this_thread::sleep_for(1.0 / framePerSec - delta);
        }
        update(1.0 / framePerSec);
        */
        update(delta);
        render();
    }

    system::shutdown();
    
    return 0;
}
