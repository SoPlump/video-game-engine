#include "TimeoutKill.h"

TimeoutKill::TimeoutKill(Entity* parent, float timeout) : Component(parent), timeout(timeout) {}

TimeoutKill::~TimeoutKill() = default;

void TimeoutKill::update(double dt) {
    if(chrono.time() > timeout) {
        parent->setForDeletion();
    }
}

void TimeoutKill::render() const {
    // do nothing
}
