#include "BallisticGame.h"
#include <system.h>
#include <iostream>
#include "logging.h"
#include "main.h"

using namespace engine;

BallisticGame::BallisticGame(Entity* const parent) : Component(parent), weaponType(GUN) {
}

void BallisticGame::update(double dt) {

	// Choose weapon in game
	if (system::isKeyPressed(SDLK_KP_1)) {
		weaponType = GUN;
	}
	if (system::isKeyPressed(SDLK_KP_2)) {
		weaponType = CANNON;
	}
	if (system::isKeyPressed(SDLK_KP_3)) {
		weaponType = LASER;
	}
	if (system::isKeyPressed(SDLK_KP_4)) {
		weaponType = TATA;
	}

	// Shoot with weapon
	if (system::isKeyPressed(SDLK_SPACE)) {
		fire();
	}
}

void BallisticGame::fire() {
	bullets.push_back(weaponFactory.makeBullet(weaponType));
}

void BallisticGame::render() const
{
}
