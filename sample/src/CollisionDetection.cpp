#include "CollisionDetection.h"
#include "../include/components/BodyDimensions.h"
#include "../include/TimeoutKill.h"
#include "system/RigidBody.h"
#include "utils/Tensors.h"
#include "Object.h"
#include "system/Primitive.h"


using namespace engine::physics;
using namespace engine;
using namespace std;

static shared_ptr<renderer::Shader> shader = nullptr;
static shared_ptr<renderer::Shader> shader2 = nullptr;
static shared_ptr<renderer::Shader> shader3 = nullptr;
static shared_ptr<renderer::Shader> shader4 = nullptr;
static shared_ptr<renderer::Mesh> cubeMesh = nullptr;
static shared_ptr<renderer::Mesh> point = nullptr;

static Chrono shootCD;

static auto glState = make_shared<renderer::GLState>();

static shared_ptr<Entity> camera;

static vector<shared_ptr<Entity>> entities;

static constexpr float lifetime = 10.0f;

static float timeToSpawn = 2.0f;
static float lastTimeSpawned = 0.0f;
static float currentTime = 0.0f;

static void loadCubeMesh() {
	vector<unsigned> elements = {
		4, 2, 0,
		2, 7, 3,
		6, 5, 7,
		1, 7, 5,
		0, 3, 1,
		4, 1, 5,
		4, 6, 2,
		2, 6, 7,
		6, 4, 5,
		1, 3, 7,
		0, 2, 3,
		4, 0, 1
	};

	vector<float> position = {
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f,  1.0f,  -1.0f,
		-1.0f, 1.0f,  1.0f,
		1.0f,  -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f,  -1.0f,
		1.0f, 1.0f,  1.0f,
	};

	vector<float> texCoords = {
		1.0f, 1.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
	};

	cubeMesh = make_shared<renderer::Mesh>();
	cubeMesh->setPositions(position);
	cubeMesh->setIndices(elements);
	cubeMesh->finalize();
}

static void loadPointMesh() {


	vector<float> position = {
		0.0f, 0.0f, 0.0f,

	};

	point = make_shared<renderer::Mesh>(GL_POINTS);
	point->setPositions(position);
	point->finalize();
}


static void loadScene()
{
	camera = make_shared<Entity>();
	auto tr = camera->addComponent<Transform3D>();
	tr->setPosition({ 0.0f, 5.0f, 100.0f });
	camera->addComponent<PerspectiveCamera>(float(M_PI / 8.0f), 0.1f, 1000.0f);
	camera->addComponent<FPSController>();

}

static bool loadDiffuseShader() {
	try {
		shader = make_shared<renderer::Shader>("shader/diffuse.vs.glsl", "shader/diffuse.fs.glsl");
		shader->use();
		shader->setUniformVector("color", glm::vec3(0.0f, 0.0f, 1.0f));
		shader->setUniformVector("lightPosition", glm::vec3(0.0f, 0.0f, 0.0f));
		shader->setUniformVector("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

		shader2 = make_shared<renderer::Shader>("shader/diffuse.vs.glsl", "shader/diffuse.fs.glsl");
		shader2->use();
		shader2->setUniformVector("color", glm::vec3(1.0f, 0.0f, 0.0f));
		shader2->setUniformVector("lightPosition", glm::vec3(0.0f, 0.0f, 0.0f));
		shader2->setUniformVector("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

		shader3 = make_shared<renderer::Shader>("shader/diffuse.vs.glsl", "shader/diffuse.fs.glsl");
		shader3->use();
		shader3->setUniformVector("color", glm::vec3(0.0f, 1.0f, 0.0f));
		shader3->setUniformVector("lightPosition", glm::vec3(0.0f, 0.0f, 0.0f));
		shader3->setUniformVector("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

		shader4 = make_shared<renderer::Shader>("shader/diffuse.vs.glsl", "shader/diffuse.fs.glsl");
		shader4->use();
		shader4->setUniformVector("color", glm::vec3(1.0f, 0.0f, 1.0f));
		shader4->setUniformVector("lightPosition", glm::vec3(0.0f, 0.0f, 0.0f));
		shader4->setUniformVector("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

		return true;
	}
	catch (const ShaderCompileError& e) {
		FATAL << "Shader compile error : " << e.what();
		glDeleteShader(e.shader);
	}
	catch (const ShaderLinkError& e) {
		FATAL << "Shader link error : " << e.what();
		glDeleteProgram(e.program);
	}
	catch (const exception& e) {
		FATAL << e.what();
	}

	return false;
}

static bool loadDiffuseShader2() {
	try {
		shader2 = make_shared<renderer::Shader>("shader/diffuse2.vs.glsl", "shader/diffuse.fs.glsl");
		shader2->use();
		shader2->setUniformVector("color", glm::vec3(0.0f, 1.0f, 0.0f));
		shader2->setUniformVector("lightPosition", glm::vec3(0.0f, 0.0f, 0.0f));
		shader2->setUniformVector("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

		return true;
	}
	catch (const ShaderCompileError& e) {
		FATAL << "Shader compile error : " << e.what();
		glDeleteShader(e.shader);
	}
	catch (const ShaderLinkError& e) {
		FATAL << "Shader link error : " << e.what();
		glDeleteProgram(e.program);
	}
	catch (const exception& e) {
		FATAL << e.what();
	}

	return false;
}

CollisionDetection::CollisionDetection(Entity* const parent) :Component(parent) {
	loadCubeMesh();
	loadPointMesh();
	world = make_shared<World>();
	loadDiffuseShader();
	loadDiffuseShader2();
	loadScene();
	createGround();
}

void CollisionDetection::update(double dt) {
	world->startFrame();
	// Here we'll run our world update method
	camera->update(dt);
	world->runPhysics(dt);
	// Here is for the different key event

	auto it = entities.begin();
	while (it != entities.end()) {
		auto& entity = *it;
		if (entity->isForDeletion())
			it = entities.erase(it);
		else {
			if (entity->isAlive()) {
				entity->update(dt);
			}
			++it;
		}
	}
	// Fire a new projectile
	if (system::isKeyPressed(SDLK_f)) {
		if (shootCD.time() >= 1.0f) {
			createProjectile();
			shootCD.reset();
		}
	}
	currentTime += dt;
}


// Note : we have to also render the centre of our cube
void CollisionDetection::render() const {
	camera->render();

	//loadCubeMesh();
	auto it = entities.begin();
	while (it != entities.end()) {
		auto& entity = *it;
		if (entity->isVisible()) {
			//shader->setUniformVector("color", glm::vec3(1.0f, 0.0f, 1.0f));

			entity->render();
		}
		++it;
	}
}

// This is what we called fire in first demo
void CollisionDetection::createProjectile() {

	shared_ptr<Entity> projectile;

	// For now, dimensions are fixed
	Vector3r dimensions = Vector3r(1.0f, 1.0f, 1.0f);

	projectile = make_shared<Entity>();
	projectile->addComponent<Transform3D>()->setScale({ dimensions.x, dimensions.y, dimensions.z });
	projectile->addComponent<MeshRenderer>(cubeMesh, shader4, glState);
	auto rb = projectile->addComponent<RigidBody>();

	// Setting properties of projectile
	rb->setMass(1.0f);
	rb->setInertiaTensor(getCube(rb->getMass(), dimensions));


	// Initial physics of the body
	rb->setVelocity(Vector3f(0.0f, 20.0f, 15.0f));
	rb->setPosition(Vector3f(0.0f, 50.0f, 0.0f));
	rb->setAngularVelocity(Vector3f(M_PI, 2.0f * M_PI, 0.5f * M_PI));

	// Add component for render
	projectile->addComponent<BodyDimensions>(BodyDimensions::Geometry::CUBE, dimensions);

	// Adding component for Timeout
	projectile->addComponent<TimeoutKill>(lifetime);

	// Applying forces on the body

	auto gravity = make_shared<Gravity>(Vector3r(0.0f, -9.81f, 0.0f));
	world->registry.add(rb, gravity);

	auto box = projectile->addComponent<Box>(Matrix4r(), dimensions / 2.0f);
	auto object = projectile->addComponent<Object>(rb->getPosition());
	object->addPrimitive(box);

	// Adding projectile to vector for render
	entities.push_back(projectile);

	// Don't forget to add the projectile to our world too !
	world->addObject(object);
}

void CollisionDetection::createGround() {

	shared_ptr<Entity> ground;
	shared_ptr<Entity> roof;

	Vector3r dimensions = Vector3r(100.0f, 0.1f, 100.0f);

	ground = make_shared<Entity>();
	ground->addComponent<Transform3D>()->setScale({ dimensions.x, dimensions.y, dimensions.z });
	ground->addComponent<MeshRenderer>(cubeMesh, shader, glState);

	auto rb = ground->addComponent<RigidBody>();
	auto plane = ground->addComponent<Plane>(Matrix4r(), Vector3r(0.0f, 1.0f, 0.0f), 0.0f);

	rb->setInverseMass(0.0f);
	rb->setPosition(Vector3f(0.0f, 0.0f, 0.0f));

	auto object = ground->addComponent<Object>(rb->getPosition());
	entities.push_back(ground);
	object->addPrimitive(plane);


	world->addObject(object);

	roof = make_shared<Entity>();
	roof->addComponent<Transform3D>()->setScale({ dimensions.x, dimensions.y, dimensions.z });
	roof->addComponent<MeshRenderer>(cubeMesh, shader, glState);

	auto rb2 = roof->addComponent<RigidBody>();
	auto plane2 = roof->addComponent<Plane>(Matrix4r(), Vector3r(0.0f, -1.0f, 0.0f), 100.0f);

	rb2->setInverseMass(0.0f);
	rb2->setPosition(Vector3f(0.0f, 100.0f, 0.0f));

	auto object2 = roof->addComponent<Object>(rb2->getPosition());
	object2->addPrimitive(plane2);
	entities.push_back(roof);

	world->addObject(object2);

	// Walls 1 & 3
	/*
	shared_ptr<Entity> wall1;
	shared_ptr<Entity> wall3;

	dimensions = Vector3r(0.1f, 100.0f, 100.0f);

	wall1 = make_shared<Entity>();
	wall1->addComponent<Transform3D>()->setScale({ dimensions.x, dimensions.y, dimensions.z });
	wall1->addComponent<MeshRenderer>(cubeMesh, shader2, glState);

	auto rb3 = wall1->addComponent<RigidBody>();
	auto plane3 = wall1->addComponent<Plane>(Matrix4r(), Vector3r(1.0f, 0.0f, 0.0f), 100.0f);

	rb3->setInverseMass(0.0f);
	rb3->setPosition(Vector3f(-100.0f, 50.0f, 0.0f));

	auto object3 = wall1->addComponent<Object>(rb3->getPosition());
	entities.push_back(wall1);

	world->addObject(object3);

	wall3 = make_shared<Entity>();
	wall3->addComponent<Transform3D>()->setScale({ dimensions.x, dimensions.y, dimensions.z });
	wall3->addComponent<MeshRenderer>(cubeMesh, shader2, glState);

	auto rb4 = wall3->addComponent<RigidBody>();
	auto plane4 = wall3->addComponent<Plane>(Matrix4r(), Vector3r(-1.0f, 0.0f, 0.0f), 100.0f);

	rb4->setInverseMass(0.0f);
	rb4->setPosition(Vector3f(100.0f, 50.0f, 0.0f));

	auto object4 = wall3->addComponent<Object>(rb4->getPosition());
	entities.push_back(wall3);

	world->addObject(object4);
	*/

}
