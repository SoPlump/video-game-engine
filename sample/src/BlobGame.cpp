#include <BlobGame.h>
#include <system/Particle.h>
#include <system/Cube.h>
#include <system/physics.h>
#include <Vector.h>
#include <system.h>
#include <main.h>
#include <pfgen.h>
#include <renderer/Shader.h>
#include <renderer/Mesh.h>
#include <renderer/GLState.h>
#include <exception/ShaderCompileError.h>
#include <logging.h>
#include <exception/ShaderLinkError.h>
#include <components/Transform3D.h>
#include <components/FPSController.h>
#include <components/PerspectiveCamera.h>
#include <components/MeshRenderer.h>
#include <stdlib.h>
#include <math.h>


using namespace engine::physics;
using namespace engine;
using namespace std;


static shared_ptr<renderer::Shader> shader = nullptr;
static shared_ptr<renderer::Mesh> mesh = nullptr;
static auto glState = make_shared<renderer::GLState>();
static shared_ptr<Entity> camera;
static shared_ptr<Entity> bottomWall;
static shared_ptr<Entity> leftWall;
static shared_ptr<Entity> rightWall;
static shared_ptr<Entity> roof;
static vector<shared_ptr<Entity>> blobs;
static const double timeToSpawn = 2; //in seconds
static double lastTimeSpawned = 0;
static double currentTime = 0;

static void loadFloorMesh() {
    vector<unsigned> elements = {
        4, 2, 0,
        2, 7, 3,
        6, 5, 7,
        1, 7, 5,
        0, 3, 1,
        4, 1, 5,
        4, 6, 2,
        2, 6, 7,
        6, 4, 5,
        1, 3, 7,
        0, 2, 3,
        4, 0, 1
    };

    vector<float> position = {
        0.0f,  0.0f, -3.0f,
        0.0f, 0.0f, -3.0f,
        0.0f,  0.0f,  3.0f,
        0.0f, 0.0f,  3.0f,
        -40.0f,  0.0f, -3.0f,
        -40.0f, 0.0f, -3.0f,
        -40.0f,  0.0f,  3.0f,
        -40.0f, 0.0f,  3.0f,
    };

    vector<float> texCoords = {
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
    };

    mesh = make_shared<renderer::Mesh>();
    mesh->setPositions(position);
    mesh->setIndices(elements);
    mesh->finalize();
}

static void loadWallMesh() {
    vector<unsigned> elements = {
        4, 2, 0,
        2, 7, 3,
        6, 5, 7,
        1, 7, 5,
        0, 3, 1,
        4, 1, 5,
        4, 6, 2,
        2, 6, 7,
        6, 4, 5,
        1, 3, 7,
        0, 2, 3,
        4, 0, 1
    };

    vector<float> position = {
        0.0f, 20.0f, -3.0f,
        0.0f, -20.0f, -3.0f,
        0.0f,  20.0f,  3.0f,
        0.0f, -20.0f,  3.0f,
        0.0f,  20.0f, -3.0f,
        0.0f, -20.0f, -3.0f,
        0.0f, 20.0f,  3.0f,
        0.0f, -20.0f,  3.0f,
    };

    vector<float> texCoords = {
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
    };

    mesh = make_shared<renderer::Mesh>();
    mesh->setPositions(position);
    mesh->setIndices(elements);
    mesh->finalize();
}

static void loadCubeMesh() {
    vector<unsigned> elements = {
        4, 2, 0,
        2, 7, 3,
        6, 5, 7,
        1, 7, 5,
        0, 3, 1,
        4, 1, 5,
        4, 6, 2,
        2, 6, 7,
        6, 4, 5,
        1, 3, 7,
        0, 2, 3,
        4, 0, 1
    };

    vector<float> position = {
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f,  1.0f,  -1.0f,
        -1.0f, 1.0f,  1.0f,
        1.0f,  -1.0f, -1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, 1.0f,  -1.0f,
        1.0f, 1.0f,  1.0f,
    };

    vector<float> texCoords = {
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
    };

    mesh = make_shared<renderer::Mesh>();
    mesh->setPositions(position);
    mesh->setIndices(elements);
    mesh->finalize();

}

static void loadSphereMesh() {
    vector<unsigned> elements = {
        0, 11, 5,
        0, 5, 1,
        0, 1, 7,
        0, 7, 10,
        0, 10, 11,
        1, 5, 9,
        5, 11, 4,
        11, 10, 2,
        10, 7, 6,
        7, 1, 8,
        3, 9, 4,
        3, 4, 2,
        3, 2, 6,
        3, 6, 8,
        3, 8, 9,
        4, 9, 5,
        2, 4, 11,
        6, 2, 10,
        8, 6, 7,
        9, 8, 1,
    };

    const double t = (1.0 + std::sqrt(5.0)) / 2.0;

    auto v1 = glm::normalize(glm::vec3(-1.0, t, 0.0));
    auto v2 = glm::normalize(glm::vec3(1.0, t, 0.0));
    auto v3 = glm::normalize(glm::vec3(-1.0, -t, 0.0));
    auto v4 = glm::normalize(glm::vec3(1.0, -t, 0.0));

    auto v5 = glm::normalize(glm::vec3(0.0, -1.0, t));
    auto v6 = glm::normalize(glm::vec3(0.0, 1.0, t));
    auto v7 = glm::normalize(glm::vec3(0.0, -1.0, -t));
    auto v8 = glm::normalize(glm::vec3(0.0, 1.0, -t));

    auto v9 = glm::normalize(glm::vec3(t, 0.0, -1.0));
    auto v10 = glm::normalize(glm::vec3(t, 0.0, 1.0));
    auto v11 = glm::normalize(glm::vec3(-t, 0.0, -1.0));
    auto v12 = glm::normalize(glm::vec3(-t, 0.0, 1.0));

    vector<float> position = {
        v1.x, v1.y, v1.z,
        v2.x, v2.y, v2.z,
        v3.x, v3.y, v3.z,
        v4.x, v4.y, v4.z,
        v5.x, v5.y, v5.z,
        v6.x, v6.y, v6.z,
        v7.x, v7.y, v7.z,
        v8.x, v8.y, v8.z,
        v9.x, v9.y, v9.z,
        v10.x, v10.y, v10.z,
        v11.x, v11.y, v11.z,
        v12.x, v12.y, v12.z,
    };

    vector<float> normals = {
        v1.x, v1.y, v1.z,
        v2.x, v2.y, v2.z,
        v3.x, v3.y, v3.z,
        v4.x, v4.y, v4.z,
        v5.x, v5.y, v5.z,
        v6.x, v6.y, v6.z,
        v7.x, v7.y, v7.z,
        v8.x, v8.y, v8.z,
        v9.x, v9.y, v9.z,
        v10.x, v10.y, v10.z,
        v11.x, v11.y, v11.z,
        v12.x, v12.y, v12.z,
    };

    mesh = make_shared<renderer::Mesh>();
    mesh->setPositions(position);
    mesh->setIndices(elements);
    mesh->finalize();
}

static bool loadDiffuseShader() {
    try {
        shader = make_shared<renderer::Shader>("shader/diffuse.vs.glsl", "shader/diffuse.fs.glsl");
        shader->use();
        shader->setUniformVector("color", glm::vec3(0.0f, 0.0f, 0.0f));
        shader->setUniformVector("lightPosition", glm::vec3(0.0f, 0.0f, 0.0f));
        shader->setUniformVector("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

        return true;
    }
    catch (const ShaderCompileError& e) {
        FATAL << "Shader compile error : " << e.what();
        glDeleteShader(e.shader);
    }
    catch (const ShaderLinkError& e) {
        FATAL << "Shader link error : " << e.what();
        glDeleteProgram(e.program);
    }
    catch (const exception& e) {
        FATAL << e.what();
    }

    return false;
}

static void loadScene() {
    camera = make_shared<Entity>();
    camera->addComponent<Transform3D>();
    camera->addComponent<PerspectiveCamera>(float(M_PI / 8.0f), 0.1f, 1000.0f);

    loadCubeMesh();

    // Floor definition

    bottomWall = make_shared<Entity>();
    bottomWall->addComponent<Transform3D>();
    bottomWall->addComponent<MeshRenderer>(mesh, shader, glState);
    

    auto floorCube = bottomWall->addComponent<Cube>();
    floorCube->setInverseMass(0.0f);
    floorCube->setCentre(Vector3r(0.0f, -0.05f, 0.0f));
    floorCube->setX(40.0f);
    floorCube->setY(0.1f);
    floorCube->setZ(6.0f);

    bottomWall->getComponent<Transform3D>()->setScale({ floorCube->getX()/2.0f, floorCube->getY()/2.0f, floorCube->getZ()/2.0f });
    bottomWall->getComponent<Transform3D>()->setPosition({ floorCube->getCentre().x, floorCube->getCentre().y, floorCube->getCentre().z});
    
    // Left wall definition

    leftWall = make_shared<Entity>();
    leftWall->addComponent<Transform3D>();
    leftWall->addComponent<MeshRenderer>(mesh, shader, glState);

    auto lWallCube = leftWall->addComponent<Cube>();
    lWallCube->setInverseMass(0.0f);
    lWallCube->setCentre(Vector3r(-19.95f, 10.0f, 0.0f));
    lWallCube->setX(0.1f);
    lWallCube->setY(20.0f);
    lWallCube->setZ(6.0f);

    leftWall->getComponent<Transform3D>()->setScale({ lWallCube->getX() / 2.0f, lWallCube->getY() / 2.0f, lWallCube->getZ() / 2.0f });
    leftWall->getComponent<Transform3D>()->setPosition({ lWallCube->getCentre().x, lWallCube->getCentre().y, lWallCube->getCentre().z });

    // Right wall definition

    rightWall = make_shared<Entity>();
    rightWall->addComponent<Transform3D>();
    rightWall->addComponent<MeshRenderer>(mesh, shader, glState);

    auto rWallCube = rightWall->addComponent<Cube>();
    rWallCube->setInverseMass(0.0f);
    rWallCube->setCentre(Vector3r(20.05f, 10.0f, 0.0f));
    rWallCube->setX(0.1f);
    rWallCube->setY(20.0f);
    rWallCube->setZ(6.0f);

    rightWall->getComponent<Transform3D>()->setScale({ rWallCube->getX() / 2.0f, rWallCube->getY() / 2.0f, rWallCube->getZ() / 2.0f });
    rightWall->getComponent<Transform3D>()->setPosition({ rWallCube->getCentre().x, rWallCube->getCentre().y, rWallCube->getCentre().z });

    // Roof definition

    roof = make_shared<Entity>();
    roof->addComponent<Transform3D>();
    roof->addComponent<MeshRenderer>(mesh, shader, glState);

    auto roofCube = roof->addComponent<Cube>();
    roofCube->setInverseMass(0.0f);
    roofCube->setCentre(Vector3r(0.0f, 20.05f, 0.0f));
    roofCube->setX(40.0f);
    roofCube->setY(0.1f);
    roofCube->setZ(6.0f);

    roof->getComponent<Transform3D>()->setScale({ roofCube->getX() / 2.0f, roofCube->getY() / 2.0f, roofCube->getZ() / 2.0f });
    roof->getComponent<Transform3D>()->setPosition({ roofCube->getCentre().x, roofCube->getCentre().y, roofCube->getCentre().z });
    
}

BlobGame::BlobGame(Entity* const parent) : Component(parent) {
    particleWorld = make_shared<ParticleWorld>(maxContacts, 3);
    loadFloorMesh();
    loadDiffuseShader();
    loadScene();
    loadSphereMesh();
    makeBlob();
}

void BlobGame::update(double dt) {
    particleWorld->runPhysics(dt);
    camera->update(dt);

	if (!particleWorld->getParticles().empty()) {
		if (system::isKeyPressed(SDLK_LEFT) || system::isKeyPressed(SDLK_q)) {
			(*(particleWorld->getParticles()).begin())->addForce(Vector3r(-5, 0, 0));
		}
		if (system::isKeyPressed(SDLK_RIGHT) || system::isKeyPressed(SDLK_d)) {
			(*(particleWorld->getParticles()).begin())->addForce(Vector3r(5, 0, 0));
		}
        if (system::isKeyPressed(SDLK_UP) || system::isKeyPressed(SDLK_w)) {
            (*(particleWorld->getParticles()).begin())->addForce(Vector3r(0, 0, -100));
        }
        if (system::isKeyPressed(SDLK_DOWN) || system::isKeyPressed(SDLK_s)) {
            (*(particleWorld->getParticles()).begin())->addForce(Vector3r(0, 0, 100));
        }
        if (system::isKeyPressed(SDLK_SPACE)) {
            (*(particleWorld->getParticles()).begin())->addForce(Vector3r(0, 1000, 0));
        }
	}

    // Create new blobs if under estimation of maxContacts
    if (system::isKeyPressed(SDLK_p)) {
        if (currentTime - lastTimeSpawned > timeToSpawn) {
            int newSize = particleWorld->getParticles().size() + 1;
            if ((2*newSize + (newSize - 1) * (newSize - 1)) < maxContacts) {
                makeBlob();
                lastTimeSpawned = currentTime;
            }
        }
    }
    currentTime += dt;
}

void BlobGame::render() const
{
    camera->render();
    shader->setUniformVector("color", glm::vec3(1.0f, 0.0f, 0.0f));
    bottomWall->render();
    roof->render();
    shader->setUniformVector("color", glm::vec3(1.0f, 1.0f, 0.0f));
    rightWall->render();
    shader->setUniformVector("color", glm::vec3(0.0f, 0.0f, 1.0f));
    leftWall->render();
    auto it = blobs.begin();
    while (it != blobs.end()) {
        auto& entity = *it;
        if (entity->isVisible())
            if (it == blobs.begin()) {
                shader->setUniformVector("color", glm::vec3(1.0f, 0.0f, 1.0f));
            }
            else {
                shader->setUniformVector("color", glm::vec3(0.0f, 1.0f, 0.0f));
            }
            entity->render();
        ++it;
    }
}

std::shared_ptr<Entity> BlobGame::makeBlob() {
    //TODO: vérifier si on ne dépasse pas le nombre de contact
    float radius;
    auto entity = std::make_shared<Entity>();
    if (particleWorld->getParticles().empty()) {
        radius = 1.0f;
    }
    else {
        float randMin = 0.1f;
        float randMax = 2.0f;
        radius = randMin + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (randMax - randMin)));
    }

    // create the rendering of blob
    entity->addComponent<Transform3D>()->setScale({radius, radius, radius});
    entity->addComponent<MeshRenderer>(mesh, shader, glState);

    // Create a particle
    auto particle = entity->addComponent <Particle>();
    particle->setMass(5.0f*((4.0f/3.0f)*M_PI*radius*radius*radius));
    particle->setVelocity(Vector3f(0.0f, 0.0f, 0.0f));
    particle->setAcceleration(Vector3f(0.0f, 0.0f, 0.0f));
    particle->setPosition(Vector3f(0.0f, 5.0f, 0.0f));
    particle->setRadius(radius);

    // Add particle to particle "world"
    particleWorld->addParticle(particle);

    // Handle links between blobs
	linkBlob(particle);

    // Forces
    auto gravity = make_shared<ParticleGravity>(Vector3r(0.0f, -9.81f, 0.0f));
    particleWorld->registry.add(particle, gravity);

    auto drag = make_shared<ParticleDrag>(0.3f, 0.9f);
    particleWorld->registry.add(particle, drag);

    // Add entity to scene blobs
    blobs.push_back(entity);

    // Add floor to particle
    auto particleFloor = make_shared<ParticleFloor>(particle, nullptr, 0.7f);
    particleWorld->contactGenerators.push_back(particleFloor);

    //Add walls to particle
    auto particleLWall = make_shared<ParticleLeftWall>(particle, nullptr, 0.7f);
    particleWorld->contactGenerators.push_back(particleLWall);
    auto particleRWall = make_shared<ParticleRightWall>(particle, nullptr, 0.7f);
    particleWorld->contactGenerators.push_back(particleRWall);

    return entity;
}

void BlobGame::linkBlob(std::shared_ptr<Particle> particle) {
    
    // Collisions
    ParticleWorld::Particles particles = particleWorld->getParticles();
    auto it = particles.begin();
    while (it != particles.end()) {
    	auto& blobParticle = *it;
    	if (!blobParticle->isForDeletion()) {
    		if (blobParticle->isAlive()) {
                // Create soft spring forces between particles
    			auto spring = std::make_shared<ParticleSoftSpring>(blobParticle, springConst, restLength, maxLength);
    			auto spring2 = std::make_shared<ParticleSoftSpring>(particle, springConst, restLength, maxLength);
    			
                if (it != particles.begin()) {
                    particleWorld->registry.add(blobParticle, spring2);
                }
                particleWorld->registry.add(particle, spring);

                // Create cable contact between particles
                auto cable = make_shared<ParticleCable>(particle, blobParticle, maxLength, restitution);
                
                particleWorld->contactGenerators.push_back(cable);

                auto contact = make_shared<ParticleSphere>(particle, blobParticle, restitution);
                particleWorld->contactGenerators.push_back(contact);

    			it++;
    		}
    	}
    }
}
