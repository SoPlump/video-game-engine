#pragma once

#include <ecm/Component.h>
#include <WeaponFactory.h>

class BallisticGame : public Component{
public:
	BallisticGame(Entity* const parent);

	void update(double dt) override;
	void render() const override;

	void fire();

private:
	WeaponType weaponType;

	WeaponFactory weaponFactory;
};