#pragma once

#include "../../engine/include/ecm/Component.h"

#include "World.h"

class CrashSimulator : public Component {
public:
	CrashSimulator(Entity* const parent);

	void update(double dt) override;
	void render() const override;

	void createCars();
	void makeCars();

private:
	std::shared_ptr<World> world;

};