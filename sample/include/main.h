#pragma once
#include <memory>
#include <vector>
#include <pfgen.h>

extern std::vector<std::shared_ptr<Entity>> bullets;
//extern std::vector<std::shared_ptr<Entity>> blobs;
extern std::shared_ptr<ParticleForceRegistry> pfrPointer;