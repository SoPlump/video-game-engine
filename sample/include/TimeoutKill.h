#pragma once

#include "ecm/Component.h"
#include "utils/Chrono.h"

class TimeoutKill : public Component {
public:
    TimeoutKill(Entity* parent, float timeout);
    ~TimeoutKill() override;

    void update(double dt) override;
    void render() const override;

private:
    Chrono chrono;
    float timeout;
};
