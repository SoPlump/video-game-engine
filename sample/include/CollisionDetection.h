#pragma once 

#include <system/physics.h>
#include <system.h>
#include <renderer/Shader.h>
#include <renderer/Mesh.h>
#include <renderer/GLState.h>
#include <exception/ShaderCompileError.h>
#include <logging.h>
#include <exception/ShaderLinkError.h>
#include <components/Transform3D.h>
#include <components/FPSController.h>
#include <components/PerspectiveCamera.h>
#include <components/MeshRenderer.h>
#include <stdlib.h>

#include "World.h"

class CollisionDetection : public Component {
public:
	CollisionDetection(Entity* const parent);

	void update(double dt) override;
	void render() const override;

	void createProjectile();

	void createGround();

private:
	std::shared_ptr<World> world;

};