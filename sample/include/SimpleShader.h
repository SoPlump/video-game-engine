#pragma once

const char* SIMPLE_VERTEX_SHADER =
    "#version 330 core\n"
    "\n"
    "layout (location = 0) in vec3 aPosition;\n"
    "\n"
    "out vec3 position;\n"
    "\n"
    "void main() {\n"
    "    gl_Position = vec4(aPosition, 1.0f);\n"
    "    position = aPosition;"
    "}";

const char* SIMPLE_FRAGMENT_SHADER =
    "#version 330 core\n"
    "\n"
    "in vec3 position;\n"
    "void main() {\n"
    "    gl_FragColor = vec4(position, 1.0);\n"
    "}";
