#pragma once

#include <system/Particle.h>
#include <ecm/Entity.h>
#include <ecm/Component.h>
#include <vector>
#include "TimeoutKill.h"

enum WeaponType {
	GUN,
	CANNON,
	LASER,
	TATA
};

class WeaponFactory {
	public:
		WeaponFactory();

        std::shared_ptr<Entity> makeBullet(WeaponType type);
};
