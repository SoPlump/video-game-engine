#pragma once

#include <ecm/Entity.h>
#include <ecm/Component.h>
#include <system/Particle.h>
#include <system/physics.h>

//Collisions
#include "pworld.h"
#include "plink.h"

class BlobGame : public Component {
public:
	BlobGame(Entity* const parent);

	void update(double dt) override;
	void render() const override;

	std::shared_ptr<Entity> makeBlob();
	void linkBlob(std::shared_ptr<engine::physics::Particle> particle);
	//void AddFloor(std::shared_ptr<engine::physics::Particle> particle);

private:
	std::shared_ptr<ParticleWorld> particleWorld;


	unsigned maxContacts = 200;
	engine::physics::real springConst = 4.0f;
	engine::physics::real restitution = 0.7f;
	engine::physics::real restLength = 5.0f;
	engine::physics::real maxLength = 7.0f;
};