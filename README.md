# Video Game Engine

## Contrôles

### Blob game (Loco Roco game like)

Pour déplacer le blob principal (mauve), utiliser les touches ZQSD.
- La touche Z (ou flèche du haut) sert à s'éloigner de la caméra.
- La touche S (ou flèche du bas) sert à se rapprocher de la caméra.
- La touche Q (ou flèche de gauche) sert à se déplacer vers la gauche.
- La touche D (ou flèche de droite) sert à se déplacer sur la droite.

Grâce à la touche espace, il est possible de faire sauter le blob. Rester appuyé sur la touche espace permet de faire voler le blob.

Pour créer de nouveaux blobs, utilisez la touche P (une limite de création des blobs est définie). On ne peut créer un blob qu'une fois toute les 2 secondes.

### Démo 1 : lancer de cubes

Commandes de tir : 
- F pour lancer le projectile

### Démo 2 : crash

Commandes de tir :
- F pour lancer la simulation

### Contrôles de la caméra

FPS Controller (actif sur les démos lancer de cube et crash) :
On utilise ce composant pour déplacer la caméra.
- Z, Q, S et D : déplacer
- souris : orienter la caméra
- espace : monter
- ctrl gauche : descendre
- shift gauche : accélérer
- L : activer/désactiver le composant pour pouvoir utiliser sa souris

## Installation du projet

### Dépendances

- [Boost](https://www.boost.org/) 1.72 : log et tests unitaires
- [SDL2](https://www.libsdl.org/) 2.0.12 : création d'une fenêtre et d'un contexte OpenGL et gestion des évènements
- [GLEW](http://glew.sourceforge.net/) 2.1.0 : accès simplifié à l'API d'OpenGL
- [GLM](https://glm.g-truc.net/0.9.9/index.html) 0.9.9 : matrices et quaternions pour le rendu 3D

### Linux

Les librairies nécessaires pour compiler le projet sont disponibles dans le gestionnaire de paquet de la plupart des distributions.

#### ArchLinux
    - `boost`
    - `boost-libs`
    - `sdl2`
    - `glew`
    - `glm`

#### Debian
    - `libboost-all-dev`
    - `libsdl2-dev`
    - `libglew-dev`
    - `libglm-dev`

### Windows

#### Boost

- Télécharger les sources
- Extraire l'archive dans le sous-dossier `lib` à la racine du projet.
La structure de dossier doit être `<project root>/lib/Boost/` (et pas `<project root>/lib/Boost/Boost`).
- Ouvrir un terminal dans le dossier `Boost` et exécuter le script `bootstrap.bat`
- Exécuter la commande `b2 link=shared threading=multi address-model=x64 toolset=msvc runtime-link=shared stage`

#### SDL2

- Télécharger les librairies de développement
- Extraire l'archive dans le sous-dossier `lib` à la racine du projet.
La structure de dossier doit être `<project root>/lib/SDL2/` (et pas `<project root>/lib/SDL2/SDL2`).
- Aller dans le dossier `SDL2/include` et déplacer son contenu dans un sous dossier `SDL2` (de sorte à obtenir le chemin `SDL2/include/SDL2`).
- Aller dans le dossier `SDL2/lib` et déplacer le contenu du sous-dossier `x64` dans le sous-dossier `lib`.

#### GLEW

- Télécharger la librairie précompilée
- Extraire l'archive dans le sous-dossier `lib` à la racine du projet.
La structure de dossier doit être `<project root>/lib/GLEW/` (et pas `<project root>/lib/GLEW/GLEW`).
- Aller dans le sous-dossier `lib/Release/x64` et déplacer son contenu dans le sous-dossier `lib`

#### GLM

- Télécharger la librairie
- Extraire l'archive dans le sous-dossier `lib` à la racine du project.
La structure de dossier doit être `<project root>/lib/glm/` (et pas `<project root>/lib/glm/glm`).

## Compilation

Le projet utilise CMake pour gérer le build.

### Visual Studio 2019

- Vérifier que le composant `Outils C++ CMake pour Windows` est bien installé.
- Lancer Visual Studio, aller dans `Fichier/Ouvrir/CMake` et sélectionner le fichier `CMakeLists.txt` à la racine du projet.

### CLion

- Ouvrir le fichier `CMakeLists.txt` à la racine du projet

### Ligne de commande

- `cd <project root>`
- `mkdir build && cd build`
- `cmake -G "Unix Makefiles" ..`
- `make all`

## Exécution

Les exécutables sont placés dans le sous-dossier `bin` du dossier de build de CMake (le chemin de ce dossier s'affiche dans la console à l'exécution de CMake).

Il y a 2 exécutables :
- `sample` : l'exécutable de démo
- `engine_tests` : l'exécutable des tests unitaires

## Eventuels problèmes avec CMake

Il est possible que l'une des commandes de CMake ne s'execute pas correctement causant un problème avec les shaders. Une fois le code compilé, vérifier que les dossiers "img", "shader" et le fichier "cubes" présents dans le dossier "engine/res" soient également présents dans le dossier "out\build\x64-Debug (par défaut)\bin". Si ce n'est pas le cas, il faut simplement les copier et les coller dans ce dossier.

## Démo

Pour lancer notre démo, éxecuter l'éxecutable `sample`. Une fois executé, une fenêtre s'ouvrira portant le nom "sample". C'est dans cette fenêtre que la démo aura lieu. Pour comprendre comment fonctionnent les commandes, voir la section commande en haut de ce fichier. Les coordonnées des blobs sont également lisibles dans un terminal mais cette solution est difficlement compréhensible.
