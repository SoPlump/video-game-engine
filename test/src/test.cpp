#define BOOST_TEST_MODULE engine-tests
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>

#include "VectorTest.h"
#include "ForceTest.h"
#include "MatrixTest.h"
#include "QuaternionTest.h"
