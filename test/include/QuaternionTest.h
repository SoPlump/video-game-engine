#pragma once

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <Quaternion.h>
#include <Vector.h>
#include <Matrix.h>

using namespace engine::physics;

BOOST_AUTO_TEST_SUITE(QuaternionTest)

BOOST_AUTO_TEST_CASE(testQuaternionEqual) {
	Quaternion q1;
	Quaternion q2;

	BOOST_TEST((q1 == q2));
}

BOOST_AUTO_TEST_CASE(testQuaternionNormalize) {
	Quaternion q1;

	q1.axis.i = 4.0f;
	q1.axis.j = 2.0f;
	q1.axis.k = -2.0f;

	q1.normalize();

	Quaternion q2;

	q2.scalar = 0.2f;
	q2.axis.i = 0.8f;
	q2.axis.j = 0.4f;
	q2.axis.k = -0.4f;

	BOOST_TEST((q1 == q2));
}

BOOST_AUTO_TEST_CASE(testQuaternionMultiplyByQuaternion) {
	Quaternion q1;
	Quaternion q2;
	Quaternion q3;

	q1.axis.i = 4.0f;
	q1.axis.j = 2.0f;
	q1.axis.k = -2.0f;

	q2.axis.i = -2.0f;
	q2.axis.j = 2.0f;
	q2.axis.k = -2.0f;

	q1 *= q2;

	q3.axis.i = 2.0f;
	q3.axis.j = 16.0f;
	q3.axis.k = 8.0f;

	BOOST_TEST((q1 == q3));
}

BOOST_AUTO_TEST_CASE(testQuaternionRotateByVector) {
	Quaternion q1;
	Quaternion q2;
	Vector3r v1;


	q1.axis.i = 4.0f;
	q1.axis.j = 2.0f;
	q1.axis.k = -2.0f;

	v1.x = -2.0f;
	v1.y = 2.0f;
	v1.z = -2.0f;

	q1.rotateByVector(v1);

	q2.scalar = 0.0f;
	q2.axis.i = -2.0f;
	q2.axis.j = 14.0f;
	q2.axis.k = 10.0f;

	BOOST_TEST((q1 == q2));
}

BOOST_AUTO_TEST_CASE(testQuaternionConvertToMatrix3) {
	Quaternion q1;
	Matrix3r m1;
	Matrix3r m2;

	q1.axis.i = 4.0f;
	q1.axis.j = 2.0f;
	q1.axis.k = -2.0f;

	m1.setOrientation(q1);

	m2[0][0] = -15;
	m2[0][1] = 12;
	m2[0][2] = -20;

	m2[1][0] = 20;
	m2[1][1] = -39;
	m2[1][2] = 0;

	m2[2][0] = -12;
	m2[2][1] = -16;
	m2[2][2] = -39;


	BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_CASE(testQuaternionConvertToMatrix4) {
	Quaternion q1;
	Vector3r v1;
	Matrix4r m1;
	Matrix4r m2;

	q1.axis.i = 4.0f;
	q1.axis.j = 2.0f;
	q1.axis.k = -2.0f;

	v1.x = -2.0f;
	v1.y = 2.0f;
	v1.z = -2.0f;

	m1.setOrientationAndPos(q1, v1);

	m2[0][0] = -15;
	m2[0][1] = 12;
	m2[0][2] = -20;
	m2[0][3] = -2.0f;

	m2[1][0] = 20;
	m2[1][1] = -39;
	m2[1][2] = 0;
	m2[1][3] = 2.0f;

	m2[2][0] = -12;
	m2[2][1] = -16;
	m2[2][2] = -39;
	m2[2][3] = -2.0f;


	BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_SUITE_END()