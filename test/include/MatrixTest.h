#pragma once
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <Matrix.h>
#include <Vector.h>


using namespace engine::physics;

BOOST_AUTO_TEST_SUITE(MatrixTest)

// Matrix4

BOOST_AUTO_TEST_CASE(testMatrix4rEqual) {
    Matrix4r m1;
    Matrix4r m2;

    BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_CASE(testMatrix4rCopy) {
    Matrix4r m1;
    Matrix4r m2(m1);

    BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_CASE(testMatrix4rNotEqual) {
    Matrix4r m1;
    Matrix4r m2;

    m1[0][0] = 42.0f;

    bool isEqual = m1 == m2;

    BOOST_TEST((isEqual == false));
}


BOOST_AUTO_TEST_CASE(testMatrix4rDeterminant) {
    Matrix4r m1;
    m1[0][0] = 2.0f;

    float det = m1.getDeterminant();

    BOOST_TEST((det == 2.0f));
}

BOOST_AUTO_TEST_CASE(testMatrix4rInvert) {
    Matrix4r m1;
    m1[0][0] = 2.0f;
    m1[0][1] = 3.0f;
    m1[1][0] = 4.0f;

    m1.invert();

    Matrix4r m2;
    m2[0][0] = -0.1f;
    m2[0][1] = 0.3f;
    m2[1][0] = 0.4f;
    m2[1][1] = -0.2f;

    BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_CASE(testMatrix4rInverse) {
    Matrix4r m1;
    m1[0][0] = 2.0f;
    m1[0][1] = 3.0f;
    m1[1][0] = 4.0f;

    Matrix4r m2 = m1.inverse();

    Matrix4r m3;
    m3[0][0] = -0.1f;
    m3[0][1] = 0.3f;
    m3[1][0] = 0.4f;
    m3[1][1] = -0.2f;

    BOOST_TEST((m2 == m3));
}

BOOST_AUTO_TEST_CASE(testMatrix4rInverse2) {
    Matrix4r m1;
    m1[0][0] = 2.0f;
    m1[0][1] = 3.0f;
    m1[1][0] = 4.0f;

    Matrix4r m2 = m1.inverse();

    BOOST_TEST((m1 != m2));
}


BOOST_AUTO_TEST_CASE(testMatrix4rMultiplicationByVector3f) {
    Matrix4r m1;
    m1[0][1] = 2.0f;
    Vector3f v1(2.0f, 1.0f, 1.0f);

    Vector3f res = m1* v1;
    Vector3f v2(4.0f, 1.0f, 1.0f);

    BOOST_TEST((res == v2));
}

BOOST_AUTO_TEST_CASE(testMatrix4rMultiplicationByVector3f2) {
    Matrix4r m1;
    m1[0][1] = 2.0f;
    Vector3f v1(2.0f, 1.0f, 1.0f);

    Vector3f res = m1 * v1;
    Vector3f v2(1.0f, 1.0f, 1.0f);

    bool isEqual = res == v2;

    BOOST_TEST((isEqual == false));
}

BOOST_AUTO_TEST_CASE(testMatrix4rMultiplicationByMatrix4r) {
    Matrix4r m1;
    m1[0][0] = 2.0f;
    Matrix4r m2;

    Vector3f v1(2.0f, 1.0f, 1.0f);

    Vector3f res = m1 * v1;
    Vector3f v2(1.0f, 1.0f, 1.0f);

    bool isEqual = res == v2;

    BOOST_TEST((isEqual == false));
}

BOOST_AUTO_TEST_CASE(testMatrix4rGetValue) {
    Matrix4r m1;

    BOOST_TEST((m1[0][0] == 1));
}

// Matrix3

BOOST_AUTO_TEST_CASE(testMatrix3rEqual) {
    Matrix3r m1;
    Matrix3r m2;

    BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_CASE(testMatrix3rCopy) {
    Matrix3r m1;
    Matrix3r m2(m1);

    BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_CASE(testMatrix3rNotEqual) {
    Matrix3r m1;
    Matrix3r m2;

    m1[0][0] = 42.0f;

    bool isEqual = m1 == m2;

    BOOST_TEST((isEqual == false));
}

BOOST_AUTO_TEST_CASE(testMatrix3rInvert) {
    Matrix3r m1;
    m1[0][0] = 2.0f;
    m1[0][1] = 3.0f;
    m1[1][0] = 4.0f;

    m1.invert();

    Matrix3r m2;
    m2[0][0] = -0.1f;
    m2[0][1] = 0.3f;
    m2[1][0] = 0.4f;
    m2[1][1] = -0.2f;

    BOOST_TEST((m1 == m2));
}

BOOST_AUTO_TEST_CASE(testMatrix3rInverse) {
    Matrix3r m1;
    m1[0][0] = 2.0f;
    m1[0][1] = 3.0f;
    m1[1][0] = 4.0f;

    Matrix3r m2 = m1.inverse();

    Matrix3r m3;
    m3[0][0] = -0.1f;
    m3[0][1] = 0.3f;
    m3[1][0] = 0.4f;
    m3[1][1] = -0.2f;

    BOOST_TEST((m2 == m3));
}

BOOST_AUTO_TEST_CASE(testMatrix3rInverse2) {
    Matrix3r m1;
    m1[0][0] = 2.0f;
    m1[0][1] = 3.0f;
    m1[1][0] = 4.0f;

    Matrix3r m2 = m1.inverse();

    BOOST_TEST((m1 != m2));
}


BOOST_AUTO_TEST_CASE(testMatrix3rMultiplicationByVector3f) {
    Matrix3r m1;
    m1[0][1] = 2.0f;
    Vector3f v1(2.0f, 1.0f, 1.0f);

    Vector3f res = m1 * v1;
    Vector3f v2(4.0f, 1.0f, 1.0f);

    BOOST_TEST((res == v2));
}

BOOST_AUTO_TEST_CASE(testMatrix3rMultiplicationByVector3f2) {
    Matrix3r m1;
    m1[0][1] = 2.0f;
    Vector3f v1(2.0f, 1.0f, 1.0f);

    Vector3f res = m1 * v1;
    Vector3f v2(1.0f, 1.0f, 1.0f);

    bool isEqual = res == v2;

    BOOST_TEST((isEqual == false));
}

BOOST_AUTO_TEST_CASE(testMatrix3rMultiplicationByMatrix3r) {
    Matrix3r m1;
    m1[0][0] = 2.0f;
    Matrix3r m2;

    Vector3f v1(2.0f, 1.0f, 1.0f);

    Vector3f res = m1 * v1;
    Vector3f v2(1.0f, 1.0f, 1.0f);

    bool isEqual = res == v2;

    BOOST_TEST((isEqual == false));
}

BOOST_AUTO_TEST_CASE(testMatrix3rGetValue) {
    Matrix3r m1;

    BOOST_TEST((m1[0][0] == 1));
}

BOOST_AUTO_TEST_SUITE_END()