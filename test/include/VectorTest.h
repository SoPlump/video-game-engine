#pragma once
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <Vector.h>


using namespace engine::physics;

BOOST_AUTO_TEST_SUITE(VectorTest)

BOOST_AUTO_TEST_CASE(testVector3fEqual) {
    Vector3f v1(1.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, 3.0f);

    bool isEqual = v1 == v2;

    BOOST_TEST((isEqual == true));
}

BOOST_AUTO_TEST_CASE(testVector3fEqual2) {
    Vector3f v1(1.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, -3.0f);

    bool isEqual = v1 == v2;

    BOOST_TEST((isEqual == false));
}

BOOST_AUTO_TEST_CASE(testVector3fNotEqual) {
    Vector3f v1(1.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, -3.0f);

    bool isNotEqual = v1 != v2;

    BOOST_TEST((isNotEqual == true));
}

BOOST_AUTO_TEST_CASE(testVector3fNotEqual2) {
    Vector3f v1(1.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, 3.0f);

    bool isNotEqual = v1 != v2;

    BOOST_TEST((isNotEqual == false));
}

BOOST_AUTO_TEST_CASE(testVector3fAdditionByVector3f) {
    Vector3f v1(1.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, -3.0f);
    Vector3f v3 = v1 + v2;

    Vector3f v4(2.0f, 4.0f, 0.0f);

    BOOST_TEST((v3 == v4));
}

BOOST_AUTO_TEST_CASE(testVector3fAdditionByVector3f2) {
    Vector3f v1(1.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, 3.0f);
    Vector3f v3 = v1 + v2;

    Vector3f v4(2.0f, 4.0f, 0.0f);

    BOOST_TEST((v3 != v4));
}

BOOST_AUTO_TEST_CASE(testVector3fSubstractionByVector3f) {
    Vector3f v1(4.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, -1.0f);
    Vector3f v3 = v1 - v2;

    Vector3f v4(3.0f, 0.0f, 4.0f);

    BOOST_TEST((v3 == v4));
}

BOOST_AUTO_TEST_CASE(testVector3fSubstractionByVector3f2) {
    Vector3f v1(4.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, 1.0f);
    Vector3f v3 = v1 - v2;

    Vector3f v4(3.0f, 0.0f, 4.0f);

    BOOST_TEST((v3 != v4));
}

BOOST_AUTO_TEST_CASE(testVector3fDotProductOperatorByVector3f) {
    Vector3f v1(3.0f, -2.0f, 1.5f);
    Vector3f v2(0.0f, -4.0f, -2.0f);
    float res = v1 * v2;

    float trueRes = 5.0;

    BOOST_TEST((res == trueRes));
}

BOOST_AUTO_TEST_CASE(testVector3fDotProductOperatorByVector3f2) {
    Vector3f v1(3.0f, -2.0f, 1.5f);
    Vector3f v2(0.0f, -4.0f, 2.0f);
    float res = v1 * v2;

    float trueRes = 5.0;

    BOOST_TEST((res != trueRes));
}

BOOST_AUTO_TEST_CASE(testVector3fDotProductMethodByVector3f) {
    Vector3f v1(3.0f, -2.0f, 1.5f);
    Vector3f v2(0.0f, -4.0f, -2.0f);
    float res = v1.dotProduct(v2);

    float trueRes = 5.0;

    BOOST_TEST((res == trueRes));
}

BOOST_AUTO_TEST_CASE(testVector3fDotProductMethodByVector3f2) {
    Vector3f v1(3.0f, -2.0f, 1.5f);
    Vector3f v2(0.0f, -4.0f, 2.0f);
    float res = v1.dotProduct(v2);

    float trueRes = 5.0;

    BOOST_TEST((res != trueRes));
}

BOOST_AUTO_TEST_CASE(testVector3fDivisionByScalar) {
    Vector3f v1(8.0f, -2.0f, 1.0f);
    Vector3f v2 = v1 / 4.0f;

    Vector3f v3(2.0f, -0.5f, 0.25f);

    BOOST_TEST((v2 == v3));
}

BOOST_AUTO_TEST_CASE(testVector3fDivisionByScalar2) {
    Vector3f v1(8.0f, -2.0f, -1.0f);
    Vector3f v2 = v1 / 4.0f;

    Vector3f v3(2.0f, -0.5f, 0.25f);

    BOOST_TEST((v2 != v3));
}

BOOST_AUTO_TEST_CASE(testVector3fDivisionByScalarAndAffectation) {
    Vector3f v1(-4.0f, -2.0f, 1.0f);
    v1 /= 2.0f;

    Vector3f v2(-2.0f, -1.0f, 0.5f);

    BOOST_TEST((v1 == v2));
}

BOOST_AUTO_TEST_CASE(testVector3fDivisionByScalarAndAffectation2) {
    Vector3f v1(-4.0f, -2.0f, -1.0f);
    v1 /= 2.0f;

    Vector3f v2(-2.0f, -1.0f, 0.5f);

    BOOST_TEST((v1 != v2));
}

BOOST_AUTO_TEST_CASE(testVector3fLeftMultiplicationByScalar) {
    Vector3f v1(-1.5f, -2.0f, 1.5f);
    Vector3f v2 = v1 * 2.0f;

    Vector3f v3(-3.0f, -4.0f, 3.0f);

    BOOST_TEST((v2 == v3));
}

BOOST_AUTO_TEST_CASE(testVector3fLeftMultiplicationByScalar2) {
    Vector3f v1(-1.5f, -2.0f, -1.5f);
    Vector3f v2 = v1 * 2.0f;

    Vector3f v3(-3.0f, -4.0f, 3.0f);

    BOOST_TEST((v2 != v3));
}

BOOST_AUTO_TEST_CASE(testVector3fRightMultiplicationByScalar) {
    Vector3f v1(3.0f, 1.0f, 1.0f);
    Vector3f v2 = 1.5f * v1;

    Vector3f v3(4.5f, 1.5f, 1.5f);

    BOOST_TEST((v2 == v3));
}

BOOST_AUTO_TEST_CASE(testVector3fRightMultiplicationByScalar2) {
    Vector3f v1(3.0f, 1.0f, 2.0f);
    Vector3f v2 = 1.5f * v1;

    Vector3f v3(4.5f, 1.5f, 1.5f);

    BOOST_TEST((v2 != v3));
}

BOOST_AUTO_TEST_CASE(testVector3fMultiplicationByScalarAndAffectation) {
    Vector3f v1(-1.0f, 4.0f, 0.5f);
    v1 *= 3.0f;

    Vector3f v2(-3.0f, 12.0f, 1.5f);

    BOOST_TEST((v1 == v2));
}

BOOST_AUTO_TEST_CASE(testVector3fMultiplicationByScalarAndAffectation2) {
    Vector3f v1(-1.0f, 4.0f, 0.5f);
    v1 *= 3.0f;

    Vector3f v2(-3.0f, 12.0f, -1.5f);

    BOOST_TEST((v1 != v2));
}

BOOST_AUTO_TEST_CASE(testVector3fCrossProductByVector3f) {
    Vector3f v1(4.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, -1.0f);
    Vector3f v3 = v1.crossProduct(v2);

    Vector3f v4(-8.0f, 7.0f, 6.0f);

    BOOST_TEST((v3 == v4));
}

BOOST_AUTO_TEST_CASE(testVector3fCrossProductByVector3f2) {
    Vector3f v1(4.0f, 2.0f, 3.0f);
    Vector3f v2(1.0f, 2.0f, -1.0f);
    Vector3f v3 = v1.crossProduct(v2);

    Vector3f v4(-8.0f, 7.0f, -6.0f);

    BOOST_TEST((v3 != v4));
}

BOOST_AUTO_TEST_CASE(testVector3fComponentProductByVector3f) {
    Vector3f v1(3.0f, 1.5f, 3.0f);
    Vector3f v2(2.0f, -2.0f, -1.0f);
    Vector3f v3 = v1.componentProduct(v2);

    Vector3f v4(6.0f, -3.0f, -3.0f);

    BOOST_TEST((v3 == v4));
}

BOOST_AUTO_TEST_CASE(testVector3fComponentProductByVector3f2) {
    Vector3f v1(3.0f, 1.5f, 3.0f);
    Vector3f v2(2.0f, -2.0f, -1.0f);
    Vector3f v3 = v1.componentProduct(v2);

    Vector3f v4(6.0f, -3.0f, 3.0f);

    BOOST_TEST((v3 != v4));
}

BOOST_AUTO_TEST_CASE(testVector3fComponentProducUpdatedByVector3f) {
    Vector3f v1(3.0f, 1.5f, 3.0f);
    Vector3f v2(2.0f, -2.0f, -1.0f);
    v1.componentProductUpdate(v2);

    Vector3f v3(6.0f, -3.0f, -3.0f);
    
    BOOST_TEST((v1 == v3));
}

BOOST_AUTO_TEST_CASE(testVector3fComponentProducUpdatedByVector3f2) {
    Vector3f v1(3.0f, 1.5f, 3.0f);
    Vector3f v2(2.0f, -2.0f, -1.0f);
    v1.componentProductUpdate(v2);

    Vector3f v3(6.0f, -3.0f, 3.0f);

    BOOST_TEST((v1 != v3));
}

BOOST_AUTO_TEST_CASE(testVector3fAddScaledVector3f) {
    Vector3f v1(0.0f, -0.5f, 2.0f);
    Vector3f v2(-0.5f, -1.0f, 1.5f);
    v1.addScaledVector(v2, 4.0f);

    Vector3f v3(-2.0f, -4.5f, 8.0f);

    BOOST_TEST((v1 == v3));
}

BOOST_AUTO_TEST_CASE(testVector3fAddScaledVector3f2) {
    Vector3f v1(0.0f, -0.5f, 2.0f);
    Vector3f v2(-0.5f, -1.0f, -1.5f);
    v1.addScaledVector(v2, 4.0f);

    Vector3f v3(-2.0f, -4.5f, 8.0f);

    BOOST_TEST((v1 != v3));
}

BOOST_AUTO_TEST_CASE(testVector3fNormalize) {
    Vector3f v1(0.0f, -3.0f, 4.0f);
    v1.normalize();

    Vector3f v2(0.0f, -0.6f, 0.8f);

    BOOST_TEST((v1 == v2));
}

BOOST_AUTO_TEST_CASE(testVector3fNormalize2) {
    Vector3f v1(0.0f, -3.0f, 4.0f);
    v1.normalize();

    Vector3f v2(0.0f, -0.6f, -0.8f);

    BOOST_TEST((v1 != v2));
}

BOOST_AUTO_TEST_CASE(testVector3fNormalizingZeroVector) {
    Vector3f v1(0.0f, 0.0f, 0.0f);
    v1.normalize();

    Vector3f v2(0.0f, 0.0f, 0.0f);

    BOOST_TEST((v1 == v2));
}

BOOST_AUTO_TEST_CASE(testVector3fNormalizingZeroVector2) {
    Vector3f v1(0.0f, 0.0f, 0.0f);
    v1.normalize();

    Vector3f v2(0.0f, 0.0f, 0.1f);

    BOOST_TEST((v1 != v2));
}

BOOST_AUTO_TEST_CASE(testVector3fMagnitude) {
    Vector3f v1(0.0f, -3.0f, 4.0f);
    float mag = v1.magnitude();

    float trueMag = 5.0f;

    BOOST_TEST((mag == trueMag));
}

BOOST_AUTO_TEST_CASE(testVector3fMagnitude2) {
    Vector3f v1(0.0f, -3.0f, 4.0f);
    float mag = v1.magnitude();

    float trueMag = 6.0f;

    BOOST_TEST((mag != trueMag));
}

BOOST_AUTO_TEST_CASE(testVector3fSquareMagnitude) {
    Vector3f v1(6.0f, -2.0f, 1.5f);
    float squareMag = v1.squareMagnitude();

    float trueSquareMag = 42.25f;

    BOOST_TEST((squareMag == trueSquareMag));
}

BOOST_AUTO_TEST_CASE(testVector3fSquareMagnitude2) {
    Vector3f v1(6.0f, -2.0f, 1.5f);
    float squareMag = v1.squareMagnitude();

    float trueSquareMag = 43.25f;

    BOOST_TEST((squareMag != trueSquareMag));
}

BOOST_AUTO_TEST_CASE(testVector3fclear) {
    Vector3f v1(6.0f, -2.0f, 1.5f);
    Vector3f v2(0.0f, 0.0f, 0.0f);
    v1.clear();

    BOOST_TEST((v1 == v2));
}

BOOST_AUTO_TEST_SUITE_END()
