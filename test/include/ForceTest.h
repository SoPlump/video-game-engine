#pragma once

#include <Vector.h>
#include <cmath>
#include "pfgen.h" 
#include <system/Particle.h>
#include <ecm/Entity.h>
#include <components/Transform3D.h>

using namespace engine::physics;

BOOST_AUTO_TEST_SUITE(ForceTest)

const real delta = 0.2f;
const real epsilon = 0.01f;

BOOST_AUTO_TEST_CASE(testForceParticleGravity) {

    auto entity = std::make_shared<Entity>();
    entity->addComponent<Transform3D>();
    auto particle = entity->addComponent<Particle>();

    particle->setMass(10);
    particle->setPosition(Vector3r(0.0f, 0.0f, 0.0f));

    auto pfrPointer = std::make_shared<ParticleForceRegistry>(ParticleForceRegistry());
    auto gravity = std::make_shared<ParticleGravity>(Vector3r(0.0f, -9.81f, 0.0f));
    pfrPointer->add(particle, gravity);

    pfrPointer->updateForces(delta);

    Vector3r supposedForce = Vector3r(0.0f, -98.1f, 0.0f);
    Vector3r resForce = particle->getForceAccum();

    real diff = std::abs(resForce.y - supposedForce.y);
    
    BOOST_TEST(diff <= epsilon);
}

BOOST_AUTO_TEST_CASE(testForceParticleDrag) {
    auto entity = std::make_shared<Entity>();
    entity->addComponent<Transform3D>();
    auto particle = entity->addComponent<Particle>();

    particle->setMass(10);
    particle->setVelocity(Vector3r(1.0f, 0.0f, 0.0f));
    particle->setPosition(Vector3r(0.0f, 0.0f, 0.0f));

    auto pfrPointer = std::make_shared<ParticleForceRegistry>(ParticleForceRegistry());
    auto drag = std::make_shared<ParticleDrag>(2.0f, 4.0f);
    pfrPointer->add(particle, drag);

    pfrPointer->updateForces(delta);

    Vector3r supposedForce = Vector3r(-6.0f, 0.0f, 0.0f);
    Vector3r resForce = particle->getForceAccum();

    real diff = std::abs(resForce.y - supposedForce.y);

    BOOST_TEST(diff <= epsilon);
}

BOOST_AUTO_TEST_CASE(testForceParticleSpring) {

    auto entity = std::make_shared<Entity>();
    entity->addComponent<Transform3D>();
    auto particle = entity->addComponent<Particle>();

    auto otherParticle = entity->addComponent<Particle>();

    particle->setMass(10);
    particle->setPosition(Vector3r(10.0f, 0.0f, 0.0f));

    otherParticle->setMass(15);
    otherParticle->setPosition(Vector3r(9.0f, 0.0f, 0.0f));

    auto pfrPointer = std::make_shared<ParticleForceRegistry>(ParticleForceRegistry());
    auto spring = std::make_shared<ParticleSpring>(otherParticle, 4.0f, 5.0f);
    pfrPointer->add(particle, spring);

    pfrPointer->updateForces(delta);

    Vector3r supposedForce = Vector3r(16.0f, 0.0f, 0.0f);
    Vector3r resForce = particle->getForceAccum();

    real diff = std::abs(resForce.x - supposedForce.x);

    BOOST_TEST(diff <= epsilon);
}

BOOST_AUTO_TEST_CASE(testForceParticleAnchoredSpring) {

    auto entity = std::make_shared<Entity>();
    entity->addComponent<Transform3D>();
    auto particle = entity->addComponent<Particle>();

    particle->setMass(10);
    particle->setVelocity(Vector3r(1.0f, 0.0f, 0.0f));
    particle->setPosition(Vector3r(10.0f, 0.0f, 0.0f));

    auto anchor = std::make_shared<Vector3r>(0.0f, 0.0f, 0.0f);

    auto pfrPointer = std::make_shared<ParticleForceRegistry>(ParticleForceRegistry());
    auto anchoredSpring = std::make_shared<ParticleAnchoredSpring>(anchor, 2.0f, 4.0f);
    pfrPointer->add(particle, anchoredSpring);

    pfrPointer->updateForces(delta);

    Vector3r supposedForce = Vector3r(-192.0f, 0.0f, 0.0f);
    Vector3r resForce = particle->getForceAccum();

    real diff = std::abs(resForce.y - supposedForce.y);

    BOOST_TEST(diff <= epsilon);
}

BOOST_AUTO_TEST_SUITE_END()